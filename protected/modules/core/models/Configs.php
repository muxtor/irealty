<?php

/**
 *  Config class file
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 *
 *
 *
 * @property $config_id
 * @property $config_name
 * @property $config_value
 * @property $config_type
 * @property $config_title
 *
 *
 * @name Config
 * @package
 * @version 0.1
 *
 */

class Configs extends CActiveRecord{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('config_name, config_title','required'),
            array('config_name', 'unique')
        );
    }

    public function tableName()
    {
        return 'core_configs';
    }

    function attributeLabels() {
        return array(
            'config_name'  => 'alias параметра',
            'config_value' => 'значение параметра',
            'config_title' => 'название параметра',
        );
    }



}