CREATE TABLE IF NOT EXISTS `core_configs` (
  `config_id` int(11) NOT NULL auto_increment,
  `config_name` varchar(255) default NULL,
  `config_value` varchar(255) default NULL,
  `config_type` int(11) NOT NULL default '1',
  `config_title` varchar(255) NOT NULL,
  PRIMARY KEY  (`config_id`),
  UNIQUE KEY `config_name` (`config_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;