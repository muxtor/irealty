<?php

/**
 * CoreModule файл класса.
 *
 * Модуль Core - основной модуль системы для веб приложений.
 *
 * Модуль core содержит в себе все основные компоненты, которые используются другими модулями
 * Это наше ядро.
 *
 */
class CoreModule extends UWebModule {

    /**
     * Layout по умолчанию административной части приложения. Для всей административной части приложения Layout
     * находиться в теме по умолчанию 'fit-admin'.
     * @var type string
     */
    public $adminLayout = 'main';

    /**
     * Тема по умолчанию административной части приложения.
     * @var type string
     */
    private $adminTheme = 'admin';
 
    
    public $adminUrl = '/admin/main/index';
    public $cabinetUrl = '/cabinet/main/index';
  
    /**
     * Возвращает alias для layout административной части приложения используеться в UBackController.
     *
     * @return type alias
     */
    public function getAdminLayoutAlias() {
        return 'webroot.themes.' . $this->adminTheme . '.views.layouts.' . $this->adminLayout;
    }

    /**
     * Возвращает тему для административной части приложения. Используеться в UBackController.
     * @return string
     */
    public function getAdminTheme() {
        if (isset($this->adminTheme)) {
            return $this->adminTheme;
        } else {
            return null;
        }
    }

   
    public function init() {
        parent::init();
        
        
        $this->setImport(array(
            'core.components.*',
            'core.models.*',
            'core.helpers.*'
        ));
    }

    /* public function beforeControllerAction($controller, $action) {
      if (parent::beforeControllerAction($controller, $action)) {
      return true;
      } else {
      return false;
      }


      } */
}
