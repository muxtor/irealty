<?php

/**
 * 
 * Основной файл скласса контролера
 *  
 */
class UMainController extends CController {

    public $core;
    public $breadcrumbs = array();

    
    public function init() {
        parent::init();
        $this->core = Yii::app()->getModule('core');
    }

    protected function performAjaxValidation($model, $id) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === $id) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function setPageTitle($value) {
        $this->pageTitle = Yii::app()->name . ' - ' . $value;
    }
    
}
