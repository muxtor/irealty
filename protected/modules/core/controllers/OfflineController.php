<?php

/**
 *  OfflineClass class file
 * -----------------------------------------------------------------------------
 * Контроллер отвечающий за обработку ошибок при каких 
 * приложения не будет роботать. Например неподдерживаемый браузер,
 * отключен javaScript, или приложения в режиме техобслуживания.
 * 
 * Все представления размещаются в теме для административной части. По умолчанию 'fit-admin'
 * -----------------------------------------------------------------------------
 *
 * 
 * @property 
 * 
 * @name OfflineClass
 * @package
 * @version 0.1
 * @author timur
 * 
 */

class OfflineController extends UBackController{
    
    /**
     * Layout размещается в теме по умолчанию fit-admin
     * 
     * @var type 
     */
    public $layout = '//layouts/error';
 
    /**
     * Приложения в режиме техобслуживания.
     */
    public function actionMaintenance() {

        $this->render('maintenance');   
    }
    
    /**
     * Неподдерживаемый браузер. 
     */
    public function actionBadBrowser() {
        
        $this->render('badbrowser');
    }
    
    /**
     * отключен javaScript 
     */
    public function actionNoScript() {
        
        $this->render('noscript');
    }
    
    
}