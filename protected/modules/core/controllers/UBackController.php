<?php

/**
 *  Основной контроллер админ части приложения
 *
 */
class UBackController extends UMainController {

	private $_behaviorIDs = array();

    public $header_info = array(
    );

    public function actionMultipleRemove() {
        if (!isset($_POST['data']))
            RAjax::error(array('messages' => 'error'));
        $removes = array();
        $removes = CJSON::decode($_POST['data']);

        if (is_array($removes) && count($removes) > 0) {
            try {
                foreach ($removes as $item) {
                    $this->_loadModel($item)->delete();
                }
            } catch (Exception $e) {
                RAjax::error(array('messages' => 'Error'));
            }
        }
        RAjax::success(array('messages' => "Выбрание элементы успешно удалены"));
    }

    public function init() {
        parent::init();

        $this->layout = $this->core->getAdminLayoutAlias();

        Yii::app()->theme = $this->core->getAdminTheme();
    }

    public function createMultilanguageReturnUrl($lang = 'en')
    {
        if (count($_GET) > 0) {
            $arr = $_GET;
            $arr['language'] = $lang;
        } else
            $arr = array('language' => $lang);
        return $this->createUrl('', $arr);
    }

    protected function loadModel($class, $id, $criteria = array(), $exceptionOnNull = true)
    {
        if (empty($criteria))
            $model = CActiveRecord::model($class)->findByPk($id);
        else {
            $finder = CActiveRecord::model($class);
            $c = new CDbCriteria($criteria);
            $c->mergeWith(array(
                'condition' => $finder->getTableAlias(true) . '.' . $finder->tableSchema->primaryKey . '=:id',
                'params' => array(':id' => $id),
            ));
            $model = $finder->find($c);
        }
        if (isset($model))
            return $model;
        else if ($exceptionOnNull)
            throw new CHttpException(404, 'Unable to find the requested object.');
    }
	
    public function createAction($actionID)
    {
        $action = parent::createAction($actionID);
        if ($action !== null)
            return $action;
        foreach ($this->_behaviorIDs as $behaviorID) {
            $object = $this->asa($behaviorID);
            if ($object->getEnabled() && method_exists($object, 'action' . $actionID))
                return new CInlineAction($object, $actionID);
        }
    }

    public function attachBehavior($name, $behavior)
    {
        $this->_behaviorIDs[] = $name;
        parent::attachBehavior($name, $behavior);
    }	
	
	
//    public function getPageTitle() {
//        //Yii::app()->name = '';
//        return parent::getPageTitle();
//    }
//
//    public function setPageTitle($value) {
//        //Yii::app()->name = '';
//        return parent::setPageTitle($value);
//    }
}
