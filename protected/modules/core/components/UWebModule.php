<?php

/**
 *  UWebModule class file
 * 
 * -----------------------------------------------------------------------------
 * Основной класс модуля, все остальные модули наследуются от него. Классы ядра рекомендуется именовать с буквы "U", пример UWebUser.
 * -----------------------------------------------------------------------------
 * 
 * 
 * @name UWebModule
 * @package
 * @version 0.1
 * 
 */
class UWebModule extends CWebModule {

    /**
     * @var string ID контроллер по умолчанию
     * @var type 
     */
    public $defaultController = 'default';

    
    protected function init() {
        return parent::init();
    }


}