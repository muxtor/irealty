<?php

class UploadedFileBehavior extends CActiveRecordBehavior
{

    public $attributeName = 'file';
    public $attributeMainName = '';

    /**
     * @var string алиас директории, куда будем сохранять файлы
     */
    public $savePathAlias = 'webroot.images.service';
    public $scenarios = array('insert', 'update');
    public $fileTypes = 'jpg,jpeg,png,gif';
    public $maxSizeFile = 10485760; //10 MB
    /**
     *
     * @var width
     * @var height
     * @var master (auto, none, width, height) - мастер ресайз
     *
     *
     */
    public $height;
    public $width;
    public $master;

    /**
     * Шорткат для Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR.
     * Возвращает путь к директории, в которой будут сохраняться файлы.
     * @return string путь к директории, в которой сохраняем файлы
     */
    public function getSavePath()
    {
        return Yii::getPathOfAlias($this->savePathAlias) . DIRECTORY_SEPARATOR;
    }

    public function saveThumbs($file)
    {
        if (!is_dir($this->getSavePath() . 'thumbs')) {
            throw new CException('отсутствует каталог Thumbs');
        }

        if (!Yii::app()->hasComponent('image')) {
            throw new CException('not found component Image');
        }

        $masterImg = null;

        switch ($this->master) {
            case 'none':
                $masterImg = Image::NONE;
                break;
            case 'auto':
                $masterImg = Image::AUTO;
                break;
            case 'height':
                $masterImg = Image::HEIGHT;
                break;
            case 'width':
                $masterImg = Image::WIDTH;
                break;
            default :
                $masterImg = null;
        }

        $images_res = Yii::app()->image->load($this->getSavePath() . $file);

        //-------------
//        $images_res->resize($this->width, $this->height, $masterImg)->specialResize($this->width, $this->height)->crop($this->width,$this->height);
        $images_res->centeredpreview($this->width, $this->height);
        //-------------

        $file_name = substr($file, 0, strpos($file, '.'));
        $file_ext = substr($file, strpos($file, '.') + 1);

        $images_res->save($this->getSavePath() . 'thumbs' . DIRECTORY_SEPARATOR . '_' . $file_name . "." . $file_ext);
    }

    public function attach($owner)
    {
        parent::attach($owner);
        //add validation
        if (in_array($owner->getScenario(), $this->scenarios)) {
            $fileValidator = CValidator::createValidator(
                'file', $owner, $this->attributeName, array(
                    'types' => $this->fileTypes,
                    'allowEmpty' => true,
                    'maxSize' => $this->maxSizeFile,
                    'safe' => false
                )
            );
            $owner->validatorList->add($fileValidator);
        }
    }

    public function beforeSave($event)
    {
        if ($file = CUploadedFile::getInstance($this->getOwner(), $this->attributeName)) {
            if (!$this->getOwner()->isNewRecord) {
                $this->deleteFile();
            }


            $name_file = md5(time() . $file->getSize() . $file->getTempName());
            $name_ext = $file->getExtensionName();

            $this->getOwner()->setAttribute($this->attributeName, $name_file . "." . $name_ext);

            if ($file->saveAs($this->getSavePath() . $name_file . "." . $name_ext)) {
                $this->saveThumbs($name_file . "." . $name_ext);
            } else {
                throw new CException('При сохранении файла произошла ошибка');
            }
        }
        return true;
    }

    public function beforeDelete($event)
    {
        $this->deleteFile();
        return true;
    }

    public function deleteFile()
    {
        $filePath = $this->getSavePath() . $this->getOwner()->getAttribute($this->attributeName);
        $filePath_thumbs = $this->getSavePath() . 'thumbs' . DIRECTORY_SEPARATOR . '_' . $this->getOwner()->getAttribute($this->attributeName);


        if (file_exists($filePath) && file_exists($filePath_thumbs) && trim($this->getOwner()->getAttribute($this->attributeName)) !== '') {

            if (!@unlink($filePath)) {
                throw new CException('При удалении файла произошла ошибка');
            }
            if (!@unlink($filePath_thumbs)) {
                throw new CException('При удалении файла произошла ошибка');
            }
        }
    }

}
