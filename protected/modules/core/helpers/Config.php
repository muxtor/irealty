<?php

/**
 *  Config class file
 * -----------------------------------------------------------------------------
 * 
 * -----------------------------------------------------------------------------
 *
 * 
 * @property 
 * 
 * @name Config
 * @package
 * @version 0.1
 * @author timur
 * 
 */
class Config {

    private static $_model;
    private static $_configs = array();
    
    public function __construct() {
   
    }
    
    public static function get($name)
    {
       
        
        if (isset(Yii::app()->params[$name])) {
            return Yii::app()->params[$name];

            } else {
                
            if (isset(self::$_configs[$name])){
                return self::$_configs[$name];
            }
           
            if (is_null(self::$_model)) {
                Yii::import('core.models.Configs');
                self::$_model = Configs::model();
                
            }
            self::$_configs[$name] = self::$_model->find("config_name=:config_name", array(":config_name" => $name))->config_value;
                    
            return self::$_configs[$name];
        }
        
    }
    
    
}