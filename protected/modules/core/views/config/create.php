<?php
/*********************** setings ***************************************/
    $this->pageTitle = 'Создания конфигурации сайта';
?>
12
<?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'config_form',
        'type' => 'horizontal',
        'enableAjaxValidation' => true,
        'clientOptions'=>array(
            'validateOnSubmit' => true,
        )
    ));
?>

<?php echo $form->textFieldRow($model, 'config_name', array('hint' => '')); ?>
<?php echo $form->textFieldRow($model, 'config_title', array('hint' => '')); ?>
<?php echo $form->textFieldRow($model, 'config_value', array('hint' => '')); ?>

<div class="form-actions">
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'success',
    'label' => 'Сохранить'
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'type'       => 'danger',
    'url'        => Yii::app()->createUrl('/core/config'),
    'label'      => 'Отмена'
)); ?>

</div>

<?php $this->endWidget();?>