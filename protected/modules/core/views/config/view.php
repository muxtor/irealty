<?php
/* ********************** setings ************************************** */
    $this->pageTitle = 'Конфигурации';
?>
<?php if (count($model)>0): ?>
    <p>Нет динамичных конфигурацый</p>
<?php endif; ?>

<?php echo CHtml::beginForm(); ?>
<?php foreach($model as $row):?>

<?php echo CHtml::label($row->config_title,"Config[{$row->config_name}]"); ?>
<?php if ($row->config_id==15) {
    cs()->registerScript('ConfigMaskedField','
        $(".config-phone-mask").mask("+38(999)999-99-99");
    ', CClientScript::POS_READY);
    echo CHtml::textField("Configs[{$row->config_name}]", $row->config_value, array('class'=>'config-phone-mask'));
}
else if ($row->config_id==16) {
    echo CHtml::checkBox("Configs[{$row->config_name}]", $row->config_value);
}
else echo CHtml::textArea("Configs[{$row->config_name}]", $row->config_value, array('cols'=>'10','rows'=>'1')); ?>

<?php endforeach;?>
<div class="header_main_ctn">
    <h2>Настойки</h2>
     <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'success',
        'label' => 'Сохранить'
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'link',
        'type'       => 'danger',
        'url'        => Yii::app()->createUrl('/core'),
        'label'      => 'Отмена'
    )); ?>
</div>
<?php echo CHtml::endForm();?>