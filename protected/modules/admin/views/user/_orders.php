<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i></i><?= Yii::t('admin', 'Истории оплаты'); ?></span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/user/index'); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>
<?php

$csrfTokenName = Yii::app()->request->csrfTokenName;
$csrfToken = Yii::app()->request->csrfToken;
$csrf = "'$csrfTokenName':'$csrfToken'";

$this->widget('bootstrap.widgets.TTbExtendedGridView', array(
    //'type' => 'striped bordered condensed',
    'id' => 'users-list',
    //'enableSorting' => false,
    //'itemsCssClass' => 'table-normal table-hover-row',
    'dataProvider' => $model->search(),
    'ajaxUrl' => array('/admin/pays/index'),
    //'filter' => $model,
    'summaryText' => Yii::t('admin', 'Истории оплаты').' '. Yii::t('admin', '{start}—{end} из <span>{count}</span>.'),
    'columns' => array(
        array(
            'name' => 'id',
            'value' => '$data->id',
            'type' => 'raw',
            'htmlOptions' => array('width' => '30px'),
        ),
        array(
            'name' => 'userId',
            'value' => '$data->user()',
            'type' => 'raw',
            'htmlOptions' => array('width' => '30px'),
        ),
        array(
            'name' => 'payPrice',
            'value' => '$data->payPrice." руб"',
            'type' => 'raw',
            'htmlOptions' => array('width' => '120px'),
        ),
        'created_on',
        'payedDate',
        'outDate',
        array(
            'type' => 'raw',
            'name' => 'status',
            'value' => '$data->getStatusForTable()',
            'htmlOptions' => array('width' => '120px'),
        ),
        /*array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'header' => 'Действия',
            'class' => 'bootstrap.widgets.TTbButtonColumn',
            'template' => '{update}',
            'afterDelete' => 'function(link,success,data){
                            data =  $.parseJSON(data);
                            if(data.response.status=="success"){
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                                                 
                                    message: { text: data.response.data.messages }
                                }).show();
                            }else{
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                 
                                    message: { text: data.response.data.messages }
                                }).show();

                        }
                        }',
        )*/
    )
));?>
<!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->

