<?php
$pageName = Yii::t('admin', 'Editing Page');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Страницы',
        'url' => array('/admin/user/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать страницу',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>