<?php
$pageName = Yii::t('admin', 'Редактирование пользователя');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => Yii::t('admin', 'Пользователи'),
        'url' => array('/admin/user/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. Yii::t('admin', 'Редактирование пользователя'),
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

echo $this->renderPartial('_orders', array('model' => $orders, 'icon' => 'icon-edit'));

?>