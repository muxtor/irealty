<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i><?= Yii::t('admin', 'Пользователь'); ?>: Баланс <?= $model->Balance() ?></span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/user/index'); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>

                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-user',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);?>
                                <?php

                                echo $form->checkBoxRow($model, 'role');
                                
                                echo $form->textFieldRow($model, 'login', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'pswd', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'name', array('class' => 'span12'));
                                ?>
                    </div>


                </div>


                <!--end paped -->
                <div class="form-actions">
                    <div class="pull-right">
                        <?php
                        echo CHtml::link(
                            '<span class="icon-circle-arrow-left"></span> ' . Yii::t('admin', 'Return'),
                            Yii::app()->createUrl('/admin/user/index'),
                            array(
                                'class' => 'link'
                            )
                        );
                        ?>
                        <?php
                        $this->widget(
                            'bootstrap.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'label' => Yii::t('admin', 'Save'),
                                'type' => null,
                                'htmlOptions' => array(
                                    'class' => 'btn btn-default',
                                    'value' => 'index',
                                    'name' => 'submit',
                                ),
                                'size' => 'small',
                            )
                        );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->
