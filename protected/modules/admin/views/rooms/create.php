<?php
$pageName = Yii::t('admin', 'Создать комната');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Комнаты',
        'url' => array('/admin/rooms/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать комната',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>