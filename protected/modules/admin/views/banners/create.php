<?php
$pageName = Yii::t('admin', 'Создать баннер');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Баннеры',
        'url' => array('/admin/banners/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать баннер',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>