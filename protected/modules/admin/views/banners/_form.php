<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i><?= Yii::t('admin', 'Баннеры'); ?></span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/banners/index'); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>

                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-pages',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);?>
                                <?php

                                echo $form->textFieldRow($model, 'title', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'link', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'text1', array('class' => 'span12'));

                                Yii::import('ext.redactor.RedactorWidget');
                                $this->widget(
                                    'RedactorWidget',
                                    array(
                                        'model' => $model,
                                        'attribute' => 'text',
                                    )
                                );
                        echo $form->checkBoxRow($model, 'status', array('class' => ''));

                        echo $form->fileFieldRow($model, 'image', array('class' => 'span4'));

                        if (!$model->isNewRecord && !empty($model->image))
                            echo "<img src='".Banner::THUMB_PATH.$model->image."'/>";



                                ?>
                    </div>


                </div>


                <!--end paped -->
                <div class="form-actions">
                    <div class="pull-right">
                        <?php
                        echo CHtml::link(
                            '<span class="icon-circle-arrow-left"></span> ' . Yii::t('admin', 'Return'),
                            Yii::app()->createUrl('/admin/banners/index'),
                            array(
                                'class' => 'link'
                            )
                        );
                        ?>
                        <?php
                        $this->widget(
                            'bootstrap.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'label' => Yii::t('admin', 'Save'),
                                'type' => null,
                                'htmlOptions' => array(
                                    'class' => 'btn btn-default',
                                    'value' => 'index',
                                    'name' => 'submit',
                                ),
                                'size' => 'small',
                            )
                        );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->
