<?php

$csrfTokenName = Yii::app()->request->csrfTokenName;
$csrfToken = Yii::app()->request->csrfToken;
$csrf = "'$csrfTokenName':'$csrfToken'";

Yii::app()->clientScript->registerScript('multidelete', '
    function multiDelete(values){
    $elements = [];    
    $.each(values,function(i, val){
        $elements.push($(val).val());
    });
    $.ajax({
        url:"' . $this->createUrl('/admin/page/MultipleRemove') . '",
        data:{data:JSON.stringify($elements), ' . $csrf . '},
        dataType:"json",
        type:"POST",
        success:function(data){
            if (data.response.status=="success"){        
                $.fn.yiiGridView.update(\'pages-list\');
                    $(".top-right").notify({
                        type:"bangTidy",
                        fadeOut:{enabled: true, delay: 3000 },
                        transition:"fade",                                                                                 
                        message: { text: data.response.data.messages }
                    }).show();

                } else {
                    $(".top-right").notify({
                        type:"bangTidy",
                        fadeOut:{enabled: true, delay: 3000 },
                        transition:"fade",                                                                                 
                        message: { text: data.response.data.messages }
                    }).show();
                }
        }
    });
    }
');




$this->widget('bootstrap.widgets.TTbExtendedGridView', array(
    //'type' => 'striped bordered condensed',
    'id' => 'pages-list',
    //'enableSorting' => false,
    //'itemsCssClass' => 'table-normal table-hover-row',
    'dataProvider' => $model->search(),
    'ajaxUrl' => array('/admin/page/index'),
    'filter' => $model,
    'summaryText' => Yii::t('admin', 'Pages').' '. Yii::t('admin', '{start}—{end} from <span>{count}</span>.'),
    'bulkActions' => array(
        'actionButtons' => array(
            array(
                'buttonType' => 'button',
                'type' => 'danger',
                'size' => 'small',
                'label' => Yii::t('admin', 'Delete selected'),
                'click' => 'js:function(values){if(confirm("'.Yii::t('admin', 'Are you sure that you want to delete this items?').'")){multiDelete(values);} }'
            //'click' => 'js:bootbox.confirm("<p class=\'lead\'>Вы действительно хотите удалить выбранные вами страницы?</p>",
            //function(value){console.log("Confirmed: "+value);})'
            ),
        ),
        'checkBoxColumnConfig' => array(
            'name' => 'id'
        ),
    ),
    'columns' => array(
        array(
            'name' => 'id',
            'value' => '$data->id',
            'type' => 'raw',
            'htmlOptions' => array('width' => '30px'),
        ),      
        array(
            'name' => 'title',
            'value' => '$data->title',
            'type' => 'raw',
             'htmlOptions' => array('width' => '400px'),
        ),
        array(
            'name' => 'meta_description',
            'value' => '$data->meta_description',
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            //'header' => 'Действия',
            'class' => 'bootstrap.widgets.TTbButtonColumn',
            'template' => '{update}  {delete}',
            'afterDelete' => 'function(link,success,data){
                            data =  $.parseJSON(data);
                            if(data.response.status=="success"){
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                                                 
                                    message: { text: data.response.data.messages }
                                }).show();
                            }else{
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                 
                                    message: { text: data.response.data.messages }
                                }).show();

                        }
                        }',
        )
    )
));
