<?php

$this->pageTitle = Yii::t('admin', 'Home');

$this->header_info = array(
    'icon' => 'icon-home icon-2x',
    'title' => Yii::t('admin', 'Home'),
    'description' => Yii::t('admin', 'This tab contains a list of frequently used modules'),

);
?>
<div class="container-fluid padded">
    <!-- find me in partials/action_nav_normal_one_row -->
    <hr class="divider">
    <!--big normal buttons-->
    <div class="action-nav-normal">



    </div>
    <hr class="divider">


</div>