$('document').ready(function(){
	function showFormTab(id) {
		$('.js-tab_content').hide();
		$(id).show();
	}	
	
	$('a.lang_tab').click(function(e){
		e.preventDefault();
		showFormTab($(this).attr('href'));
	})
	
	showFormTab($('a.lang_tab:eq(0)').attr('href'));
	
	/*
	$('.js-link-add-to-cart').click(function(e){
		e.preventDefault();
		$.get($(this).attr('href'), function(response){
			alert(response);
        }, 'text')		
	})
	
	$(".js-only-number").keydown(function (event) {
                 // Allow: backspace, delete, tab, escape, and enter
                 if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                     // Allow: Ctrl+A
                     (event.keyCode == 65 && event.ctrlKey === true) ||
                     // Allow: home, end, left, right
                     (event.keyCode >= 35 && event.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
                 }
                 else {
                     // Ensure that it is a number and stop the keypress
                     if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                         event.preventDefault();
                     }
                 }
    });	
	*/
	
});
