//sortable
jQuery(function($) {
    if (jQuery().sortable) {
        $("ul.sortable").sortable({
            handle: '.crosshair',

            stop: function(event, ui) {
                var item = $(ui.item);
                if ($(item).siblings().size() == 0) {
                    return;
                }

                var insertBeforeUrl = $(this).attr("data-val-beforeUrl");
                var insertAfterUrl = $(this).attr("data-val-afterUrl");

                if (!$(item).is(":last-child")) {
                    var url = insertBeforeUrl;

                    var insertId = $(item).attr("data-val-id");
                    var beforeId = $(item).next().attr("data-val-id");

                    var data = {
                        'insertId': insertId, 
                        'beforeId' : beforeId
                    };
                }
                else {
                    var url = insertAfterUrl;

                    var insertId = $(item).attr("data-val-id");
                    var afterId = $(item).prev().attr("data-val-id");

                    var data = {
                        'insertId': insertId, 
                        'afterId' : afterId
                    };
                }



                //alert($(this).sortable('serialize'));
                $.ajax({
                    async: false,
                    type: "GET",
                    url: url,
                    data: data,
                    success: function() {
                    // console.log('ok');
                    },
                    error: function() {
                    // console.log('error');
                    }
                });
            }
        });
        $("ul.sortable").disableSelection();
        
        $("ul.sortable-slider").sortable({
            handle: '.crosshair',

            stop: function() {
                
                var arr = [];
                
                var url = $(this).attr("link");
                
                $('ul.sortable-slider. li').each(function(index) {
                    arr[index] = $(this).attr("data-val-id");
                    //alert(index +'> '+arr[index] + " > "+$(this).attr("data-val-id"));
                });
                
                $.ajax({
                    async: false,
                    type: "POST",
                    url: url,
                    data: "arr[]="+arr,
                    success: function() {
                        
                    },
                    error: function() {
                        
                    }
                });
                
                
            }
        });
        $("ul.sortable-slider").disableSelection();
    }
});


//fancybox
$(document).ready(function(){
jQuery(function($) {
    if (jQuery().fancybox) {
        var defaultWidth = 800;
        var defaultHeight = 800;
        var defaultPadding = 30;
        $(".fancybox-modal").each(function() {
            var autoScale = $(this).attr('data-val-fbAutoScale') || 'true';
            autoScale = !(autoScale.toLowerCase() === 'false');
            $(this).fancybox({
                type : "iframe",
                padding: parseInt($(this).attr("data-val-fbPadding")) || defaultPadding,
                width: parseInt($(this).attr("data-val-fbWidth")) || defaultWidth,
                height: parseInt($(this).attr("data-val-fbHeight")) || defaultHeight,
                showCloseButton: true,
                enableEscapeButton: false,
                hideOnOverlayClick: false,
                autoScale: autoScale
            });
        });
        $('.fancybox-custom-close').click(function() {
            //alert('oik');
            //parent.$.fancybox.close();
        });
        $('a.fancybox-single-img').fancybox();
    }
});
    
});


//dataTable
jQuery(function($){
    if (jQuery().dataTable) {
        $("table.datatable").dataTable({
            "aaSorting": [],
            "bJQueryUI": true,
            "oLanguage": {
                "sProcessing":   "Подождите...",
                "sLengthMenu":   "Показать _MENU_ записей",
                "sZeroRecords":  "Записи отсутствуют.",
                "sInfo":         "Записи с _START_ до _END_ из _TOTAL_ записей",
                "sInfoEmpty":    "Записи с 0 до 0 из 0 записей",
                "sInfoFiltered": "(отфильтровано из _MAX_ записей)",
                "sInfoPostFix":  "",
                "sSearch":       "Поиск:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst": "Первая",
                    "sPrevious": "Предыдущая",
                    "sNext": "Следующая",
                    "sLast": "Последняя"
                }
            }
        });
    }
});


//CKEditor
function CKEditorInit(id) {
    CKEDITOR.replace( id,
    {
        width : '99%',
        toolbar : [
        ['Source','-','Preview','-','Templates'],
        ['Cut','Copy','Paste','PasteText','PasteWord','-','Undo','Redo'],
        ['CheckSpell','Scayt'],
        ['Bold','Italic','Underline','Strike','Subscript','Superscript'],
        ['TextColor'],
        ['Maximize','ShowBlocks'],
        '/',
        ['Numberedlist ','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink','Anchor','Image','Flash','Table','HorizontalRule','SpecialChar'],
        ['Font'],['FontSize'],
        ],
        removePlugins : 'resize'
    });
}
