<?php
$pageName = Yii::t('admin', 'Редактирование тип дома');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => Yii::t('admin', 'Тип дома'),
        'url' => array('/admin/types/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. Yii::t('admin', 'Редактирование тип дома'),
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>