<?php
$pageName = Yii::t('admin', 'Создать тип дома');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Тип дома',
        'url' => array('/admin/types/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать тип дома',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>