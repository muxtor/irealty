<?php
$pageName = ArticleServiceController::$typeName;

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => $pageName,
        'url' => array('/admin/articleService/index', 'type' => $model->type),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Редактирование',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>