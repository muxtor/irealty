<?php
/* @var $this ArticleServiceController */
/* @var $model ArticleService */

$this->breadcrumbs=array(
	'Article Services'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List ArticleService', 'url'=>array('index')),
	array('label'=>'Create ArticleService', 'url'=>array('create')),
	array('label'=>'Update ArticleService', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ArticleService', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ArticleService', 'url'=>array('admin')),
);
?>

<h1>View ArticleService #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'title',
		'url',
		'text',
		'created_on',
		'author_info',
		'meta_title',
		'meta_description',
		'meta_keywords',
		'print_on_indexpage',
	),
)); ?>
