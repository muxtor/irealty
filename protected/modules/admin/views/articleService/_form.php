<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i><?= Yii::t('admin', 'Page'); ?></span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/articleService/index', array('type' => $model->type)); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>

                <div class="uploadForm">
                    <form id="formUploadFiles" enctype="multipart/form-data" action="<?= Yii::app()->createUrl('/admin/articleService/uploadFiles'); ?>" method="POST">
                        <div class="new_advert_block">
                            <div class="fileform">
                                <div id="fileformlabel1">выбрать фотографии</div>
                                <div class="selectbutton"></div>

                                <input type="file" multiple="true" name="image[]" id="upload1" onchange="this.value,1">

                            </div>

                        </div>

                        <div class="progress-bar orange shine">
                            <span style="width: 0%"></span>
                        </div>


                        <div id="status">
                            <?php
                            if (Yii::app()->user->hasState('uploadedFiles')) {
                                $arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

                                foreach ($arrayFiles as $file) {
                                    echo "<div class='file'>" .
                                        CHtml::image(Yii::app()->baseUrl . ArticleService::THUMB_FOLDER.$file, '', array('style' => 'width:100px')) .

                                        "<br><a class='deletePhoto' href='javascript:void()' data='" . $file . "'>Удалить</a></div>";
                                }

                            }
                            if ($model->images) {

                                foreach ($model->images as $file) {
                                    echo "<div class='file'>" .
                                        CHtml::image(Yii::app()->baseUrl . ArticleService::THUMB_FOLDER.$file->file, '', array('style' => 'width:100px')) .

                                        "<br><a class='deletePhoto' href='javascript:void()' data='" . $file->file . "'>Удалить</a></div>";
                                }

                            }
                            ?>
                        </div>
                    </form>
                </div>
                <div style="clear:both"></div>

                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'article-service-form',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'focus' => array($model, 'alias'),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);?>
                                <?php
                                echo $form->checkBoxRow($model, 'print_on_indexpage');

                                echo $form->textFieldRow($model, 'title', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'url', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'meta_title', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'meta_keywords', array('class' => 'span12'));

                                echo $form->textFieldRow($model, 'meta_description', array('class' => 'span12'));

                                Yii::import('ext.redactor.RedactorWidget');
                                $this->widget(
                                    'RedactorWidget',
                                    array(
                                        'model' => $model,
                                        'attribute' => 'text',
                                    )
                                );

                                $this->widget(
                                    'RedactorWidget',
                                    array(
                                        'model' => $model,
                                        'attribute' => 'author_info',
                                    )
                                );
                                ?>
                    </div>


                </div>


                <!--end paped -->
                <div class="form-actions">
                    <div class="pull-right">
                        <?php
                        echo CHtml::link(
                            '<span class="icon-circle-arrow-left"></span> ' . Yii::t('admin', 'Return'),
                            Yii::app()->createUrl('/admin/articleService/index', array('type' => $model->type)),
                            array(
                                'class' => 'link'
                            )
                        );
                        ?>
                        <?php
                        $this->widget(
                            'bootstrap.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'label' => Yii::t('admin', 'Save'),
                                'type' => null,
                                'htmlOptions' => array(
                                    'class' => 'btn btn-default',
                                    'value' => 'index',
                                    'name' => 'submit',
                                ),
                                'size' => 'small',
                            )
                        );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->