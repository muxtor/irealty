<?php
$pageName = Yii::t('admin', 'Редактирование район');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => Yii::t('admin', 'Районы'),
        'url' => array('/admin/rayons/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. Yii::t('admin', 'Редактирование район'),
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>