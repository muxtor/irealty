<?php
$pageName = Yii::t('admin', 'Создать район');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Районы',
        'url' => array('/admin/rayons/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать район',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>