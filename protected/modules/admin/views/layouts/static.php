<?php
cs()->registerCssFile($this->assetsurl . '/css/global.css');
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <link rel="stylesheet" href="<?= $this->assetsurl; ?>/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet"/>
    <!--[if IE 7]>
      <link rel="stylesheet" href="<?= $this->assetsurl; ?>/css/font-awesome-ie7.min.css">
    <![endif]-->
</head>
<body>
    <?php echo $content; ?>
</body>
</html>
