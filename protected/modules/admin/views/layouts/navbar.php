<div class="navbar navbar-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="<?=$this->createUrl('/admin/default/index')?>"> Панель управления</a>

            <ul class="nav pull-right">
                <li class="toggle-primary-sidebar hidden-desktop" data-toggle="collapse"
                    data-target=".nav-collapse-primary"><a><i class="icon-th-list"></i></a></li>
                <li class="collapsed hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top"><a><i
                            class="icon-align-justify"></i></a></li>
            </ul>
            <div class="nav-collapse nav-collapse-top">
                <ul class="nav full pull-right">
                    <li class="dropdown user-avatar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span>
                                <?php /*/ ?>
                                <?php echo CHtml::image(
                                    Yii::app()->user->getAvatar(),
                                    Yii::app()->user->getShortName(),
                                    array('class' => 'menu-avatar')
                                ); ?>
                                <?php /*/ ?>
                                <span><?php echo Yii::app()->user->name; ?>  <i
                                        class="icon-caret-down"></i></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="with-image">
                                <div class="avatar">
                                <?php /*/ ?>
                                    <?php echo CHtml::image(
                                        Yii::app()->user->getAvatar(),
                                        Yii::app()->user->getShortName()
                                    ); ?>
                                <?php /*/ ?>
                                </div>
                                <span><?php echo Yii::app()->user->name; ?></span>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo Yii::app()->createUrl('/admin/default/logout'); ?>"><i
                                        class="icon-off"></i>
                                    <span>Выйти</span>
                                </a>
                            </li>
                        </ul>
                        <?php /*
                  <ul class="nav pull-right">

                  <li class="active" ><a id="comments-notifier" href="#"><i class="icon-comments"></i> <?=$comments['count']; ?></a>
                  <div class="comments-containner">

                  <div class="comments-containner-list hide search-dropdown box">
                  </div>
                  </div>
                  </li>
                  <li class="active"><a rel="tooltip" data-placement="bottom" data-original-title="Баланс системы"  href="#" title="Баланс системы"><i class="icon-money"></i> <?= $balanc['current_balanc'] . ' ' . Yii::t('common', 'RUB'); ?></a></li>
                  <li class="active"><a rel="tooltip" data-placement="bottom" data-original-title="Активных пользователей" href="#" title="Активных пользователей"><i class="icon-user"></i> <?= count($participant_actives); ?></a></li>
                  <li class="active"><a rel="tooltip" data-placement="bottom" data-original-title="Новых заявок" href="<?= Yii::app()->createUrl('/admin/application/index') ?>"  title="Новых заявок"><i class="icon-book"></i> <?= count($applications_new); ?></a></li>
                  <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Быстрый доступ <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                  <li><a href="<?= Yii::app()->createUrl('/admin/participant/agentСreate'); ?>">Создать агента</a></li>
                  <li><a href="<?= Yii::app()->createUrl('/admin/participant/agents'); ?>">Список агентов</a></li>
                  <li><a href="<?= Yii::app()->createUrl('/admin/application/index'); ?>">Заявки</a></li>
                  <li><a href="<?= Yii::app()->createUrl('/admin/finance/history'); ?>">Фин. история</a></li>
                  </ul>
                  </li>
                  </ul>
                 */ ?>
            </div>
        </div>
    </div>
</div>

