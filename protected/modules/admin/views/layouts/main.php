<?php
    /**
     * Основной Layout админ части приложения.
     *
     * POS_HEAD
     * POS_BEGIN
     * POS_END
     *
     * POS_READY
     * POS_LOAD
     *
     */
    $themePath = $this->assetsurl;
    cs()->registerCoreScript('jquery');
    cs()->registerScriptFile('/lib/jquery-ui/js/jquery-ui.min.js');
    cs()->registerCssFile('/lib/jquery-ui/css/jquery-ui.min.css');
    cs()->registerCssFile($themePath . '/css/global.css');

    // cs()->registerScriptFile('/js/jquery-1.11.2.min.js');
    Yii::app()->clientScript->registerScript('basePath', "basePath = '" . Yii::app()->getRequest()->getHostInfo() . "';", CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScript('loading', ' 
        $(".ajax-loading").bind("ajaxSend", function(){ 
            $(this).show(); 
        }).bind("ajaxComplete", function(){ 
            $(this).hide(); 
        }); 
    ', CClientScript::POS_READY);

    //Подключаем виджет для вывода сообщений пользователям

    $this->widget('bootstrap.widgets.Notify', array(
        'position' => 'top-right',
    ));
?>
<!DOCTYPE html>
<html lang="<?= Yii::app()->getLanguage(); ?>">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<!--  
    _____       __      __
   / ____|      \ \    / /
  | |  __ _ __ __\ \  / / 
  | | |_ | '_ ` _ \ \/ /  
  | |__| | | | | | \  /   
   \_____|_| |_| |_|\/    
                          
 ##########################                                                                          
        © 2014 #GmV# 
-->
<meta name="author" content="#GmV#"/>
<meta name="contact" content="r6@gmv.kz"/>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link rel="stylesheet" href="<?= $themePath; ?>/css/font-awesome.min.css"/>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet"/>
<link rel="shortcut icon" href="/favicon.ico" />

<!--[if IE 7]>
<link rel="stylesheet" href="<?=$themePath; ?>/css/font-awesome-ie7.min.css">
<![endif]-->
<!--<script src="<?/*=$themePath;*/?>/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript" charset="utf-8"></script>-->
<!-- <script src="<?=$themePath;?>/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script> -->
<!--<link rel="stylesheet" href="<?/*=$themePath; */?>/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8">-->

<?php Yii::app()->clientScript->registerScriptFile($themePath . '/js/main.js', CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerScriptFile($themePath . '/js/image.js', CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerScriptFile($themePath . '/js/jquery.form.js', CClientScript::POS_END);?>
<?php /*/ ?>
<!-- <script src="<?=$themePath;?>/js/jquery-1.11.1.js" type="text/javascript"></script> -->
<!-- <link rel="stylesheet" href="<?=$themePath; ?>/css/smoothness/jquery-ui.css" type="text/css" media="screen" charset="utf-8"> -->
<?php /*/ ?>
</head>
<body>
    <?php $this->renderPartial('/layouts/navbar'); ?>
    <?php $this->renderPartial('/layouts/sidebar'); ?>

    <div class="main-content">
        <div class='notifications top-right'></div>
        <?php $this->renderPartial('/layouts/header_info'); ?>
        <?php echo $content; ?>
    </div>
    <?php $this->renderClip('modal'); ?>
</body>
</html>
