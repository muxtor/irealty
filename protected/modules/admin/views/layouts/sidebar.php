<?php if ($this->beginCache('adminSidebar', array('duration' => 31536000))) { // храним год?>
    <div class="sidebar-background">
        <div class="primary-sidebar-background"></div>
    </div>
    <div class="primary-sidebar">
        <?php
        //main left menu
        $items = array(
            array('label' => 'Главная', 'url' => array('/admin/default/index'), 'icon' => 'icon-home icon-2x'),
            array('label' => 'Страницы', 'url' => array('/admin/page/index'), 'icon' => 'icon-file-alt icon-2x'),
            array('label' => 'Продажа', 'url' => array('/admin/realty/index'), 'icon' => 'icon-file-alt icon-2x'),

            array('label' => Yii::t('admin', 'Баннеры'), 'url' => array('/admin/banners/index'), 'icon' => 'icon-folder-close-alt icon-2x'),

            ///array('label' => 'Статьи', 'url' => array('/admin/articleService/index', 'type' => ArticleService::CONTENT_ARTICLE), 'icon' => 'icon-book icon-2x'),
            ///array('label' => 'Услуги', 'url' => array('/admin/articleService/index', 'type' => ArticleService::CONTENT_SERVICE), 'icon' => 'icon-tasks icon-2x'),

			///array('label' => 'Проекты', 'url' => array('/admin/project/index'), 'icon' => 'icon-shopping-cart icon-2x'),

			array('label' => Yii::t('admin', 'Параметры продажа'), 'url' => '#sellparams', 'icon' => 'icon-folder-open icon-2x',
				'submenuOptions' => array('class' => 'collapse', 'id' => 'sellparams'),
				'items' => array(
					array('label' => Yii::t('admin', 'Тип дома'),
						'url' => array('/admin/types/index'),
						'icon' => 'icon-inbox',
					),
                    array('label' => Yii::t('admin', 'Комнаты'),
                        'url' => array('/admin/rooms/index'),
                        'icon' => 'icon-inbox',
                    ),
                    array('label' => Yii::t('admin', 'Районы'),
                        'url' => array('/admin/rayons/index'),
                        'icon' => 'icon-inbox',
                    ),

				),
				//'visible' => UserAccess::is('subscribtion')
			),


            array('label' => 'Тарифы', 'url' => array('/admin/tariffs/index'), 'icon' => 'icon-file-alt icon-2x'),

            array('label' => 'Пользователи', 'url' => array('/admin/user/index'), 'icon' => 'icon-user icon-2x'),
            array('label' => 'Бан номеры', 'url' => array('/admin/ban/index'), 'icon' => 'icon-ban-circle icon-2x'),

			// array('label' => Yii::t('admin', 'Slider'), 'url' => array('/admin/slider/index'), 'icon' => 'icon-film icon-2x', 'visible' => UserAccess::is('slider')),
			
			array('label' => 'Настройки', 'url' => array('/admin/setting/update'), 'icon' => 'icon-cog icon-2x'),
        );

        $this->widget('core.components.CoreMenu', array(
            'items' => $items,
            'htmlOptions' => array(
                'class' => 'nav nav-collapse collapse nav-collapse-primary'
            ),
            'linkLabelWrapper' => 'span',
            'activateItems' => true,
            'activateParents' => true,
            'submenuHtmlOptions' => array('class' => 'collapse'),
            'itemTemplate' => "<span class=\"glow\"></span>{menu}"
        ));

        ?>
    </div>

    <?php $this->endCache();
} ?>