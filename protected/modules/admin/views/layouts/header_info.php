<!-- HEADER INFO -->
<div class="container-fluid">
    <div class="row-fluid">
        <?php if (isset($this->header_info) && count($this->header_info) > 0): ?>
            <div class="area-top clearfix">
                <div class="pull-left header">
                    <h3 class="title">
                        <?php if (isset($this->header_info['icon']) && !empty($this->header_info['icon'])): ?>
                            <i class="<?= $this->header_info['icon']; ?>"></i>
                        <?php endif; ?>
                        <?php if (isset($this->header_info['title']) && !empty($this->header_info['title'])): ?>
                            <?= $this->header_info['title']; ?>
                        <?php endif; ?>
                    </h3>
                    <?php if (isset($this->header_info['description']) && !empty($this->header_info['description'])): ?>
                        <h5>
                            <?= $this->header_info['description']; ?>
                        </h5>
                    <?php endif; ?>
                </div>
        <ul class="inline pull-right sparkline-box">
          <li class="sparkline-row">
              <div class="ajax-loading"></div>
          </li>
        </ul>
            </div>
        <?php endif; ?>
    </div>
</div>   
<!-- END HEADER INFO -->

<!-- BREDCRUMBS -->
<?php if (count($this->breadcrumbs) > 0): ?>
    <div class="container-fluid padded">
        <div class="row-fluid">
            <div id="breadcrumbs">
                <?php
                $this->widget('core.components.Breadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'tagName' => 'div',
                    //'htmlOptions' => array('class'=>'breadcrumb-button'),
                    'separator' => '',
                    'home' => array(
                        'icon' => 'icon-home',
                        'label' => 'Главная',
                        'url' => array('/admin/default/index'),
                    ),
                        //                'inactiveLinkTemplate'=>'<a>{label}</a>',
                        ///                  'homeLink' .=> CHtml::link('Все ресторанны', Yii::app()->homeUrl),
                ));
                ?>   
            </div>

        </div>
    </div>  
<?php endif; ?>
<!-- BREDCRUMBS -->
