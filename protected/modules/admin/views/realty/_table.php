<?php

$csrfTokenName = Yii::app()->request->csrfTokenName;
$csrfToken = Yii::app()->request->csrfToken;
$csrf = "'$csrfTokenName':'$csrfToken'";

Yii::app()->clientScript->registerScript('multidelete', '
    function multiDelete(values){
    $elements = [];    
    $.each(values,function(i, val){
        $elements.push($(val).val());
    });
    $.ajax({
        url:"' . $this->createUrl('/admin/realty/MultipleRemove') . '",
        data:{data:JSON.stringify($elements), ' . $csrf . '},
        dataType:"json",
        type:"POST",
        success:function(data){
            if (data.response.status=="success"){        
                $.fn.yiiGridView.update(\'pages-list\');
                    $(".top-right").notify({
                        type:"bangTidy",
                        fadeOut:{enabled: true, delay: 3000 },
                        transition:"fade",                                                                                 
                        message: { text: data.response.data.messages }
                    }).show();

                } else {
                    $(".top-right").notify({
                        type:"bangTidy",
                        fadeOut:{enabled: true, delay: 3000 },
                        transition:"fade",                                                                                 
                        message: { text: data.response.data.messages }
                    }).show();
                }
        }
    });
    }
');




$this->widget('bootstrap.widgets.TTbExtendedGridView', array(
    //'type' => 'striped bordered condensed',
    'id' => 'pages-list',
    //'enableSorting' => false,
    //'itemsCssClass' => 'table-normal table-hover-row',
    'dataProvider' => $model->search(),
    'ajaxUrl' => array('/admin/realty/index'),
    'filter' => $model,
    'summaryText' => Yii::t('admin', 'Oбъявление - Продажа').' '. Yii::t('admin', '{start}—{end} from <span>{count}</span>.'),

    'columns' => array(
        array(
            'name' => 'id',
            'value' => '$data->id',
            'type' => 'raw',
            'htmlOptions' => array('width' => '30px'),
        ),
        array(
            'name' => 'phone',
            'value' => '$data->phone',
            'htmlOptions' => array('width' => '150px'),
        ),
        array(
            'name' => 'rooms',
            'value' => '$data->rooms',
            'type' => 'raw',
             'htmlOptions' => array('width' => '150px'),
        ),
        array(
            'name' => 'price',
            'value' => '$data->price."руб"',
        ),
        array(
            'name' => 'rayons.rayon',
            //'value' => '$data->rayon',
        ),
        array(
            'name' => 'street',
            'value' => '$data->street',
        ),
        array(
            'name' => 'home',
            'value' => '$data->home',
        ),
        array(
            'type' => 'raw',
            'name' => 'vip',
            'value' => '$data->getVipForTable()',
            'htmlOptions' => array('width' => '50px'),
        ),
        array(
            'header'=>'Горячая?',
            'type' => 'raw',
            'name' => 'hot',
            'value' => '$data->getHotForTable()',
            'htmlOptions' => array('width' => '60px'),
        ),
        array(
            'type' => 'raw',
            'name' => 'showphone',
            'value' => '$data->getShowphoneForTable()',
            'htmlOptions' => array('width' => '120px'),
        ),
        array(
            'type' => 'raw',
            'name' => 'status',
            'value' => '$data->getStatusForTable()',
            'htmlOptions' => array('width' => '80px'),
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            //'header' => 'Действия',
            'class' => 'bootstrap.widgets.TTbButtonColumn',
            'template' => '{update}  {delete}',
            'afterDelete' => 'function(link,success,data){
                            data =  $.parseJSON(data);
                            if(data.response.status=="success"){
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                                                 
                                    message: { text: data.response.data.messages }
                                }).show();
                            }else{
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                 
                                    message: { text: data.response.data.messages }
                                }).show();

                        }
                        }',
        )
    )
));
