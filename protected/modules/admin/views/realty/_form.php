<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i><?= Yii::t('admin', 'Продажа'); ?></span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/realty/index'); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="uploadForm">
                    <br/>
                    <form id="formUploadFiles" enctype="multipart/form-data"
                          action="<?= Yii::app()->createUrl('/realty/uploadFiles'); ?>" method="POST">
                        <div class="new_advert_block">
                            <div class="fileform" style="position: relative; text-align: center; margin-bottom: 15px;">
                                <input style="opacity: 0; left:0; position: absolute; width:100%; cursor: pointer;" type="file" multiple="true" name="image[]" id="upload1" onchange="this.value,1">
                                <div id="fileformlabel1" ><span style="color:#0771a2; border-bottom: dotted 1px #0771a2; font-size:14px;">Загрузить изображение</span></div>
                                <div class="selectbutton"></div>
                            </div>
                        </div>

                        <div class="progress-bar orange shine">
                            <span style="width: 0%"></span>
                        </div>

                        <div id="status">
                            <?php
                            if (Yii::app()->user->hasState('uploadedFiles')) {
                                $arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

                                foreach ($arrayFiles as $file) {
                                    echo "<div class='file'>" .
                                        CHtml::image(Yii::app()->baseUrl . Realty::THUMB_FOLDER . $file, '', array('style' => 'width:100px')) .

                                        "<br><a class='deletePhoto' href='javascript:void()' data='" . $file . "'>Удалить</a></div>";
                                }

                            }
                            if ($model->images) {

                                foreach ($model->images as $file) {
                                    echo "<div class='file'>" .
                                        CHtml::image(Yii::app()->baseUrl . Realty::THUMB_FOLDER . $file->file, '', array('style' => 'width:100px')) .

                                        "<br><a class='deletePhoto' href='javascript:void()' data='" . $file->file . "'>Удалить</a></div>";
                                }

                            }
                            ?>
                        </div>
                        <div style="clear:both"></div>
                    </form>
                    <div style="clear:both"></div>
                </div>
                <div style="clear:both"></div>

                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-pages',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded" style="width: 450px;">
                        <?php echo $form->errorSummary($model);?>
                        <div class="w50p">
                            <div style="margin-left: 32px; float:left;">
                                <?php
                                echo $form->dropDownList($model, 'rooms', array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9,),
                                    array('class' => 'span12', 'empty' => 'Сколько комнат?'));
                                ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->dropDownList($model, 'type', CHtml::listData(RealtyType::model()->findAll(), 'id_type', 'type'), array('class' => 'span12', 'empty' => 'Тип дома')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->dropDownList($model, 'rayon', CHtml::listData(Rayon::model()->findAll(), 'id_rayon', 'rayon'), array('class' => 'span12', 'empty' => 'Район')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->textField($model, 'street', array('class' => 'span12', 'placeholder' => 'Улица')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                            </div>
                        </div>
                        <div class="w50p">
                            <div style="margin-right: 32px; float:right;">
                                <?php echo $form->textField($model, 'home', array('class' => 'span12', 'placeholder' => 'Дом')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->textField($model, 'korpus', array('class' => 'span12', 'placeholder' => 'Корпус')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->textField($model, 'etaj', array('class' => 'span12', 'placeholder' => 'Этаж')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->textField($model, 'kvartira', array('class' => 'span12', 'placeholder' => 'Квартира')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                                <?php echo $form->textField($model, 'year', array('class' => 'span12', 'placeholder' => 'Год постройки')); ?>
                                <div class="clear clearfix" style="margin-bottom:19px;"></div>
                            </div>
                        </div>
                        <div class="clear clearfix"></div>
                        <div style="border-bottom: 1px solid #d9d9d9;"></div>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        <div class="w50p">
                            <div style="margin-left: 32px; float:left;">
                                <div class="param-title">Цена</div>
                                <?php echo $form->textField($model, 'price', array('class' => 'span12')); ?>
                                <div class="range-metter" style="margin-top:10px;"></div>
                                <div id="price"></div>
                            </div>
                        </div>
                        <div class="w50p">
                            <div style="margin-right: 32px; float:right;">
                                <div class="param-title">Площадь</div>
                                <?php echo $form->textField($model, 'ploshad', array('class' => 'span12')); ?>
                                <div class="range-metter" style="margin-top:10px;"></div>
                                <div id="ploshad"></div>
                            </div>
                        </div>
                        <div class="clear clearfix"></div>
                        <div style="border-bottom: 1px solid #d9d9d9;"></div>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        <div class="phone" style="text-align: center;">
                            <?php echo $form->textField($model, 'phone', array('class' => 'span12', 'placeholder' => 'Ваш номер', 'style'=>'width:300px;')); ?>
                            <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        </div>
                        <div class="phone">
                            <?php echo $form->checkBoxRow($model, 'vip', array('class' => '')); ?>
                            <?php echo $form->checkBoxRow($model, 'hot', array('class' => '')); ?>
                            <?php echo $form->checkBoxRow($model, 'showphone', array('class' => '')); ?>
                            <?php echo $form->checkBoxRow($model, 'status', array('class' => '')); ?>
                            <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        </div>
                    </div>

                    <div>
                        <label>Адрес для поиска: </label><input id="address" style="width:600px;" type="text"/>
                        <div id="map_canvas" style="width:100%; height:400px"></div><br/>
                        <label>Широта (latitude): </label><?php echo $form->textField($model, 'lat', array('class' => 'span12')); ?><br/>
                        <label>Долгота (longitude): </label><?php echo $form->textField($model, 'lng', array('class' => 'span12')); ?>
                    </div>


                </div>


                <!--end paped -->
                <div class="form-actions">
                    <div class="pull-right">
                        <?php
                        echo CHtml::link(
                            '<span class="icon-circle-arrow-left"></span> ' . Yii::t('admin', 'Return'),
                            Yii::app()->createUrl('/admin/realty/index'),
                            array(
                                'class' => 'link'
                            )
                        );
                        ?>
                        <?php
                        $this->widget(
                            'bootstrap.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'label' => Yii::t('admin', 'Save'),
                                'type' => null,
                                'htmlOptions' => array(
                                    'class' => 'btn btn-default',
                                    'value' => 'index',
                                    'name' => 'submit',
                                ),
                                'size' => 'small',
                            )
                        );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <div style="clear:both"></div>
                <!-- end box content -->


            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>

<script type="text/javascript">

    var geocoder;
    var map;
    var marker;

    function initialize(){
//Определение карты
        var latlng = new google.maps.LatLng(62.03333299999999, 129.73333300000002);
        <?php if(!empty($model->lat) AND !empty($model->lng)){?>
            var latlng = new google.maps.LatLng(<?php echo $model->lat;?>,<?php echo $model->lng;?>);
        <?php } ?>

        var options = {
            zoom: 15,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), options);

        //Определение геокодера
        geocoder = new google.maps.Geocoder();

        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: true,
            title:"Маркер!"
        });

    }

    $(document).ready(function() {

        initialize();

        $(function() {
            $("#address").autocomplete({
                //Определяем значение для адреса при геокодировании
                source: function(request, response) {
                    geocoder.geocode( {'address': request.term}, function(results, status) {
                        response($.map(results, function(item) {
                            return {
                                label:  item.formatted_address,
                                value: item.formatted_address,
                                latitude: item.geometry.location.lat(),
                                longitude: item.geometry.location.lng()
                            }
                        }));
                    })
                },
                //Выполняется при выборе конкретного адреса
                select: function(event, ui) {
                    $("#Realty_lat").val(ui.item.latitude);
                    $("#Realty_lng").val(ui.item.longitude);
                    var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                    marker.setPosition(location);
                    map.setCenter(location);
                }
            });
        });

        //Добавляем слушателя события обратного геокодирования для маркера при его перемещении
        google.maps.event.addListener(marker, 'drag', function() {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#Realty_lat').val(marker.getPosition().lat());
                        $('#Realty_lng').val(marker.getPosition().lng());
                    }
                }
            });
        });

    });


</script>
