<?php
$pageName = Yii::t('admin', 'Создать объявления');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Oбъявление - Продажа',
        'url' => array('/admin/realty/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать объявления на продожу',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>