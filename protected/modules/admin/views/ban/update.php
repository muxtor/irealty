<?php
$pageName = Yii::t('admin', 'Редактировать бан номера');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => Yii::t('admin', 'Бан номеры'),
        'url' => array('/admin/ban/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. Yii::t('admin', 'Редактирование бан номера'),
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>