<?php
$pageName = Yii::t('admin', 'Бан номер');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Бан номеры',
        'url' => array('/admin/ban/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Бан номер',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>