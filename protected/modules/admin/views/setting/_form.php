<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i>Настройки</span>
                </div>

                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-setting',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'focus' => array($model, 'alias'),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);?>
                                <?php

                                echo $form->textFieldRow($model, 'email_admin', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'link_vk', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'link_fb', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'link_ok', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'link_tw', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'site_title', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'meta_keywords', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'meta_descriptions', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'feedback_email', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'country_code', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'phone_1', array('class' => 'span12'));
                                echo $form->textFieldRow($model, 'phone_2', array('class' => 'span12'));

                                ?>
                    </div>


                </div>


                <!--end paped -->
                <div class="form-actions">
                    <div class="pull-right">
                        <?php
                        $this->widget(
                            'bootstrap.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'label' => 'Сохранить',
                                'type' => null,
                                'htmlOptions' => array(
                                    'class' => 'btn btn-default',
                                    'value' => 'index',
                                    'name' => 'submit',
                                ),
                                'size' => 'small',
                            )
                        );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->