<?php
/* @var $this SettingController */
/* @var $data Setting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feedback_email')); ?>:</b>
	<?php echo CHtml::encode($data->feedback_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country_code')); ?>:</b>
	<?php echo CHtml::encode($data->country_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_1')); ?>:</b>
	<?php echo CHtml::encode($data->phone_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_2')); ?>:</b>
	<?php echo CHtml::encode($data->phone_2); ?>
	<br />


</div>