<?php
$pageName = Yii::t('admin', 'Settings');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-edit',
        'label' => ' Настройки',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));
