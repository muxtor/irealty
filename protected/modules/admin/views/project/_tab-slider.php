        <div class="sliders">
            <?php $k = 0;
                if ($model->sliders): ?>
                <?php foreach ($model->sliders as $slider_id => $slider): ?>
                    <div class='slider-container'>
                        <?php foreach ($slider as $image): ?>
                            <?php if ($image['slide_order'] == 1): ?>
                               <img src="<?=Project::SLIDER_THUMB_PATH.$image['file']?>" width="120" class="image_1">
                               <div class="horizontal-images">
                            <?php else: ?>
                                <img src="<?=Project::SLIDER_THUMB_PATH.$image['file']?>" width="100" class="image_<?=$k + 2?>">
                                <?php ++$k; ?>
                            <?php endif; ?>
                            <?php if ($k == 2): ?>
                               </div>
                            <?php $k = 0; endif; ?>
                        <?php endforeach; ?>
                        <input type="file" name="image_1" style="display: none;">
                        <input type="file" name="image_2" style="display: none;">
                        <input type="file" name="image_3" style="display: none;">
                        <div class="clear"></div>
                        <div class="btn-group">
                            <a data-id="<?=$slider_id?>" class="removeSlider btn btn-danger btn-small" href="#">Удалить</a>
                        </div> 
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="slider-container new">
                <img src="/img/379x422.jpg" width="120" height="134" data-input-id="image1">
                <div class="horizontal-images">
                    <img src="/img/319x220.jpg" width="100" data-input-id="image2">
                    <img src="/img/319x197.jpg" width="100" data-input-id="image3">
                </div>
                <input type="file" name="image[1]" id="image1" data-status="new" data-number="1" style="display: none;">
                <input type="file" name="image[2]" id="image2" data-status="new" data-number="2" style="display: none;">
                <input type="file" name="image[3]" id="image3" data-status="new" data-number="3" style="display: none;">
                <div class="clear"></div>
                <div class="btn-group">
                    <a id="addSlider" data-id="new" class="btn btn-default btn-small" href="#">Добавить</a>
                </div>
            </div>
        </div>
    </form>
</div>
<div style="clear:both"></div>
<style type="text/css">
    .slider-container {
        float: left;
        width: 223px;
        margin: 10px;
        padding: 6px;
        outline: 1px solid #ccc;
        border-radius: 3px;
    }
    .slider-container.new img {
        cursor: pointer;
    }
    .slider-container > img {
        float: left;
    }
    .horizontal-images {
        margin-left: 3px;
        float: left;
    }
    .horizontal-images img {
        display: block;
    }
    .horizontal-images img:first-child {
        margin-bottom: 3px;
    }
    .slider-container .btn-group {
        margin-top: 10px;
        padding-left: 3px;
    }
</style>
<?php 
    Yii::app()->clientScript->registerScript('uploader',
<<<EOF
$(document).on('click', '.slider-container.new img', function(e) {
    var id = $(this).attr('data-input-id');
    $('#'+id).trigger('click');
    e.preventDefault();
});
$(document).on('change', '.slider-container.new input[type="file"]', function(e) {
    var input_name = $(this).attr('name');
    var image_number = $(this).attr('data-number');
    var data = new FormData();
    data.append(input_name, $(this)[0].files[0]);
    
    $.ajax({
        url: '/admin/project/uploadFiles?id='+image_number,
        type: 'post',
        data: data,
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.success) {
                $('.slider-container.new img[data-input-id="image'+image_number+'"]').attr('src', data.thumb);
                $('.slider-container.new img[data-input-id="image'+image_number+'"]').attr('data-filename', data.file_name);
                $('.slider-container.new input[data-number="'+image_number+'"]').attr('data-status', 'uploaded');
            } else
                alert('wrong sizes!');
        }
    });
});

$(document).on('click', '#addSlider', function(e) {
    if ($('.slider-container.new input[data-status="new"]').length > 0) {
        alert('Есть не загруженные изображения');
        return false;
    }
    var images = new Object();
    var i = 1;
    $('.slider-container.new img').each(function() {
        images[i] = new Object();
        images[i]['filename'] = $(this).attr('data-filename');
        ++i;
    });
    $('.slider-container.new .btn-group a').removeAttr('id');
    
    $.ajax({
        url: '/admin/project/addSlider',
        type: 'post',
        dataType: 'json',
        cache: false,
        data: { 'images': images, 'project_id': $model->id },
        success: function (data) {
            if (data.success) {
                var btn = $('.slider-container.new .btn-group a');
                btn.removeClass('btn-default addSlider');
                btn.addClass('btn-danger removeSlider');
                btn.text('Удалить');
                btn.attr('data-id', data.slider_id);
                btn.removeAttr('id');
                $('.slider-container.new input').remove();
                $('.slider-container.new').removeClass('new');
                alert('Слайдер успешно добавлен');

                var slider_container = $('<div/>', {
                    class:  'slider-container new',
                });

                slider_container.append('<img src="/img/379x422.jpg" width="120" height="134" data-input-id="image1">');
                slider_container.append(
                    '<div class="horizontal-images">' +
                        '<img src="/img/319x220.jpg" width="100" data-input-id="image2">' + 
                        '<img src="/img/319x197.jpg" width="100" data-input-id="image3">' +
                    '</div>'
                );
                slider_container.append('<input type="file" name="image[1]" id="image1" data-status="new" data-number="1" style="display: none;">');
                slider_container.append('<input type="file" name="image[2]" id="image2" data-status="new" data-number="2" style="display: none;">');
                slider_container.append('<input type="file" name="image[3]" id="image3" data-status="new" data-number="3" style="display: none;">');
                slider_container.append('<div class="clear"></div>');
                slider_container.append(
                    '<div class="btn-group">' +
                        '<a id="addSlider" data-id="new" class="btn btn-default btn-small" href="#">Добавить</a>' +
                    '</div>'
                );

                $('.sliders').append(slider_container).trigger('refresh');
            }
        }
    });
    e.preventDefault();
});
$(document).on('click', '.slider-container a.removeSlider', function(e) {
    if (!confirm('Вы действительно хотите удалить этот чудный слайдер?'))
        return false;

    var slider_id = $(this).attr('data-id');
    var self = $(this);

    $.ajax({
        url: '/admin/project/removeSlider',
        type: 'post',
        dataType: 'json',
        data: { 'id': slider_id },
        success: function (data) {
            if (data.success) {
                self.parents('.slider-container').remove();
            }
        }
    });

    e.preventDefault();
});
EOF
, CClientScript::POS_READY);