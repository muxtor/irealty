<?php
    // $provider = ($model->isNewRecord) ? $projects->search() : $projects->search($model->id);
    $provider = $projects->search($model->id);

    $this->widget(
        'bootstrap.widgets.TTbExtendedGridView',
        array(
            'id' => 'similar-projects',
            'dataProvider' => $provider,
            
            'enableSorting' => false,
            'type' => 'striped bordered condensed',
            'summaryText' => false,
            'columns' => array(
                'id',
                array(
                    'type' => 'raw',
                    'value' => '(!empty($data->main_top_image)) ? "<img src=".Project::IMAGE_PATH.$data->main_top_image." width=\"50\" />" : ""',
                    'htmlOptions' => array(
                        'style' => 'width: 50px; text-align: center',
                    ),
                ),
                'title',
                'adress',
                'url',
                array(
                    'name' => 'Добавлен',
                    'type' => 'raw',
                    'value' => function($data) use ($similarProjectIds) {
                      if (in_array((string)$data->id, $similarProjectIds))
                        return "<input class='addToSimilar' data-id='".$data->id."' type='checkbox' value='1' checked='checked'>";
                      else 
                        return "<input class='addToSimilar' data-id='".$data->id."' type='checkbox' value='0'>";  
                    },
                    'htmlOptions' => array(
                        'style' => 'width: 50px; text-align: center',
                    ),
                ),
            ),
        )
    );

Yii::app()->clientScript->registerScript(
    'addToSimilar',
<<<EOF
$('input.addToSimilar').click(function() {
    var value = $(this).val();
    var checked_project_id = $(this).attr('data-id');
    var current_project_id = $model->id;
    // if "checked" changes to 'disabled'
    if (value == 1) {
        var action = 'delete';
        $(this).val(0);
    } else {
        var action = 'add';
        $(this).val(1);
    }

    $.ajax({
       type: "POST",
       url: "/admin/project/changeAsSimilar",
       data: {"project_id": current_project_id, "checked_project_id": checked_project_id, "action": action},
       success: function(){
        
       }
    });
});
EOF
, CClientScript::POS_READY);