<div id="attributes-block">
    <?php if (!empty($model->specs)): ?>
        <?php foreach ($model->specs as $spec): ?>
            <div data-id="<?=$spec->id?>" class="attribute-field">
                <input class="col-md-3 attr-label" type="text" value="<?=CHtml::encode($spec->name)?>" placeholder="Аттрибут" /> : <input class="col-md-3 attr-value" type="text" value="<?=CHtml::encode($spec->value)?>" placeholder="Значение" /> <a href="#" class="remove-button"><span class="icon-remove"></span></a>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <div class="attribute-field new">
        <input class="col-md-3 attr-label" type="text" value="" placeholder="Название" /> : <input class="col-md-3 attr-value" type="text" value="" placeholder="Метраж" />
    </div>
    <button id="addAttribute" class="btn btn-default btn-small">Добавить аттрибут</button>
    <div class="errors"></div>
</div>
<hr>
<style>
    #attributes-block {
        position: relative;
        padding-bottom: 30px;
    }
    #addAttribute {
        position: absolute;
        bottom: 0;
    }
    .attributes {
        border: 1px solid #ccc;
        padding: 10px;
    }
    .attribute-field {
        margin-top: 10px;
    }
    .attributes .errors {
        display: none;
    }
    .errors {
        color: #FF5858;
    }
</style>
<?php 
Yii::app()->clientScript->registerScript('project-attributes', 
<<<EOF
    $('body').on('click', '.attribute-field:not(.new) a.remove-button', function() {
        var spec_id = $(this).parent().attr('data-id');
        var handler = $(this);

        if (confirm("Удалить этот аттрибут?")) {
            $.ajax({
                url: '/admin/projectSpec/delete?id='+spec_id,
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    handler.parent().remove();
                }
            });
        }

        return false;
    });

    $('body').on('change', '.attribute-field:not(.new) input.attr-label', function() {
        var spec_id = $(this).parent().attr('data-id');
        var label = $(this).val();
        
        if (label != '')
        {
            $.ajax({
                url: '/admin/projectSpec/update?id='+spec_id,
                type: 'POST',
                dataType: 'json',
                data: {'ProjectSpec[name]': label},
            })
            .done(function() {
            });
        }
    });

    $('body').on('change', '.attribute-field:not(.new) input.attr-value', function() {
        var spec_id = $(this).parent().attr('data-id');
        var value = $(this).val();
        
        if (value != '')
        {
            $.ajax({
                url: '/admin/projectSpec/update?id='+spec_id,
                type: 'POST',
                dataType: 'json',
                data: {'ProjectSpec[value]': value},
            })
            .done(function() {
            });
        }
    });

    $('#addAttribute').on('click', function() {
        var name = $('.attribute-field.new .attr-label').val();
        var value = $('.attribute-field.new .attr-value').val();
        
        if (name == '' || value == '') {
            $('.errors').text('Заполните оба поля');
            return false;
        }

        $('.errors').hide();
        
        $.ajax({
            url: '/admin/projectSpec/create',
            type: 'POST',
            dataType: 'json',
            data: {'ProjectSpec[project_id]' : $model->id, 'ProjectSpec[name]' : name, 'ProjectSpec[value]' : value}
        })
        .done(function(data) {
            var attr_id = data.model_id;
            
            $('.attribute-field.new').append(' <a href="#" class="remove-button"><span class="icon-remove"></span></a>');
            $('.attribute-field.new').attr('data-id', attr_id);
            $('.attribute-field.new').removeClass('new');
            
            var attr_label = '<input class="col-md-3 attr-label" type="text" value="" placeholder="Аттрибут"/> : ';
            var attr_value = '<input class="col-md-3 attr-value" type="text" value="" placeholder="Значение"/>';
            
            var attribute_field = '<div class="attribute-field new">'+attr_label + attr_value + '</div>';

            $('#attributes-block').append(attribute_field);
        });
        return false;
    });
EOF
, CClientScript::POS_END); ?>