<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i>Проекты</span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/project/index'); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>
                
                <?php

                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'project-form',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        // 'focus' => array($model, 'alias'),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>
                <div class="box-content">
                    <div class="padded">
                        <?php 
                            $tabs = array(
                                array('label' => 'Основное', 'content' => $this->renderPartial('_tab-main', array('model' => $model, 'form' => $form), true), 'active' => true)
                            );

                            if (!$model->isNewRecord) {
                                $tabs[] = array('label' => 'Интерьеры в этом стиле', 'content' => $this->renderPartial('_tab-similar-projects', array('model' => $model, 'projects' => $projects, 'similarProjectIds' => $similarProjectIds), true));
                                $tabs[] = array('label' => 'Слайдер', 'content' => $this->renderPartial('_tab-slider', array('model' => $model), true));
                            }

                            $this->widget(
                                'bootstrap.widgets.TbTabs',
                                array(
                                    'type' => 'tabs',
                                    'tabs' => $tabs
                                )
                            );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->