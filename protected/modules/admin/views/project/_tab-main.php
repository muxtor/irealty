<?php 
    if (!$model->isNewRecord)
        $this->renderPartial('_attributes-block', array('model'=>$model));
    
    echo $form->errorSummary($model);
    echo $form->checkBoxRow($model, 'print_on_indexpage');

    echo $form->fileFieldRow($model, 'main_top_image', array('class' => 'span4','hint' =>'Размер изображения должен быть не меннее чем 800 пикселей в ширину'));
    
    if (!$model->isNewRecord && !empty($model->main_top_image))
        echo "<img src='".Project::THUMB_PATH.$model->main_top_image."'/>";

    echo $form->fileFieldRow($model, 'indexpage_image', array('class' => 'span4','hint' =>'Размер изображения должен быть не меннее чем 1169x730 пикселей'));
    
    if (!$model->isNewRecord && !empty($model->indexpage_image))
        echo "<img src='".Project::THUMB_PATH.$model->indexpage_image."'/>";

    echo $form->textFieldRow($model, 'title', array('class' => 'span12'));
    
    echo $form->textFieldRow($model, 'adress', array('class' => 'span12'));

    echo $form->textFieldRow($model, 'url', array('class' => 'span12'));

    echo $form->textFieldRow($model, 'parameters', array('class' => 'span12'));
    
    echo $form->textFieldRow($model, 'realize_period', array('class' => 'span12'));

    echo $form->textFieldRow($model, 'meta_title', array('class' => 'span12'));

    echo $form->textFieldRow($model, 'meta_keywords', array('class' => 'span12'));

    echo $form->textFieldRow($model, 'meta_description', array('class' => 'span12'));

    Yii::import('ext.redactor.RedactorWidget');
    $this->widget(
        'RedactorWidget',
        array(
            'model' => $model,
            'attribute' => 'text',
        )
    );

    echo $form->textAreaRow($model, 'indexpage_desc1', array('class' => 'span12'));
    echo $form->textAreaRow($model, 'indexpage_desc2', array('class' => 'span12'));
?>
<div class="form-actions" style="background-color: #fff;">
    <div class="pull-right">
        <?php
        echo CHtml::link(
            '<span class="icon-circle-arrow-left"></span> ' . Yii::t('admin', 'Return'),
            Yii::app()->createUrl('/admin/project/index'),
            array(
                'class' => 'link'
            )
        );
        ?>
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'label' => Yii::t('admin', 'Save'),
                'type' => null,
                'htmlOptions' => array(
                    'class' => 'btn btn-default',
                    'value' => 'index',
                    'name' => 'submit',
                ),
                'size' => 'small',
            )
        );
        ?>
    </div>
</div>