<?php
/* @var $this ProjectController */
/* @var $data Project */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adress')); ?>:</b>
	<?php echo CHtml::encode($data->adress); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parameters')); ?>:</b>
	<?php echo CHtml::encode($data->parameters); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realize_period')); ?>:</b>
	<?php echo CHtml::encode($data->realize_period); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_top_image')); ?>:</b>
	<?php echo CHtml::encode($data->main_top_image); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('similar_projects')); ?>:</b>
	<?php echo CHtml::encode($data->similar_projects); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('indexpage_image')); ?>:</b>
	<?php echo CHtml::encode($data->indexpage_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('print_on_indexpage')); ?>:</b>
	<?php echo CHtml::encode($data->print_on_indexpage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_title')); ?>:</b>
	<?php echo CHtml::encode($data->meta_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_description')); ?>:</b>
	<?php echo CHtml::encode($data->meta_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meta_keywords')); ?>:</b>
	<?php echo CHtml::encode($data->meta_keywords); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('portfolio_image')); ?>:</b>
	<?php echo CHtml::encode($data->portfolio_image); ?>
	<br />

	*/ ?>

</div>