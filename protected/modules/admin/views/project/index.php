<?php
$pageName = 'Проекты';

$this->pageTitle = $pageName;
$this->header_info = array(
    'icon' => 'icon-check icon-2x',
    'title' => $pageName,
    'description' => $pageName,
);

$this->breadcrumbs = array(
    array(
        'icon' => 'icon-folder-open',
        'label' => $pageName,
        'url' => '',
    ),
);
?>
<div class="container-fluid padded">
    <div class="box">
        <div class="box-header">
            <span class="title"><?= $this->header_info['title'];?></span>
            <ul class="box-toolbar">
                <li><span class="label label-blue"><?= $model->search()->getTotalItemCount(); ?></span></li>
                <li><a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Add')?>" href="<?php echo Yii::app()->createUrl('/admin/project/create'); ?>"><i class="icon-plus"></i></a></li>
            </ul>
        </div>
        <div class="box-content">
            <?php
            $this->renderPartial('_table', array(
                'model' => $model
            ));
            ?>
        </div>
    </div>
</div>