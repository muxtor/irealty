<?php

$csrfTokenName = Yii::app()->request->csrfTokenName;
$csrfToken = Yii::app()->request->csrfToken;
$csrf = "'$csrfTokenName':'$csrfToken'";

Yii::app()->clientScript->registerScript('multidelete', '
    function multiDelete(values){
    $elements = [];    
    $.each(values,function(i, val){
        $elements.push($(val).val());
    });
    $.ajax({
        url:"' . $this->createUrl('/admin/tariffs/MultipleRemove') . '",
        data:{data:JSON.stringify($elements), ' . $csrf . '},
        dataType:"json",
        type:"POST",
        success:function(data){
            if (data.response.status=="success"){        
                $.fn.yiiGridView.update(\'pages-list\');
                    $(".top-right").notify({
                        type:"bangTidy",
                        fadeOut:{enabled: true, delay: 3000 },
                        transition:"fade",                                                                                 
                        message: { text: data.response.data.messages }
                    }).show();

                } else {
                    $(".top-right").notify({
                        type:"bangTidy",
                        fadeOut:{enabled: true, delay: 3000 },
                        transition:"fade",                                                                                 
                        message: { text: data.response.data.messages }
                    }).show();
                }
        }
    });
    }
');




$this->widget('bootstrap.widgets.TTbExtendedGridView', array(
    //'type' => 'striped bordered condensed',
    'id' => 'pages-list',
    //'enableSorting' => false,
    //'itemsCssClass' => 'table-normal table-hover-row',
    'dataProvider' => $model->search(),
    'ajaxUrl' => array('/admin/tariffs/index'),
    'filter' => $model,
    'summaryText' => Yii::t('admin', 'Тарифы').' '. Yii::t('admin', '{start}—{end} from <span>{count}</span>.'),

    'columns' => array(
        array(
            'name' => 'id',
            'value' => '$data->id',
            'type' => 'raw',
            'htmlOptions' => array('width' => '30px'),
        ),
        array(
            'name' => 'title',
            'htmlOptions' => array('width' => '300px'),
        ),
        array(
            'name' => 'price',
            'value' => '$data->price." руб"',
            'type' => 'raw',
             'htmlOptions' => array('width' => '200px'),
        ),
        array(
            'name' => 'oldprice',
            'value' => '$data->oldprice==""?"Нет":$data->oldprice." руб"',
            'type' => 'raw',
            'htmlOptions' => array('width' => '200px'),
        ),
        array(
            'type' => 'raw',
            'name' => 'active',
            'value' => '$data->getStatusForTable()',
            'htmlOptions' => array('width' => '80px'),
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            //'header' => 'Действия',
            'class' => 'bootstrap.widgets.TTbButtonColumn',
            'template' => '{update}  {delete}',
            'afterDelete' => 'function(link,success,data){
                            data =  $.parseJSON(data);
                            if(data.response.status=="success"){
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                                                 
                                    message: { text: data.response.data.messages }
                                }).show();
                            }else{
                                $(".top-right").notify({
                                    type:"bangTidy",
                                    fadeOut:{enabled: true, delay: 3000 },
                                    transition:"fade",                                                 
                                    message: { text: data.response.data.messages }
                                }).show();

                        }
                        }',
        )
    )
));
