<?php
$pageName = Yii::t('admin', 'Редактирование тариф');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => 'Тарифы',
        'url' => array('/admin/tariffs/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. 'Создать тариф',
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>