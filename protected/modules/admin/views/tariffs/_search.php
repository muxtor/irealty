<?php
/* @var $this PageController */
/* @var $model Page */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'url',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'text',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'meta_keywords',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'meta_description',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'enabled'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
