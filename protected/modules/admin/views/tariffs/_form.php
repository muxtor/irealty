<div class="container-fluid padded">
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-header">
                    <span class="title"><i class="<?php echo $icon ?>"></i><?= Yii::t('admin', 'Тарифы'); ?></span>
                    <ul class="box-toolbar">
                        <li>
                            <a rel="tooltip" data-original-title="<?= Yii::t('admin', 'Back'); ?>"
                               href="<?= Yii::app()->createUrl('/admin/tariffs/index'); ?>"><i
                                    class="icon-reply"></i></a>
                        </li>
                    </ul>
                </div>

                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-pages',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">
                    <div class="span7">
                        <div class="padded">
                            <?php echo $form->errorSummary($model);?>
                            <div class="w50p" style="width: 50%; float: left;">
                                <?php

                                echo $form->textFieldRow($model, 'price', array('class' => 'span11'));

                                echo $form->textFieldRow($model, 'oldprice', array('class' => 'span11'));

                                echo $form->textFieldRow($model, 'srok', array('class' => 'span11'));

                                echo $form->radioButtonList($model, 'sroktype', array('0'=>'час(а)','female'=>'дней'), array('class' => ''));

                                echo $form->checkBoxRow($model, 'active', array('class' => ''));

                                ?>
                            </div>
                            <div class="w50p" style="width: 50%; float: left;">
                                <?php

                                echo $form->textFieldRow($model, 'title', array('class' => 'span11'));

                                ?>
                                <?php
                                $colors = array(
                                    'white' => 'Белый',
                                    'green' => 'Зеленый',
                                    'blue' => 'Синий',
                                    'yellow' => 'Желтый',
                                    'fiolet' => 'Фиолетовый',
                                    'black' => 'Черный', );
                                echo $form->dropDownListRow($model, 'color', $colors,
                                    array('class' => 'span11', 'empty' => 'Выбрать цвет'));
                                ?>
                                <?php echo $form->textFieldRow($model, 'srokTitle', array('class' => 'span11')); ?>
                            </div>
                            <div class="clear clearfix"></div>
                        </div>
                        <div class="clear clearfix"></div>
                    </div>
                    <div class="clear clearfix"></div>


                </div>


                <!--end paped -->
                <div class="form-actions">
                    <div class="pull-right">
                        <?php
                        echo CHtml::link(
                            '<span class="icon-circle-arrow-left"></span> ' . Yii::t('admin', 'Return'),
                            Yii::app()->createUrl('/admin/tariffs/index'),
                            array(
                                'class' => 'link'
                            )
                        );
                        ?>
                        <?php
                        $this->widget(
                            'bootstrap.widgets.TbButton',
                            array(
                                'buttonType' => 'submit',
                                'label' => Yii::t('admin', 'Save'),
                                'type' => null,
                                'htmlOptions' => array(
                                    'class' => 'btn btn-default',
                                    'value' => 'index',
                                    'name' => 'submit',
                                ),
                                'size' => 'small',
                            )
                        );
                        ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
            </div>
        </div>
    </div>
    <!-- row-fluid-->
</div><!--container-fluid-->
