<?php
$pageName = Yii::t('admin', 'Редактирование тариф');

$this->pageTitle = $pageName;
$this->breadcrumbs = array(
    array(
        'icon' => 'icon-check',
        'label' => Yii::t('admin', 'Тарифы'),
        'url' => array('/admin/tariffs/index'),
    ),
    array(
        'icon' => 'icon-edit',
        'label' => ' '. Yii::t('admin', 'Редактирование тариф'),
        'url' => '',
    ),
);

echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));

?>