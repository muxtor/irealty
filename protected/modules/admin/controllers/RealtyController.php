<?php

class RealtyController extends MainController
{
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Realty();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Realty']))
		{
			$model->attributes=$_POST['Realty'];
			if($model->save()) {
				if (Yii::app()->user->hasState('uploadedFiles')) {

					$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

					foreach ($arrayFiles as $file) {
						$image = new Image;
						$image->parent_id = $model->id;
						$image->file = $file;
						$image->type = 'realty';
						$image->save();

					}
				}
				Yii::app()->user->setState('uploadedFiles', null);
				Yii::app()->user->setFlash('success', 'Изменения успешно применены');
				
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Realty']))
		{
			$model->attributes=$_POST['Realty'];
			if($model->save()) {
				if (Yii::app()->user->hasState('uploadedFiles')) {

					$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

					foreach ($arrayFiles as $file) {
						$image = new Image;
						$image->parent_id = $model->id;
						$image->file = $file;
						$image->type = 'realty';
						$image->save();
					}
				}
				Yii::app()->user->setState('uploadedFiles', null);
				Yii::app()->user->setFlash('success', 'Изменения успешно применены');

				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $_GET['Realty_sort']='id.desc';
		$model=new Realty('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Realty']))
			$model->attributes=$_GET['Realty'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ArticleService the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Realty::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ArticleService $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='article-service-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Загрузка фото через ajax
	 */
	public function actionUploadFiles()
	{
		$files = CUploadedFile::getInstancesByName('image');

		$im = Yii::app()->image;
		foreach ($files as $image) {
			$filename = md5(microtime()) . LString::transliterateString($image->name);
			$filesName[] = $filename;

			$image->saveAs(Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename);
			$im->cropSave(Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename,550,1024,Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename);

			$im->cropSave(Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename,100,100,Yii::getPathOfAlias('webroot') . Realty::THUMB_FOLDER . $filename);

			echo "<div class='file'>" .
				CHtml::image(Yii::app()->baseUrl . Realty::THUMB_FOLDER.$filename, '', array('style' => 'width:100px')) .
				"<br><a class='deletePhoto' href='javascript:void()' data='" . $filename . "'>Удалить</a></div>";
		}

		if (Yii::app()->user->hasState('uploadedFiles')) {
			$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));
			$filesName = array_merge($arrayFiles, $filesName);
		}

		Yii::app()->user->setState('uploadedFiles', CJSON::encode($filesName));
		Yii::app()->end();
	}

	/**
	 * Удаление загруженных фото
	 * @param $filename название файла
	 */
	public function actionDeletePhoto($filename)
	{
		$files[] = Yii::getPathOfAlias('webroot') . Realty::THUMB_FOLDER . $filename;
		$files[] = Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename;
		
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		Image::model()->deleteAllByAttributes(array('file' => $filename));

		if (Yii::app()->user->hasState('uploadedFiles')) {

			$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

			foreach ($arrayFiles as $file) {
				if ($file != $filename)
					$newArray[] = $file;
			}

			if (!isset($newArray))
				$newArray = array();

			Yii::app()->user->setState('uploadedFiles', CJSON::encode($newArray));
		}

		echo "ok";
		Yii::app()->end();
	}
}
