<?php

class ProjectController extends MainController
{

	public function filters()
	{
		$filters = parent::filters();
		$filters[] = 'ajaxOnly + changeAsSimilar + addSlider + removeSlider';

		return $filters;
	}

	public function accessRules() 
	{
		$rules = parent::accessRules();
		$rules[2] = array(
			'allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('create', 'update', 'delete', 'multipleRemove', 'changeAsSimilar', 'uploadFiles', 'addSlider', 'removeSlider'),
			'roles'=>array(User::ROLE_ADMIN),
		);

		return $rules;
	}

	/**
	 * !!!!!!!!!Dev!!!!!!!!!!!!
	 */
	public function actionMassUrlTranslite($classNames = array('Project', 'ArticleService'))
	{
		foreach ($classNames as $className) {
			$models = $className::model()->findAll();

			foreach ($models as $model) {
				if (empty($model->url))
					$model->url = LString::urlAlias($model->title);
				$model->url = LString::urlAlias($model->title);
				$model->save(false);
			}
		}
	}

	public function actionChangeAsSimilar()
	{
		$project_id = (int)$_POST['project_id'];
		$checked_project_id = (int)$_POST['checked_project_id'];
		
		if ($_POST['action'] == 'delete') {
			$model = SimilarProject::model()->findByAttributes(array('project_id' => $project_id, 'similar_project_id' => $checked_project_id));
			if ($model === null)
				return false;

			if ($model->delete())
				echo json_encode(array('success' => 'true'));
		} 

		if ($_POST['action'] == 'add') {
			$model = new SimilarProject;
			$model->project_id = $project_id;
			$model->similar_project_id = $checked_project_id;
			
			if ($model->save())
				echo json_encode(array('success' => 'true'));
		}
	}

	public function actionCreate()
	{
		$model=new Project;

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			if($model->save()) {
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$similarProjectIds = SimilarProject::getSimilarItemsListById($id);

		if (isset($_POST['Project']))
		{
			$model->attributes = $_POST['Project'];
			$model->indexpage_desc1 = $_POST['Project']['indexpage_desc1'];
			$model->indexpage_desc2 = $_POST['Project']['indexpage_desc2'];

			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$projects = Project::model();

		$this->render('update', array(
			'model' => $model,
			'projects' => $projects,
			'similarProjectIds' => $similarProjectIds
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function actionIndex()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
	
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Загрузка фото через ajax
	 */
	public function actionUploadFiles($id = null)
	{
		$files = CUploadedFile::getInstancesByName('image');
		$filesName = array();
		$im = Yii::app()->image;

		if (!in_array($id, array(1, 2, 3))) {
			$result['success'] = false;
			echo json_encode($result);
			Yii::app()->end();
		}
		
		$dimensions = ProjectSlider::getDimensions($id);

		$width = $dimensions['width'];
		$height = $dimensions['height'];
		$tWidth = $dimensions['thumb_width'];
		$tHeight = $dimensions['thumb_height'];

		if (!empty($files)) {
			foreach ($files as $image) {
				$filename = md5(microtime()) . LString::transliterateString($image->name);
				$filesName[] = $filename;
				
				$fullpath = Yii::getPathOfAlias('webroot') . Project::SLIDER_IMAGE_PATH . $filename;
				$image->saveAs($fullpath);
				$sliderImage = Yii::app()->ih->load($fullpath);

				if ( ($sliderImage->width < $width) || ($sliderImage->height < $height) ) {
					
					if (file_exists($fullpath))
						unlink($fullpath);
					
					$result['success'] = false;

				} else {
					
					if ( ($sliderImage->width != $width) && ($sliderImage->height != $height) )
						$sliderImage->adaptiveThumb($width, $height)->save();
					
					$sliderImage->resize($tWidth, $tHeight)->save(Yii::getPathOfAlias('webroot') . Project::SLIDER_THUMB_PATH . $filename);

					$result = array(
						'thumb' => Project::SLIDER_THUMB_PATH . $filename,
						'file_name' => $filename,
						'success' => true
					);
				}

				echo json_encode($result);
			}
		}

		if (Yii::app()->user->hasState('uploadedFiles')) {
			$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));
			if (!empty($arrayFiles))
				$filesName = array_merge($arrayFiles, $filesName);
		}

		if (!empty($filesName))
			Yii::app()->user->setState('uploadedFiles', CJSON::encode($filesName));
		
		Yii::app()->end();
	}

	/**
	 * Удаление загруженных фото
	 * @param $filename название файла
	 */
	public function actionDeletePhoto($filename)
	{
		$files[] = Yii::getPathOfAlias('webroot') . Project::SLIDER_THUMB_PATH . $filename;
		$files[] = Yii::getPathOfAlias('webroot') . Project::SLIDER_IMAGE_PATH . $filename;
		
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		Image::model()->deleteAllByAttributes(array('file' => $filename));

		if (Yii::app()->user->hasState('uploadedFiles')) {

			$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

			foreach ($arrayFiles as $file) {
				if ($file != $filename)
					$newArray[] = $file;
			}

			if (!isset($newArray))
				$newArray = array();

			Yii::app()->user->setState('uploadedFiles', CJSON::encode($newArray));
		}

		echo "ok";
		Yii::app()->end();
	}

	public function actionAddSlider()
	{
		$project_id = (int)Yii::app()->request->getPost('project_id');
		$images = Yii::app()->request->getPost('images');
		$result['success'] = false;

		if (!empty($images) || (count($images) == 3) ) {
			$slider = new ProjectSlider;
			$slider->project_id = $project_id;
			
			if ($slider->save()) {
				$type = Image::TYPE_PROJECT_SLIDER;
				
				foreach ($images as $imageNumber => $image) {
					$model = new Image;
					
					$model->slide_order = $imageNumber;
					$model->type = $type;
					$model->file = $image['filename'];
					$model->parent_id = $slider->id;
					$model->save(false);
					$result['success'] = true;
					$result['slider_id'] = $slider->id;
				}
			}
		}

		echo json_encode($result);

		Yii::app()->end();
	}

	public function actionRemoveSlider()
	{
		$id = (int)Yii::app()->request->getPost('id');
		$slider = ProjectSlider::model()->findByPk($id);
		$result['success'] = false;

		if ($slider !== null) {
			$slider->delete();
			$result['success'] = true;
		}

		echo json_encode($result);

		Yii::app()->end();
	}
}
