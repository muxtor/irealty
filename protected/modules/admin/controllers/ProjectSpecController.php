<?php

class ProjectSpecController extends MainController
{
	public function filters()
	{
		return array(
			'ajaxOnly'
		);
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		
		if(isset($_POST['ProjectSpec'])) {
			$model->attributes=$_POST['ProjectSpec'];
			if ($model->save())
				echo json_encode(array('success'=>'true'));
			else 
				echo "save error!";
		} else 
			echo "request error!";
	}

	public function actionCreate()
	{
		$project_id = (isset($_POST['project_id'])) ? (int)$_POST['project_id'] : null;
		$model = new ProjectSpec;
		$result = array();

		if(isset($_POST['ProjectSpec'])) {
			$model->attributes=$_POST['ProjectSpec'];

			if ($model->save())
				$result = array('model_id' => $model->id);
			else {
				$errors = '';
				foreach ($model->getErrors() as $error)
					$errors .= "<li>".$error[0]."</li>";

				$result = array(
					'hasError' => 1,
					'content' => $errors,
				);
			}

			echo json_encode($result);
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			echo json_encode(array('success'=>'true'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProjectSpec the loaded model
	 */
	public function loadModel($id)
	{
		$model = ProjectSpec::model()->findByPk($id);
		if ($model===null)
			return false;
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ProjectSpec $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-spec-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
