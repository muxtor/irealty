<?php

class PageController extends MainController
{
	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
	 	$model=new Page;

	 	// Uncomment the following line if AJAX validation is needed
	 	// $this->performAjaxValidation($model);

	 	if(isset($_POST['Page']))
	 	{
	 		$model->attributes=$_POST['Page'];
	 		$model->url = (empty($model->url)) ? LString::urlAlias($model->title) : LString::urlAlias($model->url);
            $model->text = $_POST['Page']['text'];
	 		if($model->save())
	 		{
                Yii::app()->user->setFlash('success', 'Изменения успешно применены');
	 			$this->redirect(array('update','id'=>$model->id));
	 		}
				
	 	}

	 	$this->render('create',array(
	 	'model'=>$model,
	 	));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Page']))
		{
			$model->attributes=$_POST['Page'];
			$model->url = (empty($model->url)) ? LString::urlAlias($model->title) : LString::urlAlias($model->url);

			/*if ($model->is_main == 1) {
				$model->text_1 = $_POST['Page']['text_1'];
				$model->text_2 = $_POST['Page']['text_2'];
			}*/
            $model->text = $_POST['Page']['text'];

			if($model->save())
                Yii::app()->user->setFlash('success', 'Изменения успешно применены'); $this->redirect(array('update','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'index' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->_loadModel($id);
			$canDeleted = ($model->is_main == 1) ? false : true;

			if (!$canDeleted)
				RAjax::error(array('messages' => 'Ошибка при удалении'));
			else {
				$model->delete();
				RAjax::success(array('messages' => 'Успешно удалено'));
			}

			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$this->pageTitle = 'Страницы';
		$model=new Page('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Page']))
			$model->attributes=$_GET['Page'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Page the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=Page::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param Page $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='page-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function _loadModel($id) {
		if (!$model = Page::model()->findByPk($id)) {
			if (Yii::app()->request->isAjaxRequest) {
				RAjax::error(array('messages' => 'Клиент не существует. id = '.$id));
			} else {
				throw new CHttpException(404, 'Клиент не существует. id = '.$id);
			}
		}
		return $model;
	}
}
