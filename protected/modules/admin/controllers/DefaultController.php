<?php

class DefaultController extends MainController
{
	public function actionIndex()
	{
		$requestCount = 0;
		$pageCount = 0;
		$this->render('index', array(
			'requestCount' => $requestCount, 
			'pageCount' => $pageCount,
		));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout='static';
		$model=new LoginForm;

		if (Yii::app()->user->isGuest == false)
			$this->redirect(array('index'));

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='form-login')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(array('index'));
				// $this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}