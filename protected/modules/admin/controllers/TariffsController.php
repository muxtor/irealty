<?php

class TariffsController extends MainController
{
	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
	 	$model=new Tarif();
        $model->sroktype = 0;

	 	// Uncomment the following line if AJAX validation is needed
	 	// $this->performAjaxValidation($model);

	 	if(isset($_POST['Tarif']))
	 	{
	 		$model->attributes=$_POST['Tarif'];
	 		if($model->save())
	 		{
                Yii::app()->user->setFlash('success', 'Изменения успешно применены');
	 			$this->redirect(array('update','id'=>$model->id));
	 		}
				
	 	}

	 	$this->render('create',array(
	 	'model'=>$model,
	 	));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tarif']))
		{
			$model->attributes=$_POST['Tarif'];

			if($model->save())
                Yii::app()->user->setFlash('success', 'Изменения успешно применены'); $this->redirect(array('update','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'index' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->_loadModel($id);


			if ($model==null)
				RAjax::error(array('messages' => 'Ошибка при удалении'));
			else {
				$model->delete();
				RAjax::success(array('messages' => 'Успешно удалено'));
			}

			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$this->pageTitle = 'Тарифы';
		$model=new Tarif('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tarif']))
			$model->attributes=$_GET['Tarif'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Page the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=Tarif::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param Page $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='page-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	protected function _loadModel($id) {
		if (!$model = Tarif::model()->findByPk($id)) {
			if (Yii::app()->request->isAjaxRequest) {
				RAjax::error(array('messages' => 'Клиент не существует. id = '.$id));
			} else {
				throw new CHttpException(404, 'Клиент не существует. id = '.$id);
			}
		}
		return $model;
	}
}
