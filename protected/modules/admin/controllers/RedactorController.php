<?php

class RedactorController extends UBackController
{

    const FILE_PATH = '/uploads/images/';

    public function filters()
    {
        return array(
                // 'accessControl',
        );
    }

//    public function savePath()
//    {
//        Yii::getPathOfAlias('frontend')).'/www/images/'
//           
//    }

    public function getBaseUrl()
    {
        return Yii::app()->request->getHostInfo();
    }

    /**
     * @todo указать правила RBAC 
     */
//    public function accessRules() {
//        return array(
//            array('allow',
//                'actions' => array('uploadedImages', 'imageUpload'),
//                'roles' => array('admin', 'root'),
//            ),
//            array('deny'),
//        );
//    }

    public function actionUploadedImages()
    {
        $images = array();
        $handler = opendir(Yii::getPathOfAlias('webroot')  . self::FILE_PATH);
        while ($file = readdir($handler)) {
            if ($file != "." && $file != "..")
                $images[] = $file;
        }
        closedir($handler);
        $jsonArray = array();
        foreach ($images as $image)
            $jsonArray[] = array(
                'thumb' => $this->getBaseUrl() . self::FILE_PATH . $image,
                'image' => $this->getBaseUrl() . self::FILE_PATH . $image
            );

        header('Content-type: application/json');
        echo CJSON::encode($jsonArray);
        Yii::app()->end();
    }

    public function actionImageUpload()
    {
        $file = CUploadedFile::getInstanceByName('file');
        $file_name = md5(date('YmdHis')) . '.jpg';

        if ($file->saveAs(Yii::getPathOfAlias('webroot')  . self::FILE_PATH . $file_name)) {

            $array = array(
                'filelink' => $this->getBaseUrl() . self::FILE_PATH . $file_name
            );
            header('Content-type: application/json');
            echo CJSON::encode($array);
            Yii::app()->end();
        }

        throw new CHttpException(403, 'The server is crying in pain as you try to upload bad stuff');
    }

}
