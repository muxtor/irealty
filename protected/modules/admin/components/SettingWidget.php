<?php
class SettingWidget extends CWidget {

	public function init()
    {
        return parent::init();
    }

	public function run()
	{
		$settings = Setting::model()->findByPk(1);
		$this->render('setting', array('model' => $settings));
	}
}