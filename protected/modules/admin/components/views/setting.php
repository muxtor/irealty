<?php
/* @var $this SettingController */
/* @var $model Setting */
/* @var $form CActiveForm */
?>
<div class="sidebar-wrapper">

	<ul class="nav nav-tabs nav-justified" id="sidebar-tab">
		<li class="active">
			<a href="#settings" role="tab" data-toggle="tab"><i class="fa fa-gear"></i></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="settings">
			<h5 class="sidebar-title">Основные настройки</h5>
			<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
				'id'=>'setting-setting-form',
				'enableAjaxValidation'=>false,
				'enableClientValidation'=>true,
                'method' => 'POST',
                'clientOptions'=>array(
                     'validateOnSubmit'=>true,
                     'validateOnChange'=>true,
                     'validateOnType'=>false,
  				),
				// Please note: When you enable ajax validation, make sure the corresponding
				// controller action is handling ajax validation correctly.
				// See class documentation of CActiveForm for details on this,
				// you need to use the performAjaxValidation()-method described there.
				// 'enableAjaxValidation'=>true,
			)); ?>
										
			<ul class="media-list">
				<?php /*/ ?>
				<li class="media">
					<?php echo $form->labelEx($model,'is_maintenance_time'); ?>
					<?php echo $form->checkBox($model,'is_maintenance_time'); ?>
					<?php echo $form->error($model,'is_maintenance_time'); ?>
				</li>
				<?php /*/ ?>
				<li class="media">
					<?php echo $form->labelEx($model,'request_email'); ?>
					<div><?php echo $form->textField($model,'request_email'); ?></div>
					<?php echo $form->error($model,'request_email'); ?>
				</li>

				<li class="media">
					<?php echo $form->labelEx($model,'call_me_email'); ?>
					<div><?php echo $form->textField($model,'call_me_email'); ?></div>
					<?php echo $form->error($model,'call_me_email'); ?>
				</li>
			</ul>

			<div class="sidebar-content">
				<?php echo CHtml::htmlButton(
					'<span class="ladda-label"> Сохранить настройки </span>
					<i class="fa fa-arrow-circle-right"></i>
					<span class="ladda-spinner"></span>
					<span class="ladda-spinner"></span>
					',
					array("id"=>"settings-submit","class" => "btn btn-success ladda-button", 'data-style' => 'expand-right')
                ); ?>
			</div>

			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>