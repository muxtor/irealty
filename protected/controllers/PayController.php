<?php

class PayController extends Controller
{

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $title = 'Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }
        $model = new Pays();
        $tariffs = Tarif::model()->findAllByAttributes(array(), 'active=1');
        if (Yii::app()->user->isGuest == true) {
            $this->render('pleaseLogin');
        } else {
            //проверка на наличие оплату
            /*$payed = Pays::model()->findByAttributes(array('userId'=>Yii::app()->user->id), 'status=1');
            if($payed->outDate > date('Y-m-d H:i:s', time())){
                $this->redirect('/buy');
            }*/

            if (isset($_POST['Pays'])) {
                $model->attributes=$_POST['Pays'];
                $tarif = Tarif::model()->findByPk($model->tarifId);
                if($tarif!==null){

                    // Оплата заданной суммы с выбором валюты на сайте ROBOKASSA
                    // Payment of the set sum with a choice of currency on site ROBOKASSA

                    // регистрационная информация (логин, пароль #1)
                    // registration info (login, password #1)
                    $mrh_login = "rieltoroff.net";
                    $mrh_pass1 = "fpx3voL7zC";



                    // описание заказа
                    // order description
                    $inv_desc = "ROBOKASSA Advanced User Guide";

                    // сумма заказа
                    // sum of order
                    $out_summ = $tarif->price;

                    // тип товара
                    // code of goods
                    $shp_item = $tarif->id;

                    // предлагаемая валюта платежа
                    // default payment e-currency
                    $in_curr = "";

                    // язык
                    // language
                    $culture = "ru";

                    // encoding
                    $encoding = "utf-8";
                    $model->payPrice = $tarif->price;
                    $model->userId = Yii::app()->user->id;
                    $model->signuture = 'test';


                    if($model->save()) {
                        // номер заказа
                        // number of order
                        $inv_id = $model->id;

                        // формирование подписи
                        // generate signature
                        $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");//
                        $model->signuture = $crc;
                        $model->update();


                        $redrictUrl = "https://auth.robokassa.ru/Merchant/Index.aspx?MrchLogin=$mrh_login&".
    "OutSum=$out_summ&InvId=$inv_id&Desc=$inv_desc&SignatureValue=$crc";
                        /*$redrictUrl = "http://test.robokassa.ru/index.aspx?MrchLogin=$mrh_login&".
                            "OutSum=$out_summ&InvId=$inv_id&Desc=$inv_desc&SignatureValue=$crc&Shp_item=$shp_item";*/
                        $this->redirect($redrictUrl);

                        /*// форма оплаты товара
                        // payment form
                        print "<html>".
                            "<form action='https://merchant.roboxchange.com/Index.aspx' method=POST>".
                            "<input type=hidden name=MrchLogin value=$mrh_login>".
                            "<input type=hidden name=OutSum value=$out_summ>".
                            "<input type=hidden name=InvId value=$inv_id>".
                            "<input type=hidden name=Desc value='$inv_desc'>".
                            "<input type=hidden name=SignatureValue value=$crc>".
                            "<input type=hidden name=Shp_item value='$shp_item'>".
                            "<input type=hidden name=IncCurrLabel value=$in_curr>".
                            "<input type=hidden name=Culture value=$culture>".
                            "<input type=submit value='Pay'>".
                            "</form></html>";*/

                        // HTML-страница с кассой
                        // ROBOKASSA HTML-page
                       /* print "<html><script language=JavaScript ".
                            "src='https://auth.robokassa.ru/Merchant/PaymentForm/FormFLS.js?".
                            "MrchLogin=$mrh_login&OutSum=$out_summ&InvId=$inv_id&IncCurrLabel=$in_curr".
                            "&Desc=$inv_desc&SignatureValue=$crc&Shp_item=$shp_item".
                            "&Culture=$culture&Encoding=$encoding'></script></html>";*/
                        //Yii::app()->end;
                    }
                }




            }
            $this->render('tariffs', array('model' => $model, 'tariffs' => $tariffs));
        }
    }

    public function actionResult(){

        $title = 'Результат оплата - Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        // регистрационная информация (пароль #2)
        // registration info (password #2)
        $mrh_pass2 = "fpx3voL7zC2";

        //установка текущего времени
        //current date
        $tm=getdate(time()+9*3600);
        $date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

        // чтение параметров
        // read parameters
        $out_summ = $_REQUEST["OutSum"];
        $inv_id = $_REQUEST["InvId"];
        $shp_item = $_REQUEST["Shp_item"];
        $crc = $_REQUEST["SignatureValue"];

        $crc = strtoupper($crc);

        $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item"));

        // проверка корректности подписи
        // check signature
        if ($my_crc !=$crc)
        {
            echo "bad sign\n";
            exit();
        } else {
            $model = Pays::model()->findByPk($inv_id);
            $tarif = Tarif::model()->findByPk($shp_item);
            $model->payedDate = date('Y-m-d H:i:s', time());
            if($tarif->sroktype==0){
                $model->outDate = date('Y-m-d H:i:s', (time() + (3600 * $tarif->srok)));
            }else{
                $model->outDate = date('Y-m-d H:i:s', (time() + (24*3600 * $tarif->srok)));
            }
            if($model->status==0){
                $model->status = 1;
                $model->update();
            }
            // признак успешно проведенной операции
            // success
            echo "OK$inv_id\n";
        }

    }

    public function actionSuccess(){
        $title = 'Успешно оплачен - Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        // регистрационная информация (пароль #2)
        // registration info (password #2)
        $mrh_pass2 = "fpx3voL7zC";

        //установка текущего времени
        //current date
        $tm=getdate(time()+9*3600);
        $date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

        if(isset($_REQUEST["InvId"])){
            // чтение параметров
            // read parameters
            $out_summ = $_REQUEST["OutSum"];
            $inv_id = $_REQUEST["InvId"];
            $shp_item = $_REQUEST["Shp_item"];
            $crc = $_REQUEST["SignatureValue"];

            $crc = strtoupper($crc);

            $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item"));//

            // проверка корректности подписи
            // check signature
            if ($my_crc !=$crc)
            {
                echo "bad sign\n";
                exit();
            } else {
                $model = Pays::model()->findByPk($inv_id);
                $tarif = Tarif::model()->findByPk($shp_item);
                $model->payedDate = date('Y-m-d H:i:s', time());
                if($tarif->sroktype==0){
                    $model->outDate = date('Y-m-d H:i:s', (time() + (3600 * $tarif->srok)));
                }else{
                    $model->outDate = date('Y-m-d H:i:s', (time() + (24*3600 * $tarif->srok)));
                }
                if($model->status==0){
                    $model->status = 1;
                    $model->update();
                }
                $this->render('success');
            }
        }else{
            $this->redirect(array('index'));
        }


    }
    public function actionFail(){
        $title = 'Вы отказались от оплаты - Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        if(isset($_REQUEST["InvId"])){
            $model = Pays::model()->findByPk($_REQUEST["InvId"]);
            if($model->status==0){
                $model->status = 2;
                $model->update();
            }
            $this->render('fail');
        }else{
            $this->redirect(array('index'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Pays::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function setMetaTags($title, $description = false, $keywords = false)
    {
        $settings = Setting::model()->findByPk(1);
        $settings->site_title;

        $this->pageTitle = $title .' - '.$settings->site_title;

        if ($keywords !== false) {
            Yii::app()->clientScript->registerMetaTag($keywords, 'keywords');
            Yii::app()->clientScript->registerMetaTag($description, 'description');
        } else {
            Yii::app()->clientScript->registerMetaTag($title, 'keywords');
            Yii::app()->clientScript->registerMetaTag($title, 'description');
        }
    }
}