<?php

class RealtyController extends Controller
{
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $title = 'Подать объявление о продаже - Продать';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }
		$model=new Realty();
        $model->phone = 7;
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='article-service-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

		if(isset($_POST['Realty']))
		{
			$model->attributes=$_POST['Realty'];

            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('phone', str_replace('+','',$model->phone), true);

            $ban = PhoneBanned::model()->find($criteria);
            if($ban!=null){
                Yii::app()->clientScript->registerScript(
                    'myHideEffect',
                    '$("#modal .modal-content").html("<div style=\"padding: 100px; text-align: center; color:red; font-weight: bold; font-size: 20px;\">'.CHtml::encode($ban->info).'</div>");
                    $("#modal").modal("show");
                    ',
                    //$("#modal").animate({opacity: 1.0}, 10000).fadeOut("slow");
                    CClientScript::POS_READY
                );
                Yii::app()->user->setFlash('success', $ban->info);
            }else{
                if($model->save()) {
                    if (Yii::app()->user->hasState('uploadedFiles')) {

                        $arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

                        foreach ($arrayFiles as $file) {
                            $image = new Image;
                            $image->parent_id = $model->id;
                            $image->file = $file;
                            $image->type = 'realty';
                            $image->save();

                        }
                    }
                    Yii::app()->user->setState('uploadedFiles', null);

                    Yii::app()->clientScript->registerScript(
                        'myHideEffect',
                        '$("#modal .modal-content").html("<div style=\"padding: 100px; text-align: center; color:green; font-weight: bold;\">Спасибо в скором времени наш менеджер добавит ваше обьявление</div>");
                        $("#modal").modal("show");
                        ',
                        //$("#modal").animate({opacity: 1.0}, 10000).fadeOut("slow");
                        CClientScript::POS_READY
                    );

                    Yii::app()->user->setFlash('success', 'Спасибо в скором времени наш менеджер добавит ваше обьявление');
                    $model = new Realty();
                    //$this->redirect('/buy');
                }
            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


    public function actionView($id)
    {
        $model=Realty::model()->with(array('types','rayons'))->findByPk($id);
        $bigImage = Image::model()->findByAttributes(array('parent_id'=>$id), 'type="realty"');

        $title = 'Посмотреть - Купить';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        $this->render('view',array(
            'model'=>$model,
            'bigImage'=>$bigImage,
        ));
    }

	/**
	 * Lists all models.
     * $criteria = new CDbCriteria();
    $criteria->order='vip DESC';
    $criteria->condition = 'status=1';
    $data = new CActiveDataProvider('Realty', array('criteria' => $criteria, 'pagination' => array(
    'pageSize' => 18,
    ),));
	 */
	public function actionIndex()
	{

        $title = 'Главная';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        $conditions = 'status=1';

        $criteria = new CDbCriteria();
        $criteria->order='vip DESC,id DESC';
        $criteria->condition = Realty::model()->myCriteria($conditions);
        $data = new CActiveDataProvider('Realty', array('criteria' => $criteria, 'pagination' => array(
            'pageSize' => 18,
        ),));


        if (Yii::app()->user->hasState('visitors')) {
            $visitors = Yii::app()->user->getState('visitors');
        }else{
            $id = 29683320;
            $token = 'e9bef68daf944231bcb88f00bd0c0bd2';
            //$today=date('Ymd', strtotime('-1 days'));
            $today=date('Ymd');
            $metrika_url = "http://api-metrika.yandex.ru/stat/traffic/summary.json?id=29683320&pretty=1&date1=$today&date2=$today&oauth_token=69d2a2b127254b86b025ec8d1a7dc105";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $metrika_url);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            $metrika = curl_exec ($ch);
            curl_close ($ch);
            $metrika_o = json_decode($metrika);
            $visitors = $metrika_o->data[0]->visitors;
            Yii::app()->user->setState('visitors',$visitors);
        }


		$this->render('index',array(
			'data'=>$data,
			'visitors'=>$visitors,
		));
	}

    public function actionBuy()
    {
        $title = 'Купить';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        $conditions = 'status=1';

        $sort = new CSort();
        // имя $_GET параметра для сортировки,
        // по умолчанию ModelName_sort
        $sort->sortVar = 'sort';
        // сортировка по умолчанию
        $sort->defaultOrder = 'vip DESC';
        // включает поддержку мультисортировки,
        // т.е. можно отсортировать сразу и по названию и по цене
        $sort->multiSort = true;
        // здесь описываем аттрибуты, по которым будет сортировка
        // ключ может быть произвольный, это будет $_GET параметр
        $sort->attributes = array(
            'price'=>array(
                'asc'=>'price ASC',
                'desc'=>'price DESC',
                'default'=>'desc',
                'label'=>'Цена',
                'htmlOptions'=>array('class'=>'date_sort'),
            ),
            'ploshad'=>array(
                'label'=>'Площади, кв.м',
                'asc'=>'ploshad ASC',
                'desc'=>'ploshad DESC',
                'default'=>'desc',
                'htmlOptions'=>array('class'=>'date_sort'),
            ),

        );

        $criteria = new CDbCriteria();
        //$criteria->order='vip DESC,id DESC';
        $criteria->condition = Realty::model()->myCriteria($conditions);
        $data = new CActiveDataProvider('Realty', array('criteria' => $criteria, 'pagination' => array(
            'pageSize' => 18,

        ),'sort'=>$sort,));

        $this->render('buy',array(
            'data'=>$data,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ArticleService the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Realty::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ArticleService $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='article-service-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Загрузка фото через ajax
	 */
	public function actionUploadFiles()
	{
		$files = CUploadedFile::getInstancesByName('image');

		$im = Yii::app()->image;
		foreach ($files as $image) {
			$filename = md5(microtime()) . LString::transliterateString($image->name);
			$filesName[] = $filename;

			$image->saveAs(Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename);
			$im->cropSave(Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename,550,1024,Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename);

			$im->cropSave(Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename,100,100,Yii::getPathOfAlias('webroot') . Realty::THUMB_FOLDER . $filename);

			echo "<div class='file'>" .
				CHtml::image(Yii::app()->baseUrl . Realty::THUMB_FOLDER.$filename, '', array('style' => 'width:100px')) .
				"<br><a class='deletePhoto' href='javascript:void()' data='" . $filename . "'>Удалить</a></div>";
		}

		if (Yii::app()->user->hasState('uploadedFiles')) {
			$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));
			$filesName = array_merge($arrayFiles, $filesName);
		}

		Yii::app()->user->setState('uploadedFiles', CJSON::encode($filesName));
		Yii::app()->end();
	}

	/**
	 * Удаление загруженных фото
	 * @param $filename название файла
	 */
	public function actionDeletePhoto($filename)
	{
		$files[] = Yii::getPathOfAlias('webroot') . Realty::THUMB_FOLDER . $filename;
		$files[] = Yii::getPathOfAlias('webroot') . Realty::IMAGE_FOLDER . $filename;
		
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		Image::model()->deleteAllByAttributes(array('file' => $filename));

		if (Yii::app()->user->hasState('uploadedFiles')) {

			$arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

			foreach ($arrayFiles as $file) {
				if ($file != $filename)
					$newArray[] = $file;
			}

			if (!isset($newArray))
				$newArray = array();

			Yii::app()->user->setState('uploadedFiles', CJSON::encode($newArray));
		}

		echo "ok";
		Yii::app()->end();
	}
    protected function setMetaTags($title, $description = false, $keywords = false)
    {
        $settings = Setting::model()->findByPk(1);
        $this->pageTitle = $title .' - '.$settings->site_title;

        if ($keywords !== false) {
            Yii::app()->clientScript->registerMetaTag($keywords, 'keywords');
            Yii::app()->clientScript->registerMetaTag($description, 'description');
        } else {
            Yii::app()->clientScript->registerMetaTag($title, 'keywords');
            Yii::app()->clientScript->registerMetaTag($title, 'description');
        }
    }
}
