<?php

class SiteController extends Controller
{
	public $layout='//layouts/static';
	public $defaultAction = 'index';
	public $assetsPath;
	public $themePath;
	public $phones;

	public function init()
	{
		$this->assetsPath = Yii::app()->theme->basePath.'/web';
		$this->themePath = Yii::app()->theme->baseUrl.'/web';
		return parent::init();
	}

	public function filters()
	{
		return array(
			'ajaxOnly + addReview + feedback'
		);
	}
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
		);
	}

	public function beforeAction($action)
	{
		$phones = array();
		$settings = Setting::model()->findByPk(1);
		
		$phones['country_code'] = $settings->country_code;
		$phones['phone_1'] = $settings->phone_1;
		$phones['phone_2'] = $settings->phone_2;
		$phones['full_phone_1'] = $settings->country_code .' '. $settings->phone_1;
		$phones['full_phone_2'] = $settings->country_code .' '. $settings->phone_2;

		$this->phones = $phones;
		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		$this->layout = '//layouts/blank';
        $settings = Setting::model()->findByPk(1);

		$title = $settings->site_title;
		$description = $settings->meta_descriptions;
		$keywords = $settings->meta_keywords;
		$this->setMetaTags($title, $description, $keywords);

		return $this->render('index', array());
	}

    public function actionFinditems()
    {
        function roomToText($rooms){
            $room = array();
            $room[1] = 'однокомнатная';
            $room[2] = 'двухкомнатная';
            $room[3] = 'трехкомнатная';
            $room[4] = 'чтерехкомнатная';
            $room[5] = 'пятикомнатная';
            $room[6] = 'шестикомнатная';
            $room[7] = 'семикомнатная';
            $room[8] = 'восемькомнатная';
            $room[9] = 'девятькомнатная';
            $room[10] = 'десятькомнатная';
            if(isset($room[$rooms])){
                return $room[$rooms];
            }else{
                return '';
            }

        }

        $this->layout = '//layouts/blank';

        $conditions = 'status=1';
        $criteria = new CDbCriteria();
        $criteria->order='id DESC';
        $criteria->condition = $conditions;
        $criteria->with = array('types','rayons');
        $items = Realty::model()->findAll($criteria);
        //print_r($items);die;
        echo '<br/>';
        echo '@start';
        //delete search items
        Searchs::model()->deleteAll();
        foreach($items as $item){
            $text = 'квартира '.$item->rooms.' комнатная '.roomToText($item->rooms).' '.$item->types->type.' тип дома '.$item->rayons->rayon.' район ';
            $text .= $item->street.' улица ';
            $text .= $item->etaj.' этаж ';
            $text .= $item->price.' цена ';



            $add = new Searchs;
            $add->item_id = $item->id;
            $add->title = 'Недвижимость';
            $add->text = $text;
            if(count($item->getImages())>0){
                $add->photo = 1;
            }
            $add->insert();
        }
        echo '<br/>';
        echo '@finish';

    }

    public function actionSearch()
    {
        $title = 'Поиск';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);
        $criteria = new CDbCriteria();
        if(isset($_GET['q']) AND !empty($_GET['q'])){
            $criteria->compare('text',$_GET['q'],true);
            if(isset($_GET['photo'])){
                $criteria->condition= 'photo=1';
            }
        }else{
            $criteria->condition= 'id=0';
        }

        $data = new CActiveDataProvider('Searchs', array('criteria' => $criteria, 'pagination' => array(
            'pageSize' => 18,
        ),));

        $this->render('search',array(
            'data'=>$data,
        ));
    }


    public function actionPage($url)
    {
        $model = Page::model()->findByAttributes(array('url' => $url));

        $title = $model->meta_title;
        $description = $model->meta_description;
        $keywords = $model->meta_keywords;
        $this->setMetaTags($title, $description, $keywords);

        return $this->render('page', array('model' => $model));
    }

	public function actionAbout()
	{
		$model = Page::model()->findByAttributes(array('url' => 'about'));

		$title = $model->meta_title;
		$description = $model->meta_description;
		$keywords = $model->meta_keywords;
		$this->setMetaTags($title, $description, $keywords);
		
		return $this->render('page', array('model' => $model));
	}

	public function actionPortfolio()
	{
		$models = Project::getItemsList();
		$this->setMetaTags('Портфолио');

		return $this->render('portfolio', array('projects' => $models));
	}

	/**
	 * Portfolio's child 
	 */
	public function actionProject($url)
	{
		$model = Project::model()->findByAttributes(array('url' => $url));
		
		$id = $model->id;
		$title = $model->meta_title;
		$description = $model->meta_description;
		$keywords = $model->meta_keywords;
		$this->setMetaTags($title, $description, $keywords);

		$similarProjectsIds = SimilarProject::getSimilarItemsListById($id);
		$similarProjects = Project::model()->findAllByAttributes(
			array('id' => $similarProjectsIds), 
			array(
				'select' => 'id, adress, url'
			)
		);

		return $this->render('portfolio-item', array('project' => $model, 'similarProjects' => $similarProjects));
	}

	public function actionArticleService($type, $url = null)
	{
		try {
			$model = ($url === null) ? ArticleService::model()->$type()->lastRecord()->find() : ArticleService::model()->findByAttributes(array('url' => $url));
			
			if ($model === null)
				throw new CHttpException(404, "Страница не найдена");

			$title = $model->meta_title;
			$description = $model->meta_description;
			$keywords = $model->meta_keywords;
			$this->setMetaTags($title, $description, $keywords);

			$except_id = $model->id;
			$models = ArticleService::model()->$type()->findAll(array(
				'select' => 'id, title, url',
				'condition' => 'id != :id',
				'params' => array(
					':id' => $except_id,
				),
				'order' => 'created_on DESC'
			));
		} catch (Exception $e) {
			throw new CHttpException(404, "Неправильно набран адрес или такой страницы не существует.");
		}

		return $this->render('article-service', array('model' => $model, 'models' => $models));
	}

	/**
	 * Sends feedback to email by ajax
	 * @return string
	 */
	public function actionFeedback()
	{
		$feedbackEmail = Setting::model()->findByPk(1)->feedback_email;
		$email = CHtml::encode($_POST['email']);
		$text = CHtml::encode($_POST['text']);
		
		$message = "
			<html>
			<body>
				<h1>Обратная связь с сайта svoi-design.com</h1>
				<table>
					<tr>
						<td>Email: </td>
						<td>$email</td>
					</tr>
					<tr>
						<td>Текст: </td>
						<td>$text</td>
					</tr>
				</table>
				<pre>Отправлено ".date('d-m-Y h:i:s', time())."</pre>
			</body>
			</html>
		";

		$mailer = Yii::createComponent('application.extensions.mailer.EMailer');
		// $mailer->Host = <your smtp host>;
		// $mailer->IsSMTP();
		$mailer->From = 'support@svoi-design.com';
		$mailer->AddAddress($feedbackEmail);
		$mailer->FromName = 'svoi-design.com';
		$mailer->CharSet = 'UTF-8';
		$mailer->Subject = "Обратная связь с сайта svoi-design.com";
		$mailer->ContentType = "text/html";
		$mailer->Body = $message;

		$success = ($mailer->Send()) ? true : false;
		
		echo json_encode(array(
			'success' => $success
		));
	}

	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				switch ($error['code']) {
					case 505:
						$view = 'error505';
					break;
					
					case 500:
						$view = 'error500';
					break;

					default:
						$view = 'error404';
					break;
				}
				$this->setMetaTags('Ошибка '.$error['code']);
				$this->renderPartial($view, $error);
			}
		}
	}

	protected function setMetaTags($title, $description = false, $keywords = false)
	{
        $settings = Setting::model()->findByPk(1);
        $settings->site_title;

		$this->pageTitle = $title .' - '.$settings->site_title;

		if ($keywords !== false) {
			Yii::app()->clientScript->registerMetaTag($keywords, 'keywords');
			Yii::app()->clientScript->registerMetaTag($description, 'description');
		} else {
			Yii::app()->clientScript->registerMetaTag($title, 'keywords');
			Yii::app()->clientScript->registerMetaTag($title, 'description');
		}
	}
}