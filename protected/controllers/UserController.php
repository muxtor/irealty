<?php

class UserController extends Controller
{


	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionSignup()
	{
        $title = 'Регистрация - Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        $model=new User;
        $model->login = 7;
        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }

        if (Yii::app()->user->hasState('userInfo')) {
            $before = Yii::app()->user->getState('userInfo');
        }

        // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
            $model->login = '+'.preg_replace('/[^\d]+/', '', $model->login);
			if($model->validate()){
                $code = LString::generateNum(5);
                Yii::app()->user->setState('userInfo',
                    array(
                        'phone'=>$model->login,
                        'pass'=>$model->pswd,
                        'name'=>$model->name,
                        'code'=>$code,
                    )
                );

                $sms = new Sms();
                $sms->sms_send($model->login, 'Код для потверждения: '.$code, '79247652654', time(), $translit = false, $test = false);

                $confirm = new PhoneConfirm();
                $confirm->phone = $model->login;
                $this->render('phoneconfirm',array(
                    'model'=>$confirm,
                    'details'=>$model,
                ));
                Yii::app()->end();
            }

		}

		$this->render('signup',array(
		'model'=>$model,
		));
	}

    public function actionConfirm()
    {
        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }
        if (!Yii::app()->user->hasState('userInfo')) {
            $this->redirect(array('signup'));
        }

        $model=new PhoneConfirm;
        $sms = new Sms();
        $noOther = false;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['PhoneConfirm']))
        {
            $model->attributes=$_POST['PhoneConfirm'];
            if($model->validate()){
                //Проверка на наличие сессия для регистрация
                if (Yii::app()->user->hasState('userInfo')) {
                    $before = Yii::app()->user->getState('userInfo');
                    if($model->code==$before['code'] AND $model->phone==$before['phone']){
                        $user=new User;
                        $user->login = $before['phone'];
                        $user->phone = $before['phone'];
                        $user->name = $before['name'];
                        //$user->pswd = $before['pass'];
                        $user->pswd = md5(md5(User::PSWD_SALT.$before['pass']));
                        $user->activeted = 1;
                        if($user->save()){
                            Yii::app()->user->setState('userInfo', null);
                            $this->render('confirmed');
                            $noOther = true;
                            $sms->sms_send($user->login, 'Ваш номер успешно потвержден. Пароль для входа: '.$before['pass'], '79247652654', time(), $translit = false, $test = false);
                            Yii::app()->end();
                        }
                    }
                }else{
                    $this->redirect(array('signup'));
                }
            }
        }

        if($noOther==false){
            $this->render('phoneconfirm',array(
                'model'=>$model,
                'details'=>$model,
            ));
        }

    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $title = 'Логин - Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        $this->layout='static';
        $model=new LoginForm;
        $model->username = 7;
        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }

        if (Yii::app()->user->isGuest == false)
            $this->redirect(array('index'));

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='form-login')
        {
            $model->username = '+'.preg_replace('/[^\d]+/', '', $model->username);
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            $model->login = '+'.preg_replace('/[^\d]+/', '', $model->login);
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect('/');
            //$this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * Displays the login page
     */
    public function actionRemind()
    {
        $this->layout='static';
        $model=new RemindForm();
        $model->phone = 7;
        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }
        if (Yii::app()->user->isGuest == false)
            $this->redirect(array('index'));

        // if it is ajax validation request
        /*if(isset($_POST['ajax']) && $_POST['ajax']==='form-login')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }*/

        // collect user input data
        if(isset($_POST['RemindForm']))
        {
            $model->attributes=$_POST['RemindForm'];
            $model->phone = '+'.preg_replace('/[^\d]+/', '', $model->phone);
            if(isset($_GET['getCode'])){
                if (Yii::app()->user->hasState('remind')) {
                    $codeSended = Yii::app()->user->getState('remind');
                    $timeAgo = strtotime(date("Y-m-d H:i:s")) - strtotime($codeSended['date']);
                    if($timeAgo/60 < 20){
                        echo '20min';
                        Yii::app()->end();
                    }
                }
                $user = User::model()->findByAttributes(array('phone'=>$model->phone));
                if($user===null){
                    echo 'Нет такое номер';
                    Yii::app()->end();
                }else{
                    $code = LString::generateNum(5);

                    Yii::app()->user->setState('remind',null );
                    Yii::app()->user->setState('remind',
                        array(
                            'phone'=>$model->phone,
                            'code'=>$code,
                            'date'=>date("Y-m-d H:i:s"),
                        )
                    );

                    echo 'ok';
                    $sms = new Sms();
                    $sms->sms_send($user->login, 'Код для Восстановление пароля: '.$code, '79247652654', time(), $translit = false, $test = false);
                    Yii::app()->end();
                }
            }else{
                // validate user input and redirect to the previous page if valid
                if($model->validate()){
                    $code = Yii::app()->user->getState('remind');
                    $user = User::model()->findByAttributes(array('phone'=>$code['phone']));

                    if($user->phone == $model->phone){
                        Yii::app()->user->setState('change',
                            array(
                                'userId'=>$user->id,
                            )
                        );

                        $this->redirect(array('change'));
                    }else{
                        $model->addError('phone', 'Ошибка: введите номер который отправлен СМС. ');
                    }

                }
            }


            //$this->redirect(Yii::app()->user->returnUrl);
        }
        if (Yii::app()->user->hasState('remind')) {
            $codeSended = Yii::app()->user->getState('remind');
        }

        // display the login form
        $this->render('remind',array('model'=>$model));
    }

    /**
     * Displays the login page
     */
    public function actionChange()
    {
        $this->layout='static';
        $model=new ChangeForm();
        if (Yii::app()->user->isGuest == true){
            if (!Yii::app()->user->hasState('change')) {
                $this->redirect(array('signup'));
            }
        }
        if(isset($_GET['ajax'])){
            $this->layout = 'blank';
        }
        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='form-login')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['ChangeForm']))
        {
            $model->attributes=$_POST['ChangeForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate()){
                $id = Yii::app()->user->getState('change');
                if (Yii::app()->user->isGuest == false){
                    $user = User::model()->findByPk(Yii::app()->user->id);
                }else{
                    $user = User::model()->findByPk($id['userId']);
                }

                //$user->pswd = $model->password;
                $user->pswd = md5(md5(User::PSWD_SALT.$model->password));
                if($user->save())
                    Yii::app()->user->setState('remind',null ); Yii::app()->user->setState('change',null ); $this->redirect(array('login'));
            }

            //$this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('change',array('model'=>$model));
    }

	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
        $title = 'Личный кабинет - Получить доступ';
        $description = '';
        $keywords = '';
        $this->setMetaTags($title, $description, $keywords);

        $this->layout='static';
        $change=new ChangeForm();
        // collect user input data
        if(isset($_POST['ChangeForm']))
        {
            $change->attributes=$_POST['ChangeForm'];
            // validate user input and redirect to the previous page if valid
            if($change->validate()){
                $user = User::model()->findByPk(Yii::app()->user->id);
                //$user->pswd = $change->password;
                $user->pswd = md5(md5(User::PSWD_SALT.$change->password));
                if($user->update()){
                    $this->redirect('/user');
                }
            }

            //$this->redirect(Yii::app()->user->returnUrl);
        }
        $model = User::model()->findByPk(Yii::app()->user->id);
        // collect user input data
        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            // validate user input and redirect to the previous page if valid
            if($model->update()){
                $this->redirect('/user');
            }

            //$this->redirect(Yii::app()->user->returnUrl);
        }
        $pay = new Pays();
        $tariffs = Tarif::model()->findAllByAttributes(array(), 'active=1');
        // display the login form
        $this->render('index',array('change'=>$change,'model'=>$model,'pay'=>$pay, 'tariffs'=>$tariffs));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return User the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param User $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    protected function setMetaTags($title, $description = false, $keywords = false)
    {
        $settings = Setting::model()->findByPk(1);
        $settings->site_title;

        $this->pageTitle = $title .' - '.$settings->site_title;

        if ($keywords !== false) {
            Yii::app()->clientScript->registerMetaTag($keywords, 'keywords');
            Yii::app()->clientScript->registerMetaTag($description, 'description');
        } else {
            Yii::app()->clientScript->registerMetaTag($title, 'keywords');
            Yii::app()->clientScript->registerMetaTag($title, 'description');
        }
    }
}