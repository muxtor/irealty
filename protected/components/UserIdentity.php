<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	const ERROR_ACCESS_DENIED = -1;
	private $_id;

	public function authenticate()
	{
		sleep(2);
		$user = User::model()->findByAttributes(array('login'=>$this->username));
		
		if (is_null($user))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif ($user->pswd !== md5(md5(User::PSWD_SALT.$this->password)))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else 
		{
			$this->_id = $user->id;
			$this->setState('name', $user->login);
			$this->setState('title', $user->login);
			$this->errorCode=self::ERROR_NONE;
		}
		
		// return ($this->errorCode === -1) ? $this->errorCode : false;
		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}

	public function getUsername()
	{
		return $this->_username;
	}
}