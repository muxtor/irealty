<?php
/**
 * Class LString
 * Класс для работы со строками
 *
 */
class LString {
	/**
	 * Умная обрезка предложения до указанного размера, не обрезая слова (по знакам препинания или пробелам).
	 *
	 *
	 * @param string $string Текст, который надо обрезать
	 * @param int $max_length Максимальная длина строки, которую нельзя превышать
	 * @return string
	 */
	public static function trimSentence($string, $max_length) {
		if (mb_strlen($string) <= $max_length) return $string;// Если длина строки меньше, чем надо, сразу возвращаем результат

		$chars = '\s\?\.:;';// Список символов, по которым можно обрезать строку
		$result = preg_replace('/[' . $chars . '][^' . $chars . ']+$/', '', mb_substr($string, 0, $max_length)); 

		return $result . '...';
	}

	/**
	 * Вывод количества чего-то с окончанием согласно морфологии русского языка.
	 *
	 * countWord($AutoCnt, array ('товар', 'товара', 'товаров'), 'показывать 0 словом нет?', 'выводить перед словом число?')
	 *
	 *
	 * @param int $count Число
	 * @param array $cases Варианты слов с нужными окончаниями, например: array('товар', 'товара', 'товаров')
	 * @param string | false $zeroAsWord Показывать 0 каким-либо словом (например, "нет") или показывать как число 0 (false)
	 * @param bool $showCount Выводить перед словом число/количество (true) или нет (false)
	 * @return string
	 */
	public static function countPostfix($count, $cases, $zeroAsWord = 'нет', $showCount = true) {
		$countString = preg_replace('/[^\d]+/', '', $count);// Удаляем всё, кроме чисел

		// -- Определяем, какой тип окончания использовать
		if ($countString == 0) {
			$caseIndex = 2;
		} else if (in_array(substr($countString, -2), array(11, 12, 13, 14))) {// Например, 11 (одиннадцать) товаров, ведь оканчиваясь на единицу будет неверно - 1 товар
			$caseIndex = 2;
		} else if ($countString == 1) {
			$caseIndex = 0;
		} else if (in_array(substr($countString, -1), array(2, 3, 4))) {// Например, 2 товара
			$caseIndex = 1;
		} else {
			$caseIndex = 2;
		}
		// -- -- -- --

		$result = $cases[$caseIndex];

		// -- Если необходимо вывести перед словом количество
		if ($showCount !== false) {
			// -- Если количество равно нулю, и надо отобразить не число а слово (например, "нет")
			if ($count == 0 && $zeroAsWord !== false) {
				$count = $zeroAsWord;
			}
			// -- -- -- --

			$result = $count . ' ' . $result;
		}
		// -- -- -- --

		return $result;
	}

	/**
	 * Вытаскивает идентификатор видео из URL адреса на YouTube.
	 *
	 *
	 * @param string $url URL адрес
	 * @return string | false Идентификатор видео или false, если извлечь не удалось
	 */
	public static function parseYoutubeUrl($url) {
		$pattern = '#^(?:https?://)?';		# Optional URL scheme. Either http or https.
		$pattern .= '(?:www\.)?';			#  Optional www subdomain.
		$pattern .= '(?:';					#  Group host alternatives:
		$pattern .=   'youtu\.be/';			#    Either youtu.be,
		$pattern .=   '|youtube\.com';		#    or youtube.com
		$pattern .=   '(?:';				#    Group path alternatives:
		$pattern .=     '/embed/';			#      Either /embed/,
		$pattern .=     '|/v/';				#      or /v/,
		$pattern .=     '|/watch\?v=';		#      or /watch?v=,
		$pattern .=     '|/watch\?.+&v=';	#      or /watch?other_param&v=
		$pattern .=   ')';					#    End path alternatives.
		$pattern .= ')';					#  End host alternatives.
		$pattern .= '([\w-]{11})';			# 11 characters (Length of Youtube video ids).
		$pattern .= '(?:.+)?$#x';			# Optional other ending URL parameters.

		preg_match($pattern, $url, $matches);

		return (isset($matches[1])) ? $matches[1] : false;
	}

	/**
	 * Подсветка указанного текста (регистр букв не учитывается).
	 * Подсветка игнорирует атрибуты тегов, дабы их не портить (например, a href="" или img src="").
	 *
	 *
	 * @param string $text Текст, в котором надо подсветить
	 * @param array | string $entries Отдельная строка или массив строк
	 * @param string $cssClass Класс стиля, который будет применён к найденому тексту
	 * @return string
	 */
	public static function highlight($text, $entries, $cssClass = 'cl_highlight') {
		if (!is_array($entries)) $entries = array($entries);// Приводим к массиву для совместимости

		// -- Проходимся по каждому указанному тексту и делаем синтаксическую обёртку
		foreach ($entries as $entry) {
			$entry = trim(mb_strtolower($entry));// Удаляем пробелы справа/слева и приводим в нижний регистр
			if ($entry == '') continue;

			$text = preg_replace('/(' . $entry . ')/ui', '[HL]\1[/HL]', $text);// Обёртываем в синтаксис
		}
		// -- -- -- --

		$text = preg_replace('/(<[^>]*?)\[HL\](.*?)\[\/HL\](.*?>)/ui', '\1\2\3', $text);// Удаляем некорректные подстветки, например, в URL адресах ссылок или атрибутах src

		// -- Заменяем обёртки на HTML код
		$text = str_replace('[HL]', '<span class="' . $cssClass . '">', $text);
		$text = str_replace('[/HL]', '</span>', $text);
		// -- -- -- --

		return $text;
	}

	/**
	 * Конвертация строки из UTF-8 в WIN-1251
	 *
	 *
	 * @param string $string
	 * @return string
	 */
	public static function utf8_win($string) {
		$out = '';
		$c1 = '';
		$byte2 = false;

		for ($c = 0; $c < strlen($string); $c++){
			$i = ord($string[$c]);

			if ($i <= 127) $out .= $string[$c];

			if ($byte2) {
				$new_c2 = ($c1 & 3) * 64 + ($i & 63);
				$new_c1 = ($c1 >> 2) & 5;
				$new_i = $new_c1 * 256 + $new_c2;

				if ($new_i == 1025){
					$out_i = 168;
				} else {
					if ($new_i == 1105){
						$out_i = 184;
					}else {
						$out_i = $new_i-848;
					}
				}

				$out .= chr($out_i);
				$byte2 = false;
			}

			if (($i >> 5) == 6) {
				$c1 = $i;
				$byte2 = true;
			}
		}

		return $out;
	}

	/**
	 * Получение url адреса исходя из заданного текста.
	 * Например, чтобы исходя из названия товара (или новости) отобразить человеко-понятный URL.
	 *
	 *
	 * @param string $text
	 * @return string
	 */
	public static function urlAlias($text) {
		$text = preg_replace('/\[([^\]]+)\]/u', '', $text);
		$text = preg_replace('/\(([^\)]+)\)/u', '', $text);

		$translit = array(
			'/' =>  '-', '\\' =>  '-', ' ' =>  '-', 'а' =>  'a', 'б' =>  'b', 'в' =>   'v', 'г' => 'g', 'д' => 'd',
			'е' =>  'e',  'ё' => 'yo', 'ж' => 'zh', 'з' =>  'z', 'и' =>  'i', 'й' =>   'j', 'к' => 'k', 'л' => 'l',
			'м' =>  'm',  'н' =>  'n', 'о' =>  'o', 'п' =>  'p', 'р' =>  'r', 'с' =>   's', 'т' => 't', 'у' => 'u',
			'ф' =>  'f',  'х' =>  'x', 'ц' =>  'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ы' => 'y', 'э' => 'e',
			'ю' => 'yu',  'я' => 'ya', 'ь' =>   '', 'ъ' =>   '', '-' => '-',
		);

		$text = mb_strtolower($text, 'UTF-8');

		$text = str_replace(array_keys($translit), array_values($translit), $text);

		$text = preg_replace('/[^\-_A-z0-9]/u', '', $text);
		$text = str_replace('-[]', '', $text);
		$text = str_replace('[', '', $text);
		$text = str_replace(']', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('/-+/', '-', $text);

		return $text;
	}

	/**
	 * Транслитерация строки
	 * @param  string $text
	 * @return string
	 */
	public static function transliterateString($text) 
    {
		$searchurl = array("а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", "А", "Б","В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", " ");
		$replaceurl = array("a", "b", "v", "g", "d", "e", "jo", "zh", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "w", "tz", "y", "mz", "je", "ju", "ja", "A", "B","V", "G", "D", "E", "JO", "ZH", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "CH", "SH", "W", "TZ", "Y", "MZ", "JE", "JU", "_");
		return str_replace($searchurl, $replaceurl, $text);
	}

    public static  function generateHash($length = 6)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

    /**
     * Generate password auto
     */
    public static  function generateNum($length = 6)
    {
        $chars = '0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }
}