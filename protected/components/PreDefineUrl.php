<?php

    final class PreDefineUrl {

        public static function SiteUrl() {
            $array = array();
            return Yii::app()->createUrl('site', $array);
        }
        
        public static function AboutUrl() {
            $array = array();
            return Yii::app()->createUrl('page/about/', $array);
        }

 


        public static function CssCurrentUrl($pagename, $type='main') {
             
            if(strtolower($pagename) == strtolower(Yii::app()->controller->id)) {
                if($type == 'main') {
                    return 'class="current"';
                } else if ($type == 'submain') {
                    return 'current';
                }
            }
            return '';
        }


        public static function BannerImg()
        {
            return '/timthumb.php?w=218&h=181&src=';
        }

        public static function RealtyBigImage()
        {
            return '/timthumb.php?w=714&h=380&src=';
        }

        public static function RealtyImagesThumb()
        {
            return '/timthumb.php?w=155&h=101&src=';
        }

        public static function RealtyItemsThumb()
        {
            return '/timthumb.php?w=218&h=142&src=';
        }

        public static function ImageSize100x100()
        {
            return '/timthumb.php?w=100&h=100&src=';
        }
        public static function ImageSize80x80()
        {
            return '/timthumb.php?w=80&h=80&src=';
        }
        public static function ImageSize50x50()
        {
            return '/timthumb.php?w=50&h=50&src=';
        }
        public static function ImageSize40x40()
        {
            return '/timthumb.php?w=40&h=40&src=';
        }
        public static function ImageSize32x32()
        {
            return '/timthumb.php?w=32&h=32&src=';
        }
        public static function ImageSize24x24()
        {
            return '/timthumb.php?w=24&h=24&src=';
        }
    }

    
?>
