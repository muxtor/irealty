<?php

/**
 * This is the model class for table "project_slider".
 *
 * The followings are the available columns in table 'project_slider':
 * @property string $id
 * @property string $project_id
 */
class ProjectSlider extends CActiveRecord
{
	private static $dimensions = array(
		1 => array(
			'width' => 379,
			'height' => 422,
			'thumb_width' => 120,
			'thumb_height' => 134
		),
		2 => array(
			'width' => 319,
			'height' => 220,
			'thumb_width' => 100,
			'thumb_height' => 69
		),
		3 => array(
			'width' => 319,
			'height' => 197,
			'thumb_width' => 100,
			'thumb_height' => 62
		)
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project_slider';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_id', 'required'),
			array('project_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, project_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('project_id',$this->project_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectSlider the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeDelete()
	{
		$images = Image::model()->findAllByAttributes(array(
			'parent_id' => $this->id,
			'type' => Image::TYPE_PROJECT_SLIDER
		));

		if (!empty($images)) {
			foreach ($images as $image) {
				$file = getcwd().Project::SLIDER_IMAGE_PATH.$image->file;
				if (file_exists($file))
					unlink($file);

				$file = getcwd().Project::SLIDER_THUMB_PATH.$image->file;
				if (file_exists($file))
					unlink($file);

				$image->delete();
			}
		}

		return parent::beforeDelete();
	}

	public static function getImages($slider_id)
	{
		$images = Image::model()->slider()->findAll(array(
			'select' => 'id, file, slide_order',
			'params' => array(
				':parent_id' => $slider_id,
			),
			'condition' => 'parent_id = :parent_id',
		));

		foreach ($images as &$image) {
			$image = $image->attributes;
		}
		unset($image);

		return $images;
	}

	/**
	 * Returns slider's image dimensions
	 * @param  integer | null $slide_order
	 * @return array
	 */
	public static function getDimensions($slide_order = null)
	{
		$dimensions = self::$dimensions;
		return ($slide_order === null) ? $dimensions : $dimensions[$slide_order];
	}
}
