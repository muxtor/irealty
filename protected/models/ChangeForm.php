<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ChangeForm extends CFormModel
{
	public $password;
	public $password2;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('password, password2', 'required'),
            array('password2', 'compare', 'compareAttribute'=>'password', 'allowEmpty' => false,),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'password'=>'Пароль',
			'password2' => 'Повторить пароль',
		);
	}
}
