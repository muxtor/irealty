<?php

/**
 * This is the model class for table "setting".
 *
 * The followings are the available columns in table 'setting':
 * @property integer $id
 * @property string $feedback_email
 * @property string $country_code
 * @property string $phone_1
 * @property string $phone_2
 * @property string $email_admin
 * @property string $link_vk
 * @property string $link_fb
 * @property string $link_ok
 * @property string $link_tw
 * @property string $site_title
 * @property string $meta_keywords
 * @property string $meta_descriptions
 */
class Setting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('feedback_email, country_code, phone_1, phone_2', 'required'),
			array('feedback_email, country_code, phone_1, phone_2, link_vk, link_fb, link_ok, link_tw, site_title', 'length', 'max'=>255),
			array('email_admin', 'length', 'max'=>128),
			array('meta_keywords, meta_descriptions', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, feedback_email, country_code, phone_1, phone_2, email_admin, link_vk, link_fb, link_ok, site_title, meta_keywords, meta_descriptions', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'feedback_email' => 'Email для обратной связи',
			'country_code' => 'Код города',
			'phone_1' => 'Телефон 1',
			'phone_2' => 'Телефон 2',
			'email_admin' => 'E-mail администратора',
			'link_vk' => 'Ссылка на группу vk.com',
			'link_fb' => 'Ссылка на публичную страницу Facebook',
			'link_ok' => 'Ссылка на сообщество в odnoklassniki.ru',
			'link_tw' => 'Ссылка на сообщество в twitter.com',
			'site_title' => 'Название сайта (Стандартный Title для всех страниц)',
			'meta_keywords' => 'SEO Description для главной страницы',
			'meta_descriptions' => 'SEO Keywords  для главной страницы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('feedback_email',$this->feedback_email,true);
		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('phone_1',$this->phone_1,true);
		$criteria->compare('phone_2',$this->phone_2,true);
		$criteria->compare('email_admin',$this->email_admin,true);
		$criteria->compare('link_vk',$this->link_vk,true);
		$criteria->compare('link_fb',$this->link_fb,true);
		$criteria->compare('link_ok',$this->link_ok,true);
		$criteria->compare('link_tw',$this->link_tw,true);
		$criteria->compare('site_title',$this->site_title,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('meta_descriptions',$this->meta_descriptions,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Setting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
