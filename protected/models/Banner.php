<?php

/**
 * This is the model class for table "banner".
 *
 * The followings are the available columns in table 'banner':
 * @property integer $id
 * @property string $title
 * @property string $text1
 * @property string $text
 * @property string $link
 * @property string $razdel
 * @property integer $status
 * @property string $image
 */
class Banner extends CActiveRecord
{

    const IMAGE_PATH = '/uploads/images/';
    const THUMB_PATH = '/uploads/images/.thumbs/'; // admin thumb

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'banner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, link', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('title, text1, link, razdel, image', 'length', 'max'=>255),
            array('image', 'file', 'allowEmpty'=>true,'types'=>'jpg, gif, png, jpeg'),
			array('text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, text1, text, link, razdel, status, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название баннера',
			'text1' => 'Текст под название',
			'text' => 'Описание',
			'link' => 'Ссылка',
			'razdel' => 'Для раздела',
			'status' => 'Статус',
			'image' => 'Рисунок',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text1',$this->text1,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('razdel',$this->razdel,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeValidate()
    {
        $images = array();
        $image = CUploadedFile::getInstance($this,'image');
        if (empty($this->image) || !empty($image))
            $images['image'] = $image;

        if (!empty($images)) {

            foreach ($images as $field => $model) {
                if($model !== null){
                    $strSource = uniqid() . '.' . $model->getExtensionName();
                    $imagePath = Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource;
                    $model->saveAs($imagePath);

                    Yii::app()->image->cropSave(Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource, 300,200, Yii::getPathOfAlias('webroot') . self::THUMB_PATH . $strSource);

                    if ($field == 'image') {
                        Yii::app()->image->cropSave(Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource, 1024,1024, Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource);
                        $this->image = $strSource;
                    }

                    $this->$field = $strSource;
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * возвращает статус для таблицы
     * @return string
     */
    public function getStatusForTable()
    {
        $label = '*';
        if ($this->status == 0) {
            $label = '<span class="label label-red"> Не опубликован </div>';
        } elseif($this->status == 1){
            $label = '<span class="label label-green"> Опубликован </div>';
        }
        return $label;
    }
}
