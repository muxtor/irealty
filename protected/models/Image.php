<?php

/**
 * This is the model class for table "image".
 *
 * The followings are the available columns in table 'image':
 * @property string $id
 * @property string $type
 * @property string $file
 * @property string $parent_id
 * @property int $is_vertical_image
 */
class Image extends CActiveRecord
{
	const TYPE_REALTY = 'realty';
	const TYPE_ARTICLE = 'article';
	const TYPE_SERVICE = 'service';
	const TYPE_PROJECT_SLIDER = 'project_slider';
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'image';
	}

	public function scopes()
	{
		return array(
			'realty'=>array(
				'condition'=>'type = :type',
				'params' => array(
					':type' => Image::TYPE_REALTY
				)
			),
            'article'=>array(
                'condition'=>'type = :type',
                'params' => array(
                    ':type' => Image::TYPE_ARTICLE
                )
            ),
			'service'=>array(
				'condition'=>'type = :type',
				'params' => array(
					':type' => Image::TYPE_SERVICE
				)
			),
			'slider'=>array(
				'condition'=>'type = :type',
				'params' => array(
					':type' => Image::TYPE_PROJECT_SLIDER
				)
			),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, file, parent_id', 'required'),
			array('type', 'length', 'max'=>255),
			array('file', 'length', 'max'=>512),
			array('parent_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, file, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Parent table (article_service | project_slider)',
			'file' => 'Image path',
			'parent_id' => "Parent item's id",
			'is_vertical_image' => "Slider's first image flag"
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('parent_id',$this->parent_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Image the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function afterDelete()
	{
		// $image = getcwd().Project::SLIDER_IMAGE_PATH.$this->file;
		// $thumb = getcwd().Project::SLIDER_THUMB_PATH.$this->file;
		
		// if (file_exists($image))
		// 	unlink($image);
		
		// if (file_exists($thumb))
		// 	unlink($thumb);

		return parent::afterDelete();
	}
}
