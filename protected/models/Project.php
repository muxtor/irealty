<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 * @property string $id
 * @property string $title
 * @property string $adress
 * @property string $url
 * @property string $parameters
 * @property string $realize_period
 * @property string $main_top_image
 * @property string $text
 * @property string $similar_projects
 * @property string $indexpage_image
 * @property integer $print_on_indexpage
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $portfolio_image
 */
class Project extends CActiveRecord
{
	const IMAGE_PATH = '/uploads/projects/';
	const THUMB_PATH = '/uploads/projects/.thumbs/'; // admin thumb
	const IMAGE_PORTFOLIO_PATH = '/uploads/projects/portfolio_preview/'; // * x 50
	const INDEXPAGE_THUMB_PATH = '/uploads/projects/indexpage_thumb/'; // 100 x 58
	const SLIDER_THUMB_PATH = '/uploads/projects/slider/.thumbs/'; // admin thumb
	const SLIDER_IMAGE_PATH = '/uploads/projects/slider/';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project';
	}

	public function scopes()
	{
		return array(
			'favorite'=>array(
				'condition'=>'print_on_indexpage = 1',
			)
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, adress, parameters, realize_period, text, meta_title, meta_description, meta_keywords, main_top_image', 'required'),
			array('url', 'unique'),
			array('print_on_indexpage', 'boolean'),
			array('title, adress, url, parameters, realize_period, main_top_image, similar_projects, indexpage_image, meta_title, meta_keywords, portfolio_image', 'length', 'max'=>255),
			array('main_top_image, indexpage_image', 'file', 'allowEmpty'=>true,'types'=>'jpg, gif, png, jpeg'),
			// array('main_top_image', 'required', 'on' => 'insert'),
			array('print_on_indexpage', 'isFavorite'),

			array('id, title, adress, url, parameters, realize_period, main_top_image, text, similar_projects, indexpage_image, print_on_indexpage, meta_title, meta_description, meta_keywords, portfolio_image', 'safe', 'on'=>'search'),
		);
	}

	public function isFavorite($attribute)
	{
		if ($this->$attribute == 1) {
			
			if (empty($this->indexpage_image))
				$this->addError('indexpage_image', 'Необходимо заполнить "'.$this->getAttributeLabel('indexpage_image').'"');
			if (empty($this->indexpage_desc1))
				$this->addError('indexpage_desc1', 'Необходимо заполнить "'.$this->getAttributeLabel('indexpage_desc1').'"');
			if (empty($this->indexpage_desc2))
				$this->addError('indexpage_desc2', 'Необходимо заполнить "'.$this->getAttributeLabel('indexpage_desc2').'"');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'specs' => array(self::HAS_MANY, 'ProjectSpec', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заголовок',
			'adress' => 'Адрес',
			'url' => 'URL',
			'parameters' => 'Параметры',
			'realize_period' => 'Период выполнения работ',
			'main_top_image' => 'Основная картинка',
			'text' => 'Текстовый блок',
			'similar_projects' => 'Интерьеры в этом стиле',
			'meta_title' => 'Meta-title',
			'meta_description' => 'Meta-description',
			'meta_keywords' => 'Meta-keywords',
			'portfolio_image' => 'Изображение для портфолио',
			'print_on_indexpage' => 'Отображать на главной',
			'indexpage_image' => 'Фон (главная страница)',
			'indexpage_desc1' => 'Описание 1 (главная страница)',
			'indexpage_desc2' => 'Описание 2 (главная страница)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($id = null)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('adress',$this->adress,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('parameters',$this->parameters,true);
		$criteria->compare('realize_period',$this->realize_period,true);
		$criteria->compare('main_top_image',$this->main_top_image,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('similar_projects',$this->similar_projects,true);
		$criteria->compare('indexpage_image',$this->indexpage_image,true);
		$criteria->compare('print_on_indexpage',$this->print_on_indexpage);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('portfolio_image',$this->portfolio_image,true);

		if ($id !== null) {
			$criteria->condition = 'id != :id';
			$criteria->params = array(
				':id' => $id
			);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id ASC',
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getItemsList()
	{
		$models = self::model()->findAll(array(
			'select' => 'id, adress, portfolio_image, url',
			'order' => 'id DESC',
		));

		return $models;
	}

	public function beforeValidate() 
	{
		$images = array();
		$indexpage_image = CUploadedFile::getInstance($this,'indexpage_image');
		if (empty($this->indexpage_image) || !empty($indexpage_image))
			$images['indexpage_image'] = $indexpage_image;

		$main_top_image = CUploadedFile::getInstance($this,'main_top_image');
		if (empty($this->main_top_image) || !empty($main_top_image))
			$images['main_top_image'] = $main_top_image;

		if (!empty($images)) {

			foreach ($images as $field => $model) {
				if($model !== null){
					$strSource = uniqid() . '.' . $model->getExtensionName();
					$imagePath = Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource;
					$model->saveAs($imagePath);

					Yii::app()->image->cropSave(Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource, 300,200, Yii::getPathOfAlias('webroot') . self::THUMB_PATH . $strSource);

					if ($field == 'main_top_image') {
						Yii::app()->image->cropSave(Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource, 800,1024, Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource);
						Yii::app()->image->cropSave(Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource, 1200,50, Yii::getPathOfAlias('webroot') . self::IMAGE_PORTFOLIO_PATH . $strSource, true);
						$this->portfolio_image = $strSource;
					} elseif ($field == 'indexpage_image') {
						Yii::app()->image->cropSave(Yii::getPathOfAlias('webroot') . self::IMAGE_PATH . $strSource, 100,58, Yii::getPathOfAlias('webroot') . self::INDEXPAGE_THUMB_PATH . $strSource);
					}

					$this->$field = $strSource;
				}
			}
		}

		return parent::beforeValidate();
	}

	protected function beforeSave()
	{
		$this->url = (empty($this->url)) ? LString::urlAlias($this->title) : LString::urlAlias($this->url);
		return parent::beforeSave();
	}

	/**
	 * Returns favourites array divided by image and text values
	 * @return array
	 */
	public static function getFavorites()
	{
		$favourites = array();
		
		$models = Project::model()->favorite()->findAll(array(
			'select' => 'id, title, indexpage_image, indexpage_desc1, indexpage_desc2',
			'order' => 'id DESC',
		));

		foreach ($models as $model) {
			$favourites[$model->id]['title'] = $model->title;
			$favourites[$model->id]['background'] = $model->indexpage_image;
			$favourites[$model->id]['desc1'] = $model->indexpage_desc1;
			$favourites[$model->id]['desc2'] = $model->indexpage_desc2;
		}

		return $favourites;
	}

	public function getProjectsActiveDataProvider($id = null)
	{
		$criteria = new CDbCriteria;

		$projects = new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns model's sliders with images as associative array
	 * This uses instead of relation
	 * @return array
	 */
	public function getSliders()
	{
		$project_id = $this->id;
		$sliders = array();

		$sliderModels = ProjectSlider::model()->findAllByAttributes(array('project_id' => $project_id));

		if (!empty($sliderModels)) {
			foreach ($sliderModels as $slider) {
				$images = ProjectSlider::getImages($slider->id);
				
				if (!empty($images) || (count($images) == 3) )
					$sliders[$slider->id] = $images;
			}
		}

		return $sliders;
	}

	/**
	 * Returns model's images
	 * This uses instead of relation
	 * @return CActiveRecord
	 */
	public function getImages()
	{
		$id = $this->id;
		$type = Image::TYPE_PROJECT_SLIDER;

		return Image::model()->$type()->findAllByAttributes(array('parent_id' => $id));
	}
}
