<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $login
 * @property string $pswd
 * @property string $name
 * @property integer $role
 * @property integer $activeted
 * @property integer $smsCode
 * @property integer $phone
 * @property integer $smsSendDate
 */
class User extends CActiveRecord
{
	const PSWD_SALT = 'ddsakl4235mv/?/213';
	const ROLE_ADMIN = '1';
	const ROLE_USER = '0';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, pswd, name', 'required'),
			array('role, login', 'numerical', 'integerOnly'=>true),
			array('login, name', 'length', 'max'=>18),
            array('login', 'checkLogin'),
			array('phone', 'length', 'max'=>14),
			array('activeted, smsCode, phone, smsSendDate, role', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login, name, role, activeted, smsCode, phone, smsSendDate', 'safe', 'on'=>'search'),
		);
	}

    public function checkLogin()
    {
        if(!empty($this->login)){
            //$model= new User();
            //$model = call_user_func_array(array(&$model, 'model'), array());//$args = массив аргументов
            $criteria = new CDbCriteria();
            $criteria->addSearchCondition('login', str_replace('+','',$this->login), true);
            $usered = User::model()->find($criteria);

            if (strlen($this->login) < 12) {
                $labels = $this->attributeLabels();
                $this->addError('login', 'Номер должен быть не менее 12 символов(например: +7(914)100-10-10)');
            }elseif(substr($this->login,0,2)!='+7') {
                $this->addError('login', 'Номер должен начать с +7 (например: +7(914)100-10-10)');
            }elseif($usered!=null AND str_replace('+','',$usered->login)==str_replace('+','',$this->login)) {
                $this->addError('login', 'Профиль с таким номер существует');
            }
        }

    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Ваш номер',
			'pswd' => 'Пароль',
			'name' => 'Имя',
			'role' => 'Администратор',
			'activeted' => 'Номер потвержден',
			'smsCode' => 'СМС код',
			'phone' => 'Телефон',
			'smsSendDate' => 'Дата отправка СМС код',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('pswd',$this->pswd,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('activeted',$this->activeted);
		$criteria->compare('smsCode',$this->smsCode);
		$criteria->compare('phone',$this->phone);
		$criteria->compare('smsSendDate',$this->smsSendDate);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		//$this->pswd = md5(md5(self::PSWD_SALT.$this->pswd));
        $this->phone = $this->login;
        return parent::beforeSave();
	}

    public function Balance(){
        $payed = Pays::model()->findByAttributes(array('userId'=>$this->id), 'status=1 AND outDate >= "'.date('Y-m-d H:i:s', time()).'"');

        if($payed!==null AND $payed->outDate > date('Y-m-d H:i:s', time())){
            $money = $payed->payPrice;
        }else{
            $money = 0;
        }
        return $money .' руб';
    }
}
