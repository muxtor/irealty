<?php

/**
 * This is the model class for table "project_similar".
 *
 * The followings are the available columns in table 'project_similar':
 * @property string $id
 * @property string $project_id
 * @property string $similar_project_id
 */
class SimilarProject extends CActiveRecord
{
	public function tableName()
	{
		return 'project_similar';
	}

	public function rules()
	{
		return array(
			array('project_id, similar_project_id', 'required'),
			array('project_id, similar_project_id', 'length', 'max'=>10),
			// array('id, project_id, similar_project_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => 'Project',
			'similar_project_id' => 'Similar Project',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('project_id',$this->project_id,true);
		$criteria->compare('similar_project_id',$this->similar_project_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getSimilarItemsListById($id)
	{
		$field = 'similar_project_id';

		$criteria = new CDbCriteria;
		$criteria->select = 'project_id, similar_project_id';
		$criteria->condition = 'project_id = :project_id';
		$criteria->params = array(
			':project_id' => $id
		);
		
		$projectsList = SimilarProject::model()->findAll($criteria);

		// through search
		if (empty($projectsList)) {
			$criteria->condition = 'similar_project_id = :similar_project_id';
			$criteria->params = array(
				':similar_project_id' => $id
			);
			$projectsList = SimilarProject::model()->findAll($criteria);
			$field = 'project_id';
		}

		$similarProjectIds = array();
		
		foreach ($projectsList as $project) {
			$similarProjectIds[] = $project->$field;
		}

		return $similarProjectIds;
	}
}
