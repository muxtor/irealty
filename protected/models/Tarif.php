<?php

/**
 * This is the model class for table "tarif".
 *
 * The followings are the available columns in table 'tarif':
 * @property integer $id
 * @property integer $title
 * @property double $price
 * @property double $oldprice
 * @property integer $srok
 * @property integer $srokTitle
 * @property integer $sroktype
 * @property integer $active
 * @property integer $color
 */
class Tarif extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, price, srok, sroktype', 'required'),
			array('srok, sroktype, active', 'numerical', 'integerOnly'=>true),
			array('price, oldprice', 'numerical'),
			array('color,srokTitle', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, price, oldprice, srok, srokTitle, sroktype, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название тарифа',
			'price' => 'Цена тарифа',
			'oldprice' => 'Старая цена',
			'srok' => 'Срок',
			'srokTitle' => 'Час/день',
			'sroktype' => 'Тип срок',
			'active' => 'Опубликован',
			'color' => 'Цвет',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title);
		$criteria->compare('price',$this->price);
		$criteria->compare('oldprice',$this->oldprice);
		$criteria->compare('srok',$this->srok);
		$criteria->compare('srokTitle',$this->srokTitle);
		$criteria->compare('sroktype',$this->sroktype);
		$criteria->compare('active',$this->active);
		$criteria->compare('color',$this->color);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    /**
     * возвращает статус для таблицы
     * @return string
     */
    public function getStatusForTable()
    {
        $label = '*';
        if ($this->active == 0) {
            $label = '<span class="label label-red"> Нет </div>';
        } elseif($this->active == 1){
            $label = '<span class="label label-green"> Да </div>';
        }
        return $label;
    }
}
