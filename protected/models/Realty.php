<?php

/**
 * This is the model class for table "realty".
 *
 * The followings are the available columns in table 'realty':
 * @property integer $id
 * @property integer $userId
 * @property integer $rooms
 * @property integer $type
 * @property integer $price
 * @property integer $ploshad
 * @property string $phone
 * @property integer $rayon
 * @property string $street
 * @property string $home
 * @property string $korpus
 * @property string $etaj
 * @property string $kvartira
 * @property string $year
 * @property double $ox
 * @property double $oy
 * @property string $image
 * @property integer $vip
 * @property integer $hot
 * @property integer $status
 * @property string $created
 * @property string $showphone
 *
 * The followings are the available model relations:
 * @property RealtyType $type0
 * @property Rayon $rayon0
 */

class Realty extends CActiveRecord
{
    const IMAGE_FOLDER = '/uploads/images/';
    const THUMB_FOLDER = '/uploads/images/.thumbs/';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'realty';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rooms, type, price, ploshad, rayon, street, price, ploshad, phone', 'required'),
			array('userId, rooms, year, type, price, ploshad, rayon, vip, hot, status', 'numerical', 'integerOnly'=>true),
			array('phone', 'length', 'max'=>32),
			array('street, home, image', 'length', 'max'=>128),
			array('korpus, etaj, kvartira', 'length', 'max'=>45),
			array('lat, lng', 'length', 'max'=>20),
			array('lat, lng, created, showphone', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, year, userId, rooms, type, price, ploshad, phone, rayon, street, home, korpus, etaj, kvartira, ox, oy, image, vip, hot, status, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'types' => array(self::BELONGS_TO, 'RealtyType', 'type'),
			'rayons' => array(self::BELONGS_TO, 'Rayon', 'rayon'),
		);
	}

    /**
     * Returns model's images
     * This uses instead of relation
     * @return CActiveRecord
     */
    public function getImages()
    {
        $id = $this->id;
        return Image::model()->findAllByAttributes(array('parent_id' => $id,'type' => 'realty'));
    }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userId' => 'User',
			'rooms' => 'Сколько комнат?',
			'type' => 'Тип дома',
			'price' => 'Цена',
			'ploshad' => 'Площадь',
			'phone' => 'Ваш номер',
			'rayon' => 'Район',
			'street' => 'Улица',
			'home' => 'Дом',
			'korpus' => 'Корпус',
			'etaj' => 'Этаж',
			'kvartira' => 'Квартира',
			'year' => 'Год постройки',
			'lat' => 'Широта',
			'lng' => 'Долгота',
			'image' => 'Рисунок',
			'vip' => 'Vip?',
			'hot' => 'Горячая предложения?',
			'status' => 'Опубликован?',
			'created' => 'Дата создания',
			'showphone' => 'Показать номер?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userId',$this->userId);
		$criteria->compare('rooms',$this->rooms);
		$criteria->compare('type',$this->type);
		$criteria->compare('price',$this->price);
		$criteria->compare('ploshad',$this->ploshad);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('rayon',$this->rayon);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('home',$this->home,true);
		$criteria->compare('korpus',$this->korpus,true);
		$criteria->compare('etaj',$this->etaj,true);
		$criteria->compare('kvartira',$this->kvartira,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lng',$this->lng);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('vip',$this->vip);
		$criteria->compare('hot',$this->hot);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('showphone',$this->showphone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Realty the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function myCriteria($conditions){
        if(isset($_GET['filtr'])){
            $filtr = $_GET['filtr'];
            $conditions .= ' AND price >= '.$filtr['priceStart'];
            $conditions .= ' AND price <= '.$filtr['priceEnd'];
            $conditions .= ' AND ploshad >= '.$filtr['ploshadStart'];
            $conditions .= ' AND ploshad <= '.$filtr['ploshadEnd'];

            //Tip dom
            if(isset($filtr['tip'])){
                if (is_array($filtr['tip'])) {
                    $tipIds = implode(',', $filtr['tip']);
                }
                if(isset($_GET['ajax'])){
                    $tipIds = substr($tipIds,1);
                }
                $conditions .= ' AND type IN ('.$tipIds.')';
            }

            //Rooms
            if(isset($filtr['room'])){
                if (is_array($filtr['room'])) {
                    $roomIds = implode(',', $filtr['room']);
                }
                if(isset($_GET['ajax'])){
                    $roomIds = substr($roomIds,1);
                }
                $conditions .= ' AND rooms IN ('.$roomIds.')';
            }

            //Rayon
            if(isset($filtr['rayon'])){
                if (is_array($filtr['rayon'])) {
                    $rayonIds = implode(',', $filtr['rayon']);
                }
                if(isset($_GET['ajax'])){
                    $rayonIds = substr($rayonIds,1);
                }
                $conditions .= ' AND rayon IN ('.$rayonIds.')';
            }

        }
        return $conditions;
    }

    /**
     * возвращает статус для таблицы
     * @return string
     */
    public function getStatusForTable()
    {
        $label = '*';
        if ($this->status == 0) {
            $label = '<span class="label label-red"> Новый </div>';
        } elseif($this->status == 1){
            $label = '<span class="label label-green"> Опубликован </div>';
        }
        return $label;
    }
    public function getVipForTable()
    {
        $label = '*';
        if ($this->vip == 0) {
            $label = '<span class="label label-red"> Нет </div>';
        } elseif($this->vip == 1){
            $label = '<span class="label label-green"> Да </div>';
        }
        return $label;

    }
    public function getHotForTable()
    {
        $label = '*';
        if ($this->hot == 0) {
            $label = '<span class="label label-red"> Нет </div>';
        } elseif($this->hot == 1){
            $label = '<span class="label label-green"> Да </div>';
        }
        return $label;

    }
    public function getShowphoneForTable()
    {
        $label = '*';
        if ($this->showphone == 0) {
            $label = '<span class="label label-red"> Нет </div>';
        } elseif($this->showphone == 1){
            $label = '<span class="label label-green"> Да </div>';
        }
        return $label;

    }
}
