<?php

/**
 * This is the model class for table "article_service".
 *
 * The followings are the available columns in table 'article_service':
 * @property integer $id
 * @property string $type
 * @property string $title
 * @property string $url
 * @property string $text
 * @property string $created_on
 * @property string $author_info
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property integer $print_on_indexpage
 */
class ArticleService extends CActiveRecord
{
	const CONTENT_ARTICLE = 'article';
	const CONTENT_SERVICE = 'service';
	const CONTENT_ARTICLE_NAME = 'Статьи';
	const CONTENT_SERVICE_NAME = 'Услуги';
	const IMAGE_FOLDER = '/uploads/images/';
	const THUMB_FOLDER = '/uploads/images/.thumbs/';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article_service';
	}

	public function scopes()
	{
		return array(
			'article'=>array(
				'condition'=>'type = :type',
				'params' => array(
					':type' => ArticleService::CONTENT_ARTICLE
				)
			),
			'service'=>array(
				'condition'=>'type = :type',
				'params' => array(
					':type' => ArticleService::CONTENT_SERVICE
				)
			),
			'lastRecord' => array(
				'order' => 'id DESC',
				'limit' => 1,
			),
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, text, author_info, meta_title, meta_description, meta_keywords', 'required'),
			array('print_on_indexpage', 'numerical', 'integerOnly'=>true),
			array('url', 'unique'),
			array('type, title, url, author_info, meta_title, meta_keywords', 'length', 'max'=>255),
			// array('banner', 'file','allowEmpty'=>true,'types'=>'jpg, gif, png'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, title, url, text, created_on, author_info, meta_title, meta_description, meta_keywords, print_on_indexpage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'images'=>array(self::HAS_MANY,'Image','parent_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Тип контента (Статья | Услуга)',
			'title' => 'Заголовок',
			'url' => 'URL',
			'text' => 'Текстовый блок',
			'created_on' => 'Дата создания',
			'author_info' => 'Автор',
			'meta_title' => 'Meta-title',
			'meta_description' => 'Meta-description',
			'meta_keywords' => 'Meta-keywords',
			'print_on_indexpage' => 'Отображать на главной',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('author_info',$this->author_info,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('print_on_indexpage',$this->print_on_indexpage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArticleService the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		$this->url = (empty($this->url)) ? LString::urlAlias($this->title) : LString::urlAlias($this->url);
		return parent::beforeSave();
	}

	public static function getTypeName($type)
	{
		switch ($type) {
			case self::CONTENT_ARTICLE:
				return self::CONTENT_ARTICLE_NAME;
			break;
			
			case self::CONTENT_SERVICE:
				return self::CONTENT_SERVICE_NAME;
			break;

			default:
				return null;
			break;
		}
	}

	/**
	 * Returns articles list for index page (right block)
	 * @return array
	 */
	public static function getArticleList()
	{
		return ArticleService::model()->article()->findAll(array(
			'select' => 'id, title, url',
			'order' => 'created_on DESC'
		));
	}

	/**
	 * Returns model's images
	 * This uses instead of relation
	 * @return CActiveRecord
	 */
	public function getImages()
	{
		$id = $this->id;
		$type = 'realty';

		return Image::model()->$type()->findAllByAttributes(array('parent_id' => $id));
	}
}
