<?php
/**
 * -----------------------------------------------------------------------------
 * helpers для ajax ответов
 * 
 * RAjax::success(array('messages' => 'запись успешно сохранена'));
 * 
 * RAjax::error(array('errors' => array(
 *  array(
 *    'id'=>'',
 *    'messages' => 'error1'
 *  ),
 * array(
 *  'id'=>'',
 *  'messages' => 'error1'
 *  ),
 * 
 * )));
 * 
 * return
 *  {"response":{"status":"error","data":{"messages":""}}}
 * 
 * RAjax::data(array('messages'=>'test'));
 * 
 * RAjax::dataText('messages');
 * 
 * -----------------------------------------------------------------------------
 *
 * 
 * @name RAjax
 * @package helpers
 * @version 0.1
 * @author timur
 * 
 */


class RAjax
{    
    /**
     *
     * @param type $data array 
     */
    public static function success($data = null)
    {
        echo CJSON::encode(array('response' => array(
                            'status' => 'success',
                            'data'   => $data,
        )));

        Yii::app()->end();
    }

    /**
     *
     * @param type $data array 
     */
    public static function error($data = null)
    {
        echo CJSON::encode(array('response' => array(
                            'status' => 'error',
                            'data'   => $data,
        )));

        Yii::app()->end();
    }
    
    /**
    *
    * @param type $data array 
    */
    public static function data($data)
    {
        echo CJSON::encode($data);
        Yii::app()->end();
    }

    /**
     *
     * @param type $data text 
     */
    public static function dataText($data)
    {
        echo $data;
        Yii::app()->end();
    }
}