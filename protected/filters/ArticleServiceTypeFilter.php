<?php
class ArticleServiceTypeFilter extends CFilter {
	public $except;

	public function preFilter($filterChain) {
		$exceptActions = $this->except;

		foreach ($exceptActions as $action) {
			if (Yii::app()->controller->action->id == $action)
				return true;
		}

		$_param = Yii::app()->request->getParam('type');
		$type = $_param;
		$typeName = '';
		
		switch ($_param) {
			case ArticleService::CONTENT_ARTICLE:
				$typeName = ArticleService::CONTENT_ARTICLE_NAME;
			break;
			
			case ArticleService::CONTENT_SERVICE:
				$typeName = ArticleService::CONTENT_SERVICE_NAME;
			break;

			default:
				throw new CHttpException(404, "Неправильно набран адрес или такой страницы не существует.");
			break;
		}

		ArticleServiceController::$type = $type;
		ArticleServiceController::$typeName = $typeName;
		return true;
	}

	public function postFilter($filterChain) {}
}