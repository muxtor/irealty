<?php
$config	= (!defined('PRODUCTION')) ? require dirname(__FILE__) . '/_db_dev.php' : require dirname(__FILE__) . '/_db_production.php';

return array(
	'theme'=> 'main',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Realty',
	'language' => 'ru',
	// preloading 'log' component
	'preload'=>array(
		'log',
		'bootstrap',
	),

	'aliases' => array(
        // yiistrap configuration
        'ex-bootstrap' => dirname(__FILE__) . '/../extensions/ex-bootstrap',
        // 'bootstrap' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'yii-bootstrap-3-module'
    ),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.filters.ArticleServiceTypeFilter',
		'application.helpers.*',
		'application.modules.core.helpers.*',
		'application.modules.core.components.*',
		'application.modules.core.controllers.*',
		// 'application.vendor.GifCreator.src.GifCreator.GifCreator',
        // 'bootstrap.behaviors.*',
        // 'bootstrap.helpers.*',
        // 'bootstrap.widgets.*'
	),
	
	'modules'=> array(
		'admin',
		'core',
		// uncomment the following to enable the Gii tool
        'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('192.168.1.2','::1'),
		),
	),

	'components'=>array(
        /*'sms' => array(
            'class' => 'application.extensions.smsru.Sms',
            'api_id' => '7cf6b9cc-9f0c-68f4-01ae-5418e114c3e6',
            'login' => '9247652654',
            'password' => 'rieltoroff2015'
        ),*/
		'mailer' => array(
	      'class' => 'application.extensions.mailer.EMailer',
	      'pathViews' => 'application.views.email',
	      'pathLayouts' => 'webroot.themes.main.views.email.layouts'
	    ),
		'authManager' => array(
		    // Будем использовать свой менеджер авторизации
		    'class' => 'PhpAuthManager',
		    // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
		    'defaultRoles' => array('guest'),
		),
		'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true,
        ),
        'image' => array(
            'class' => 'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            'driver' => 'GD',
            // ImageMagick setup path
            'params' => array('directory' => '/opt/local/bin'),
        ),
        'ih'=>array(
	        'class'=>'application.components.CImageHandler',
	    ),
    	'user'=>array(
			'class' => 'WebUser',
			'allowAutoLogin'=>true,
			'loginUrl'=>array('admin/default/login'),
			'returnUrl'=>array('admin/default'),
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
            // 'urlSuffix' => '.aspx',
			'rules'=>array(
				// 'gii' => 'gii',
				'admin' => 'admin/default/index',
				'<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>/<id>',
				'<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
				
				'<type:(service|article)>/<url>'=>'site/articleService',
				'<action:(project)>/<url>'=>'site/<action>',
				
				'<type:(service|article)>'=>'site/articleService',
				'<action:(portfolio)>'=>'site/<action>',
				'o-nas'=>'site/about',
				'user'=>'user/index',
				'pay'=>'pay/index',
				'realty'=>'realty/index',
				'buy'=>'realty/buy',
				'sell/add'=>'realty/create',
				'sell/<id:\d+>'=>'realty/view',
				''=>'realty/index',
				'/<url>'=>'site/page',

				// 'gif'=>'dev/gif',

				// 'site/<action:(project)>/<url>'=>'site/<action>',
				// 'site/<action:(portfolio|articleService)>'=>'<controller>/<action>',
				// '<url>'=>'site/index',
				// 
				// '<lang:(ru|kz)>/<action:(goszakup|gallery|comments|gazeta)>'=>'main/<action>',
				// '<lang:(ru|kz)>/<action:(news)>/<page:\d+>/<id:\d+>'=>'main/<action>',
				// '<lang:(ru|kz)>/<action:(news)>/<page:\d+>'=>'main/<action>',
				// '<lang:(ru|kz)>/<name:\w.+>/<id:\d+>' => 'main/index',
				// '<lang:(ru|kz)>/<name:\w.+>'=> 'main/index',

				// 'search/<category:\d+>/<priceot:\d+>/<pricedo:\d+>/<filter1:\d+>/<filter2:\d+>/<filter3:\d+>/<filter4:\d+>/<filter5:\d+>/<filter6:\d+>/<distance:\d+>'=>'site/search',
			),
		),
		
		'db' => $config['db'],
		
		'errorHandler'=>array(
			'errorAction'=> 'site/error',
			// 'errorAction'=> $controller.'/error',
		),
		
		'log'=> $config['log'],

		'request' => array(
            // 'baseUrl' => 'http://hospital_child/',
        ),
	),

	'params' => array(
	),
);
