<?php
	$config = array();
	$config['db'] = array(
        'connectionString' => 'mysql:host=localhost;dbname=muxtor_irealty',
        'emulatePrepare' => true,
        'username' => '045617529_realty',
        'password' => 'pass12345',
        'charset' => 'utf8',
        'tablePrefix' => '',
	);

	$config['log'] = array(
		'class'=>'CLogRouter',
		'routes'=>array(
			array(
				'class'=>'CFileLogRoute',
				'levels'=>'error, warning',
			),
			// uncomment the following to show log messages on web pages
			
			// array(
			// 	'class'=>'CWebLogRoute',
			// ),
			
		),
	);

	return $config;