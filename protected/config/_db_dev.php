<?php
	$config = array();
	
	$config['db'] = array(
		'connectionString' => 'mysql:host=localhost;dbname=irealty',
		'emulatePrepare' => true,
		'username' => 'irealty',
		'password' => 'c24HbTGPpsCrAARu',
		'charset' => 'utf8',
		'tablePrefix' => '',

		// включаем профайлер
		'enableProfiling'=>true,
        // показываем значения параметров
        'enableParamLogging' => true,
	);

	$config['log'] = array(
		'class' => 'CLogRouter',
        'routes' => array(
			// 'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
		// phpconsole
            //'class' => 'ext.php-console-yii.src.extensions.phpconsole.PhpConsoleLogRoute',
            ///* Default options:
            //'isEnabled' => true,
            //'handleErrors' => true,
            //'handleExceptions' => true,
            //'sourcesBasePath' => $_SERVER['DOCUMENT_ROOT'],
            //'phpConsolePathAlias' => 'application.vendors.PhpConsole.src.PhpConsole',
            //'registerHelper' => true,
            //'serverEncoding' => null,
            //'headersLimit' => null,
            //'password' => null,
            //'enableSslOnlyMode' => false,
            //'ipMasks' => array(),
            //'dumperLevelLimit' => 5,
            //'dumperItemsCountLimit' => 100,
            //'dumperItemSizeLimit' => 5000,
            //'dumperDumpSizeLimit' => 500000,
            //'dumperDetectCallbacks' => true,
            //'detectDumpTraceAndSource' => true,
            //'isEvalEnabled' => false,
            //*/
        ),
 	);

	return $config;