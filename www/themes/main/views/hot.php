<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 01.03.15
 * Time: 12:44
 */

$datas = Realty::model()->findAll(array(),'status=1 AND hot=1');
$counts = count($datas);
$i=1;
?>
<?php if($counts>0){?>
<!--Recommend block-->
<div class="recommend-block">
    <div class="container2">
        <div class="recommend">
            <div class="r-title">Обратите внимание</div>
            <div class="item-scroll">
                <div class="ritems" style="width: <?php if($counts>10) {echo 10*248;}else{echo $counts*248;} ?>px;">
                    <?php foreach($datas as $data){ if($i>10){ continue; break; } ?>
                    <div class="item <?php echo $data->vip==1?'vip':''?>">
                        <?php echo $data->vip==1?'<div class="stick-vip"></div>':''?>
                        <div class="img">
                            <a href="<?php echo Yii::app()->createUrl('sell/' . $data->id); ?>" style="text-decoration: none;">
                            <div class="hovered">
                                <div class="blue-bg"></div>
                                <div class="item-link"><a href="<?php echo Yii::app()->createUrl('sell/' . $data->id); ?>" class="button button-grey">Посмотреть</a></div>
                            </div>
                            <?php
                            $image = Image::model()->findByAttributes(array('parent_id'=>$data->id),'type="realty"');
                            if($image!==null){ ?>
                                <img src="<?php echo PreDefineUrl::RealtyItemsThumb().Realty::IMAGE_FOLDER.$image->file; ?>" alt="" />
                            <?php }else{ ?>
                                <img src="<?php echo PreDefineUrl::RealtyItemsThumb(); ?>/images/nophoto.jpg" alt="" />
                            <?php } ?>
                            </a>
                        </div>
                        <div class="corner-blue"></div>
                        <div class="item-info">
                            <div class="item-params">
                                <div class="item-param">Комнат: <b class="pull-right"><?php echo $data->rooms; ?> комнатная</b></div>
                                <div  class="item-param">Кв.м.: <b class="pull-right"><?php echo $data->ploshad; ?></b></div>
                            </div>
                            <div class="item-price"><?php echo number_format($data->price, 0, ' ', ' ')?> <span>руб.</span></div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <script>
        (function($){
            $(window).load(function(){
                $.mCustomScrollbar.defaults.scrollButtons.enable=false; //enable scrolling buttons by default
                $.mCustomScrollbar.defaults.axis="x"; //enable 2 axis scrollbars by default
                $(".item-scroll").mCustomScrollbar({
                    theme:"rounded-dark"
                });

            });
        })(jQuery);
    </script>
</div>
<!--//Recommend block-->
<?php } ?>