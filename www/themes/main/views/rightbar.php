<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 01.03.15
 * Time: 12:59
 */ ?>
<!--Rightbar-->
<div class="rightbar">
    <?php if(isset($filtr)){?>
        <?php echo $this->renderPartial('../filtr'); ?>
    <?php } ?>

    <?php if(isset($sellBtn)){ ?>
        <div class="blocks">
            <div class="button-sell">
                <a href="#" data-toggle="modal" data-target="#sell-form"  class="button button-bg-green btn-buy">ПРОДАТЬ</a>
            </div>
        </div>
    <?php } ?>

    <?php if(isset($banner)){

        $banner = Banner::model()->findByAttributes(array(),'status=1');
        ?>
        <?php if($banner!==null){?>
            <div class="blocks">
                <a href="<?php echo $banner->link;?>" style="text-decoration: none;">
                    <div class="banner">
                        <div class="img"><?php if(!empty($banner->image)){?><img style="width: 100%;" src="<?php echo PreDefineUrl::BannerImg().Banner::THUMB_PATH.$banner->image;?>"/><?php } ?></div>
                        <div class="corner-white"></div>
                        <div class="banner-text">
                            <div class="banner-title"><?php echo $banner->title;?></div>
                            <div class="b-small-text"><?php echo $banner->text1;?></div>
                        </div>
                    </div>
                </a>
            </div>
        <?php }?>
    <?php } ?>
    <div class="clear"></div>
</div>
<!--//Rightbar-->