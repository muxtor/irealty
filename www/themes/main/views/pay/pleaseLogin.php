<div class="sell-form" style="width: 360px; text-align:center; ">
    <h1 style="font-size:18px; color: #212121; font-weight: bold; margin-bottom:30px;">Для пополнения счета Вам нужно войти или зарегистрироваться (бесплатно)</h1>
    <?php
        $this->pageTitle = 'Для пополнения счета Вам нужно войти или зарегистрироваться (бесплатно)';
    ?>
    <p><div class="logged-in">
        <div class="user-info">
            <a href="/user/login" class="to_account">Войти</a> или
            <a href="/user/signup" class="to_account">Регистрация</a>
        </div>

    </div></p>
</div>