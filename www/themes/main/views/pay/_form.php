
                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-user',
                        'action'=>'/pay',
                        'type' => 'vertical',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);  ?>
                        <?php //echo $form->radioButtonList($model, 'tarifId', CHtml::listData($tariffs, 'id' ,'price'), array('class' => '')); ?>
                        <input name="Pays[tarifId]" id="Pays_tarifId_0" value="0" type="hidden" >
                        <?php if(count($tariffs)>0){?>
                            <div class="tariffs">
                                <?php foreach($tariffs as $tarif){?>
                                    <div class="titem <?php echo $tarif->color; ?>">
                                        <h3 style="text-align: center;"><?php echo $tarif->title; ?></h3>
                                        <label for="Pays_tarifId_<?=$tarif->id;?>" style="width: 100%;">
                                            <div class="tinput">
                                                <input name="Pays[tarifId]" id="Pays_tarifId_<?=$tarif->id;?>" value="<?=$tarif->id;?>" type="radio" >
                                                <?=$tarif->srok;?>
                                                <?=$tarif->srokTitle; ?>
                                            </div>
                                            <div class="tprice">
                                                <?=$tarif->price;?> руб.
                                            </div>
                                            <div class="toldprice">
                                                <?=$tarif->oldprice>0?$tarif->oldprice.' руб.':'';?>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="clear clearfix" style="margin-bottom:5px;"></div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                    </div>


                </div>


                <!--end paped -->
                <!--end paped -->
                <div class="form-actions" style="text-align:center;">
                    <?php
                    $this->widget(
                        'bootstrap.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'label' => Yii::t('admin', 'Получить доступ'),
                            'type' => null,
                            'htmlOptions' => array(
                                'class' => 'button button-blue',
                                'value' => 'index',
                                'name' => 'submit',
                            ),
                            'size' => 'small',
                        )
                    );
                    ?>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
