<div class="sell-form" style="width: 360px; text-align:center; ">
    <h1 style="font-size:18px; color: #212121; font-weight: bold; margin-bottom:30px;">Операция прошла успешно</h1>
    <?php
        $this->pageTitle = 'Операция прошла успешно';
    ?>
    <p><div class="logged-in">
        <div class="user-info">
            Операция прошла успешно<br>
            Operation of payment is successfully completed
        </div>

    </div></p>
</div>