<?php
$controller = Yii::app()->getController();

$isIndex = ($this->action->id != $controller->defaultAction) ? false : true;
$menus = Page::model()->findAllByAttributes(array(),'is_main=1');
$setting = Setting::model()->findByPk(1);
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->renderPartial('/layouts/_head');?>
</head>
<body>
<?php echo $content; ?>
</body>
</html>
