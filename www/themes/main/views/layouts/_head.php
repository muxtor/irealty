<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 09.03.15
 * Time: 10:24
 */
$cs = Yii::app()->getClientScript();

?>
    <meta charset="utf-8" />
    <!--
             _     _
           /  \  /  \                ___   __    _
          /    \/    \   |   | \  /   |   /  \  | \
         /  /\    /\  \  |   |  \/    |   | | | |_/
        /  /  \  /  \  \ |___| /  \   |   \__/  | \
        ===========================================
        ###########################################
                © 2015 Muxtor
    -->
    <meta name="author" content="Muxtor"/>
    <meta name="contact" content="muxtorsoft@gmail.com"/>
    <title><?=CHtml::encode($this->pageTitle);?></title>
    <?php
    $cs->registerCoreScript('jquery');
    //$cs->registerCoreScript('jquery.ui');
    $cs->registerScriptFile('/lib/jquery-ui/js/jquery-ui.min.js');
    $cs->registerScriptFile('/js/scroll/jquery.mCustomScrollbar.concat.min.js');
    $cs->registerScriptFile('/js/phone/jquery.inputmask.js');
    $cs->registerScriptFile('/js/phone/jquery.bind-first-0.1.min.js');
    $cs->registerScriptFile('/js/phone/jquery.inputmask-multi.js');
    //$cs->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');
    $cs->registerCssFile('/lib/bootstrap/css/bootstrap.min.css');
    $cs->registerCssFile('/lib/jquery-ui/css/jquery-ui.min.css');
    $cs->registerCssFile('/js/scroll/jquery.mCustomScrollbar.css');
    $cs->registerCssFile('/css/style.css');
    $cs->registerCssFile('/css/grid.css');
    $cs->registerCssFile('/css/navbar.css');
    $cs->registerScriptFile('/js/site.js');
    ?>
    <script>
    var maskList = $.masksSort($.masksLoad("/js/phone/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function(maskObj, determined) {
            if (determined) {
                var hint = maskObj.name_ru;
                if (maskObj.desc_ru && maskObj.desc_ru != "") {
                    hint += " (" + maskObj.desc_ru + ")";
                }
                $("#descr").html(hint);
            } else {
                $("#descr").html("Маска ввода");
            }
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    var listRU = $.masksSort($.masksLoad("/js/phone/phones-ru.json"), ['#'], /[0-9]|#/, "mask");
    var optsRU = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: listRU,
        listKey: "mask",
        onMaskChange: function(maskObj, determined) {
            if (determined) {
                if (maskObj.type != "mobile") {
                    $("#descr").html(maskObj.city.toString() + " (" + maskObj.region.toString() + ")");
                } else {
                    $("#descr").html("мобильные");
                }
            } else {
                $("#descr").html("Маска ввода");
            }
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };
</script>
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->