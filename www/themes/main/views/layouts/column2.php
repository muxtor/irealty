<?php 
  $model = $this->requestForm;
  $this->beginContent('//layouts/main'); 
?>
<section class="text">
  <?=$content?> 
</section>
<div class="get-credit">
  <h3>Оформить онлайн заявку</h3>
  <p id="requestFormResult" style="display: none"></p>
  <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'request-form-form',
      'enableAjaxValidation'=>false,
      'enableClientValidation'=>true,
      'method' => 'POST',
      'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'afterValidate'=>'js:function(form,data,hasError){
                $.ajax({
                  "type":"POST",
                  "url":"'.CHtml::normalizeUrl(array("ajax/createRequest")).'",
                  "data": (form.serialize()),
                  "success":function(data){
                    if(!hasError) {
                      $("#requestFormResult").text("Ваша заявка успешно отправлена! Спасибо");
                      $("#requestFormResult").css("color", "green");
                      $("#requestFormResult").css("font-weight", "bold");
                      $("#request-form-form").hide();
                    } else {
                      $("#requestFormResult").text("Ошибка отправки!");
                      $("#requestFormResult").css("color", "red");
                    }
                    $("#requestFormResult").show();
                  },
                    
                });
       }'
      )
  )); ?>
    <?php echo $form->errorSummary($model); ?>
    
    <div class="row">
        <?php echo $form->labelEx($model,'amount'); ?>
        <?php echo $form->textField($model,'amount'); ?>
        <?php echo $form->error($model,'amount'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'second_name'); ?>
        <?php echo $form->textField($model,'second_name'); ?>
        <?php echo $form->error($model,'second_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name'); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'middle_name'); ?>
        <?php echo $form->textField($model,'middle_name'); ?>
        <?php echo $form->error($model,'middle_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'service_type_id'); ?>
        <?php echo $form->dropDownList($model,'service_type_id', ServiceType::getItemsForDropdowm(), array('empty' => 'Выберите услугу...')); ?>
        <?php echo $form->error($model,'service_type_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'phone'); ?>
        <?php echo $form->textField($model,'phone'); ?>
        <?php echo $form->error($model,'phone'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('', array('class' => 'send-button')); ?>
    </div>

<?php $this->endWidget(); ?>

</div>
<a href="<?=$this->createUrl('/site/calculator')?>" class="calculator-link">
    Кредитный калькулятор
</a>
<iframe class="currencyInformer" src="http://www.exdex.ru/moscow/ya_widget/?cataloxy=1" frameborder="0"></iframe>
<div class="clear"></div>
<section class="partners">
  <h2 style="text-align:center; padding:10px; margin-bottom: 12px;margin-top: 15px;">Наши партнеры</h2>
  <ul class="bxslider">
    <li><img src="/uploads/bank/bank-alfa.jpg" alt="Альфа Банк"></li>
    <li style="padding-top: 0.2%"><img src="/uploads/bank/bank-moskovskijkreditnyj.jpg"></li>
    <li><img src="/uploads/bank/bank-moskvy.jpg"></li>
    <li><img src="/uploads/bank/bank-sberbank.jpg"></li>
    <li><img src="/uploads/bank/bank-vtb24.jpg"></li>
    <li><img src="/uploads/bank/bank-unicreditbank.jpg"</li>
    <li><img src="/uploads/bank/bank-uralsib.jpg"></li>
    <li><img src="/uploads/bank/bank-gazprom.png"></li>
    <li><img src="/uploads/bank/bank-promsvyaz.jpg"</li>
  </ul>
</section>

<?php $this->endContent(); ?>

<?php Yii::app()->clientScript->registerScript(
  'slider',
  "
    $('.bxslider').bxSlider({
       responsive: true,
       slideWidth: 280,
       minSlides: 4,
       maxSlides: 4,
       slideMargin: 35,
    });
  "
);?>