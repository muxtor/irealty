<?php
    $controller = Yii::app()->getController();

    $isIndex = ($this->action->id != $controller->defaultAction) ? false : true;
    $menus = Page::model()->findAllByAttributes(array(),'is_main=1');
    $setting = Setting::model()->findByPk(1);
if(!isset($_GET['url'])){
    $_GET['url'] = '';
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->renderPartial('/layouts/_head');?>
</head>
<body>
<!--Header block-->
<div class="header">
    <div class="top-header">
        <div class="container2">
            <div class="top-menu">
                <ul>
                    <li>
                        <a href="/">Главная</a>
                    </li>
                    <?php if($menus!==null){ ?>
                        <?php
                        foreach($menus as $menu){ ?>
                            <li <?php if($_GET['url']==$menu->url){ echo 'class="active"'; }?>>
                                <a  href="/<?=$menu->url?>"><?=$menu->title?></a>
                                <div class="top-menu-sep"></div>
                            </li>
                        <?php
                        } ?>
                    <?php } ?>
                    <li>
                        <a data-toggle="modal" data-target="#sell-form" href="#">Продать</a>
                        <div class="top-menu-sep"></div>
                    </li>
                </ul>
            </div>
            <div class="top-user-block">
                <div class="logged-in">
                    <?php if(Yii::app()->user->isGuest == true){?>
                        <div class="user-info">
                            <a href="#" data-toggle="modal" data-target="#logins-form" class="to_account">Войти</a>
                            <a href="#" data-toggle="modal" data-target="#signup-form" class="to_account">Регистрация</a>
                        </div>
                        <div class="modal fade" id="logins-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width: 360px;">
                                <a class="red-close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                                <div class="modal-content">
                                    <?php $model = new LoginForm(); $model->username = 7; ?>
                                    <div class="sell-form" style="width: 360px; text-align:center; ">
                                        <h1 style="font-size:24px; color: #212121; font-weight: bold; margin-bottom:30px;">Войти в личный кабинет</h1>
                                        <?php echo $this->renderPartial('/user/_login', array('model' => $model)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="signup-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" style="width: 360px;">
                                <a class="red-close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                                <div class="modal-content">
                                    <?php $model0 = new User(); $model0->login = 7; ?>
                                    <div class="sell-form" style="width: 360px; text-align:center; ">
                                        <h1 style="font-size:24px; color: #212121; font-weight: bold; margin-bottom:30px;">Регистрация</h1>
                                        <?php
                                        echo $this->renderPartial('/user/_form', array('model' => $model0, 'icon' => 'icon-edit'));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }else{
                        $payed = Pays::model()->findByAttributes(array('userId'=>Yii::app()->user->id), 'status=1 AND outDate >= "'.date('Y-m-d H:i:s', time()).'"');

                        if($payed!==null AND $payed->outDate > date('Y-m-d H:i:s', time())){
                            $tarif = Tarif::model()->findByPk($payed->tarifId);
                            $money = $tarif->srok.' '.$tarif->srokTitle;
                        }else{
                            $money = 'не куплен';
                        }
                        ?>
                        <div class="user-info">
                            <a href="/user" class="to_account">Личный кабинет</a>
                            <div class="account_balance">Тариф: <?php echo $money; ?></div>
                        </div>
                        <div class="user-avatar"><img src="/img/user.png"/></div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header">
        <div class="container2">
            <div class="logo">
                <a href="/">
                    <b><span>Риэлторофф</span><span class="logo_color_2">.нет</span></b>
                    <div class="logo_slogan">Вся недвижимость без посредников</div>
                </a>
            </div>
            <div class="header-infos">
                <?php $doc1 = Page::model()->findByPk(6);?>
                <?php $doc2 = Page::model()->findByPk(7);?>
                <?php if($doc1!=null){?>
                <div class="hinfo">
                    <div class="pull-right"><a style="width: 130px;" href="/<?php echo $doc1->url;?>"><?php echo $doc1->title;?></a></div>
                    <div class="circled sell_buy"></div>
                </div>
                <?php } ?>
                <?php if($doc2!=null){?>
                <div class="hinfo">
                    <div class="pull-right"><a style="width: 145px;" href="/<?php echo $doc2->url;?>"><?php echo $doc2->title;?></a></div>
                    <div class="circled oformit_realty"></div>
                </div>
                <?php }?>
                <div class="hinfo">
                    <div class="pull-right phone_numed"><?=$setting->country_code?> <span class="phone_big_num"><?=$setting->phone_1?></span></div>
                    <div class="circled phone_num"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<!--//Header block-->
<div style="min-height: 200px;">
<?php echo $content; ?>
</div>
<div class="clear"></div>
<!--//Content-->


<!--Footer block-->
<div class="footer">
    <div class="container2">
        <div class="footer-menu">
            <ul>
                <?php if($menus!==null){ ?>
                    <?php
                    foreach($menus as $menu){ ?>
                        <li>
                            <a href="/<?=$menu->url?>"><?=$menu->title?></a>
                        </li>
                        <?php
                    } ?>
                <?php } ?>

            </ul>
            <div class="clear"></div>
        </div>
        <div class="footer-info">
            <div class="copyright">
                Риелторофф.нет<br/>
                Все права защищены, 2015.
            </div>
            <div class="socials">
                <a href="<?=$setting->link_vk?>" target="_blank" class="soc vk"></a>
                <a href="<?=$setting->link_ok?>" target="_blank" class="soc ok"></a>
                <a href="<?=$setting->link_fb?>" target="_blank" class="soc fb"></a>
                <a href="<?=$setting->link_tw?>" target="_blank" class="soc tw"></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--//Footer block-->
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <a class="red-close" onclick="$('#modal .modal-content').html('<div style=\' padding: 100px; text-align: center; \'>загрузка...</div>')" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
        <div class="modal-content">
            <div style="padding: 100px; text-align: center;">загрузка...</div>
        </div>
    </div>
</div>
<div class="modal fade" id="sell-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <a class="red-close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
        <div class="modal-content">
            <?php $model2 = new Realty(); $model2->phone = 7; ?>
            <div class="sell-form">
                <h1 style="font-size:24px; color: #212121; font-weight: bold; text-align:center; margin-bottom:30px;">Подать объявление о продаже</h1>
                <?php echo $this->renderPartial('/realty/_form', array('model' => $model2)); ?>
            </div>
        </div>
    </div>
</div>
<!-- Yandex.Metrika informer -->
<a style="display:block; width:0px; height:0px; overflow: hidden; visibility: hidden;" href="https://metrika.yandex.ru/stat/?id=29683320&amp;from=informer"
   target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/29683320/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                       style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:29683320,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter29683320 = new Ya.Metrika({id:29683320,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/29683320" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>