<?php 
	// TODO: Changing this construction to CMenu
	$action = Yii::app()->controller->action->id;
	$type = Yii::app()->request->getParam('type');
?>
<li <?= ($action == 'articleService' && $type == ArticleService::CONTENT_SERVICE) ? 'class="active"' : '' ?>>
	<?=CHtml::link('Услуги', array('articleService', 'type' => ArticleService::CONTENT_SERVICE))?>
</li>
<?php /*/ ?>
<li <?= ($action == 'portfolio') ? 'class="active"' : '' ?>>
<?php /*/ ?>
<li <?= ($action == 'portfolio' || $action == 'project') ? 'class="active"' : '' ?>>
	<?=CHtml::link('Портфолио', array('portfolio'))?>
</li>
<li <?= ($action == 'about') ? 'class="active"' : '' ?>>
	<?=CHtml::link('О нас', array('about'))?>
</li>
<li <?= ($action == 'articleService' && $type == ArticleService::CONTENT_ARTICLE) ? 'class="active"' : '' ?>>
	<?=CHtml::link('Статьи', array('articleService', 'type' => ArticleService::CONTENT_ARTICLE))?>
</li>