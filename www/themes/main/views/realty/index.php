<!--Sell-buy-block-->
<div class="sell-buy-block">
    <div class="container2">
        <div class="sb-buttons">
            <div class="w50p">
                <a href="/buy" class="button button-bg-blue btn-buy">КУПИТЬ</a>
                <div class="sp-info">
                    <div class="sp-num"><?php echo Realty::model()->count('status=1')?></div>
                    собственника на нашем сайте
                </div>
            </div>
            <div class="w50p">
                <a href="#" data-toggle="modal" data-target="#sell-form" class="button button-bg-green btn-buy">ПРОДАТЬ</a>
                <div class="sp-info">
                    <div class="sp-num"><?php echo $visitors; ?></div> покупателя заходит ежедневно
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="find-block">
            <form action="/site/search">
                <input class="find-input" name="q" onblur="if($(this).val()==''){$(this).val('Введите Ваш запрос. Например, “квартира однокомнатная”');}" onfocus="if($(this).val()=='Введите Ваш запрос. Например, “квартира однокомнатная”'){$(this).val('');}" value="Введите Ваш запрос. Например, “квартира однокомнатная”">
                <button class="find-button" type="submit"></button>
                <span style="margin-left: 50px"><input name="photo" type="checkbox"> Только с фото</span>
            </form>
        </div>
    </div>

</div>
<!--//Sell-buy-block-->

<!--Content-->
<div class="content">
<div class="container2">

<!--Main content-->
<div class="main-content">
<div class="ritems">
<?php

$this->widget('bootstrap.widgets.TbListView', array(
    'dataProvider'=>$data,
    'itemView'=>'_item',
    'ajaxUpdate'=>false,
    'enableSorting' => false,
    'template'=>'{pager}<div class="clear clearfix"></div>{items}<div class="clear clearfix"></div>{pager}{sorter}',
    'cssFile'=>false,
    'sorterHeader'=>'Сортировка',
    /*'sortableAttributes'=> array(
        'price'=>'Цена',
        'ploshad',

    ),*/
    //'sorterCssClass'=>'filter-menu sort',
    'pagerCssClass'=>'paginat',
    'pager'=>array(
        'maxButtonCount'=>'3',
        'header'         => '',
        /*'firstPageLabel' => '&lt;&lt;',
        'prevPageLabel'  => '<img src="images/pagination/left.png">',
        'nextPageLabel'  => '<img src="images/pagination/right.png">',
        'lastPageLabel'  => '&gt;&gt;',*/
    ),
    'htmlOptions'=>array('class'=>'',),
));
?>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<!--//Main content-->

<?php echo $this->renderPartial('../rightbar',array('filtr'=>'yes','banner'=>'yes')); ?>
</div>
</div>