<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 14.08.14
 * Time: 6:55
 */
?>
<div class="item <?php echo $data->vip==1?'vip':''?>">
    <?php echo $data->vip==1?'<div class="stick-vip"></div>':''?>
    <div class="img">
        <a href="<?php echo Yii::app()->createUrl('sell/' . $data->id); ?>" style="text-decoration: none;">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="<?php echo Yii::app()->createUrl('sell/' . $data->id); ?>" class="button button-grey">Посмотреть</a></div>
        </div>
        <?php
        $image = Image::model()->findByAttributes(array('parent_id'=>$data->id),'type="realty"');
        if($image!==null){ ?>
            <img src="<?php echo PreDefineUrl::RealtyItemsThumb().Realty::IMAGE_FOLDER.$image->file; ?>" alt="" />
        <?php }else{ ?>
            <img src="<?php echo PreDefineUrl::RealtyItemsThumb(); ?>/images/nophoto.jpg" alt="" />
        <?php } ?>
        </a>
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Комнат: <b class="pull-right"><?php echo $data->rooms; ?> комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right"><?php echo $data->ploshad; ?></b></div>
        </div>
        <div class="item-price"><?php echo number_format($data->price, 0, ' ', ' ')?> <span>руб.</span></div>
    </div>
</div>
