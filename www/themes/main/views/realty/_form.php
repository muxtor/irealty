<?php
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('/js/image.js');
$cs->registerScriptFile('/js/jquery.form.js');
?>


    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'article-service-form',
            //'type' => 'vertical',
            'action'=>'/sell/add',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,
            ),
            //'focus' => array($model, 'alias'),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data'
            ),
        )
    );
    ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="w50p">
        <div style="margin-left: 32px; float:left;">
            <?php
            echo $form->dropDownList($model, 'rooms', array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9,),
                array('class' => 'span12', 'empty' => 'Сколько комнат?'));
            ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
            <?php echo $form->dropDownList($model, 'type', CHtml::listData(RealtyType::model()->findAll(), 'id_type', 'type'), array('class' => 'span12', 'empty' => 'Тип дома')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
            <?php echo $form->dropDownList($model, 'rayon', CHtml::listData(Rayon::model()->findAll(), 'id_rayon', 'rayon'), array('class' => 'span12', 'empty' => 'Район')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
            <?php echo $form->textField($model, 'street', array('class' => 'span12', 'placeholder' => 'Улица')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
        </div>
    </div>
    <div class="w50p">
        <div style="margin-right: 32px; float:right;">
            <?php echo $form->textField($model, 'home', array('class' => 'span12', 'placeholder' => 'Дом')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
            <?php echo $form->textField($model, 'korpus', array('class' => 'span12', 'placeholder' => 'Корпус')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
            <?php echo $form->textField($model, 'etaj', array('class' => 'span12', 'placeholder' => 'Этаж')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
            <?php echo $form->textField($model, 'kvartira', array('class' => 'span12', 'placeholder' => 'Квартира')); ?>
            <div style="font-size: 11px; color: #808080;">Квартиру вы указываете только для администрации, что бы не возникло повторов</div>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
        </div>
    </div>
    <div class="clear clearfix"></div>
    <div style="border-bottom: 1px solid #d9d9d9;"></div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <div class="w50p">
        <div style="margin-left: 32px; float:left;">
            <div class="param-title">Цена</div>
            <?php echo $form->textField($model, 'price', array('class' => 'span12')); ?>
            <div class="range-metter" style="margin-top:10px;"></div>
            <div id="price"></div>
        </div>
    </div>
    <div class="w50p">
        <div style="margin-right: 32px; float:right;">
            <div class="param-title">Площадь</div>
            <?php echo $form->textField($model, 'ploshad', array('class' => 'span12')); ?>
            <div class="range-metter" style="margin-top:10px;"></div>
            <div id="ploshad"></div>
        </div>
    </div>
    <div class="clear clearfix" =""></div>
    <div style="border-bottom: 1px solid #d9d9d9;"></div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <div class="phone" style="text-align: center;">
        <?php echo $form->textField($model, 'phone', array('class' => 'span12', 'placeholder' => '+7(___)___-__-__', 'style'=>'width:300px;')); ?>
        <div class="clear clearfix" style="margin-bottom:19px;"></div>
    </div>
    <script>
        $('#Realty_phone').inputmasks(optsRU);
    </script>


    <!--end paped -->
    <div class="form-actions" style="text-align:center; position: absolute; left:0; right:0; margin: 0 auto; bottom:20px;">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'label' => Yii::t('admin', 'Добавить'),
                'type' => null,
                'htmlOptions' => array(
                    'class' => 'button button-blue',
                    'value' => 'index',
                    'name' => 'submit',
                ),
                'size' => 'small',
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>
    <div style="clear:both"></div>
    <!-- end box content -->
    <div class="uploadForm">
        <form id="formUploadFiles" enctype="multipart/form-data"
              action="<?= Yii::app()->createUrl('/realty/uploadFiles'); ?>" method="POST">
            <div class="new_advert_block">
                <div class="fileform" style="position: relative; text-align: center; margin-bottom: 15px;">
                    <input style="opacity: 0; position: absolute; width:100%; cursor: pointer;" type="file" multiple="true" name="image[]" id="upload1" onchange="this.value,1">
                    <div id="fileformlabel1" ><span style="color:#0771a2; border-bottom: dotted 1px #0771a2; font-size:14px;">Загрузить изображение</span></div>
                    <div class="selectbutton"></div>
                </div>
            </div>
            <style>
                .progress-bar span{
                    display: block;
                }
            </style>
            <div class="progress-bar orange shine" style="width:100%; background: none; border-radius: 5px; overflow: hidden; margin-bottom: 10px">
                <span style="width: 0%; height: 20px; background: green; "></span>
            </div>

            <div id="status">
                <?php
                if (Yii::app()->user->hasState('uploadedFiles')) {
                    $arrayFiles = CJSON::decode(Yii::app()->user->getState('uploadedFiles'));

                    foreach ($arrayFiles as $file) {
                        echo "<div class='file'>" .
                            CHtml::image(Yii::app()->baseUrl . Realty::THUMB_FOLDER . $file, '', array('style' => 'width:100px')) .

                            "<br><a class='deletePhoto' href='javascript:void()' data='" . $file . "'>Удалить</a></div>";
                    }

                }
                if ($model->images) {

                    foreach ($model->images as $file) {
                        echo "<div class='file'>" .
                            CHtml::image(Yii::app()->baseUrl . Realty::THUMB_FOLDER . $file->file, '', array('style' => 'width:100px')) .

                            "<br><a class='deletePhoto' href='javascript:void()' data='" . $file->file . "'>Удалить</a></div>";
                    }

                }
                ?>
            </div>
            <div style="clear:both"></div>
        </form>
        <div style="clear:both"></div>
    </div>
    <div style="clear:both"></div>