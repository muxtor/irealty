<?php

$this->pageTitle = 'Update'; ?>
<div class="sell-form">
    <h1 style="font-size:24px; color: #212121; font-weight: bold; text-align:center; margin-bottom:30px;">Подать объявление о продаже</h1>

    <?php
        echo $this->renderPartial('_form', array('model' => $model, 'icon' => 'icon-edit'));
    ?>
</div>