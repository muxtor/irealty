<?php
$pageName = 'Продать';
?>

<div class="sell-form">
    <h1 style="font-size:24px; color: #212121; font-weight: bold; text-align:center; margin-bottom:30px;">Подать объявление о продаже</h1>
    <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="flash-success">
            <?php //echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>