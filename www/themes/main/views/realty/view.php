<?php
/* @var $this RealtyController */
/* @var $model Realty */
?>

<?php echo $this->renderPartial('../mapblock',array('closed'=>'closed')); ?>
<style>
    #image-modal .modal-dialog{
        width: 80%;
        margin: 50px auto;
    }
</style>

<!--Item info block-->
<div class="iteminfo-block">
    <div class="container2">
        <!--Main content-->
        <div class="main-content">
            <?php if($bigImage!==null){ ?>
                <img style="cursor: pointer;" data-toggle="modal" data-target="#image-modal" onclick="$('#bannerimg').attr('src',$(this).attr('src'))" src="<?php echo PreDefineUrl::RealtyBigImage().Realty::IMAGE_FOLDER.$bigImage->file; ?>" alt="" />
            <?php }else{ ?>
                <img src="<?php echo PreDefineUrl::RealtyBigImage(); ?>/images/nophoto.jpg" alt="" />
            <?php } ?>
            <div class="clear"></div>
        </div>
        <!--//Main content-->

        <!--Rightbar-->
        <div class="rightbar">
            <div class="item-params">
                <div class="item-param">Комнат: <b class="pull-right"><?php echo $model->rooms;?></b></div>
                <div  class="item-param">Тип дома: <b class="pull-right"><?php echo $model->types->type;?></b></div>
                <div  class="item-param">Год постройки: <b class="pull-right"><?php echo $model->year;?></b></div>
                <?php if(!empty($model->etaj)){?>
                <div  class="item-param">Этаж: <b class="pull-right"><?php echo $model->etaj;?></b></div>
                <?php } ?>
                <div  class="item-param">Кв.м.: <b class="pull-right"><?php echo $model->ploshad;?></b></div>
                <div  class="item-param">Район: <b class="pull-right"><?php echo $model->rayons->rayon;?></b></div>
                <div  class="item-param">Улица: <b class="pull-right"><?php echo $model->street;?></b></div>
            </div>
            <div class="item-price"><?php echo number_format($model->price, 0, ' ', ' ')?> руб.</div>
            <div class="item-phone">
                <div><b>Телефон владельца:</b></div>
                <?php
                $showForm = true;
                if($model->showphone==1){
                    echo "<div class=\"isphone\">$model->phone</div>";
                    echo '<style>.item-phone {background: none!important;}</style>';
                    $showForm = false;
                }else{
                    if(Yii::app()->user->isGuest != true){
                        $payed = Pays::model()->findByAttributes(array('userId'=>Yii::app()->user->id), 'status=1');

                        if($payed!==null AND $payed->outDate > date('Y-m-d H:i:s', time())){
                            echo "<div class=\"isphone\">$model->phone</div>";
                            echo '<style>.item-phone {background: none!important;}</style>';
                            $showForm = false;
                        }
                    }
                    if($showForm==true){
                        echo '<div class="isphone">+7 (ххх) ххх-хх-хх</div>
                        <div class="wantknowphone">Хотите узнать номер?</div>';
                    }
                }

                ?>
            </div>

            <?php if($showForm==true){ ?>
                <div><a href="#" data-toggle="modal" data-target="#tarif-form" class="button button-blue" style="font-size: 16px; width: 100%; padding: 0 10px;">Узнать номер телефона</a></div>
                 <!-- Modal /pay?ajax-->
                <div class="modal fade" id="tarif-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 500px; margin-top: 8%;">
                        <a class="red-close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
                        <div class="modal-content">
                            <?php $pays = new Pays(); ?>
                            <div class="sell-form">
                                <h1 style="font-size:20px; color: #212121; font-weight: bold; text-align:center; margin-bottom:30px;">Получите доступ к <?php echo Realty::model()->count('status=1')?>
                                    собственникам с нашего
                                    сайта прямо сейчас!</h1>
                                <?php echo $this->renderPartial('../pay/_form', array('model' => $pays,'tariffs'=>Tarif::model()->findAllByAttributes(array(), 'active=1'))); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="clear"></div>
        </div>
        <!--//Rightbar-->

        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<!--//Item info block-->


<!--Content-->
<div class="content">
    <div class="container2">

        <!--Main content-->
        <div class="main-content">
            <?php $images = $model->getImages();?>
            <?php if(count($images)>0){?>
                <div class="item-photos">
                    <?php foreach($images as $image){?>
                            <div class="iphoto">
                                <a data-toggle="modal" data-target="#image-modal" onclick="$('#bannerimg').attr('src',$(this).attr('data'))" href="#" data="<?php echo PreDefineUrl::RealtyBigImage().Realty::IMAGE_FOLDER.$image->file; ?>">
                                    <div class="blue-bg"></div>
                                    <img src="<?php echo PreDefineUrl::RealtyImagesThumb().Realty::IMAGE_FOLDER.$image->file; ?>"/>
                                </a>
                            </div>
                    <?php }?>
                </div>
            <?php }else{ ?>
                <!--<div class="iphoto">
                    <a href="">
                        <div class="blue-bg"></div>
                        <img src="<?php /*//echo PreDefineUrl::RealtyItemsThumb(); */?>/images/nophoto.jpg"/>
                    </a>
                </div>-->
            <?php } ?>
            <div class="clear"></div>
        </div>
        <!--//Main content-->

        <?php echo $this->renderPartial('../rightbar',array('sellBtn'=>'yes','banner'=>'yes')); ?>

        <div class="clear"></div>

    </div>
</div>
<!--//Content-->

<?php echo $this->renderPartial('../hot'); ?>
<!-- Modal -->
<div class="modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="overflow: hidden;">
        <a class="red-close" style="position: absolute; right: 0;" onclick="$('#modal .modal-content').html('<div style=\' padding: 100px; text-align: center; \'>загрузка...</div>')" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>
        <div class="modal-content"  style="overflow: hidden;">
            <img style="width: 100%;" src="" id="bannerimg"/>
        </div>
    </div>
</div>