<?php echo $this->renderPartial('../mapblock'); ?>
<?php echo $this->renderPartial('../hot'); ?>

<!--Content-->
<div class="content">
<div class="container2">

<!--Main content-->
<div class="main-content">
<div class="ritems">

<?php
$this->widget('bootstrap.widgets.TbListView', array(
    'dataProvider'=>$data,
    'itemView'=>'_item',
    'ajaxUpdate'=>true,
    'enableSorting' => true,
    'template'=>'{sorter}{pager}<div class="clear clearfix"></div><div class="clear clearfix"></div>{items}<div class="clear clearfix"></div>{pager}',
    'cssFile'=>false,
    'sortableAttributes'=> array(
        'price'=>'Цене',
        'ploshad'=>'Площади',

    ),
    'sorterHeader'=>'<b>Сортировать по:</b>',
    //'sorterCssClass'=>'filter-menu sort',
    'pagerCssClass'=>'paginat',
    'pager'=>array(
        'maxButtonCount'=>'3',
        'header'         => '',
        /*'firstPageLabel' => '&lt;&lt;',
        'prevPageLabel'  => '<img src="images/pagination/left.png">',
        'nextPageLabel'  => '<img src="images/pagination/right.png">',
        'lastPageLabel'  => '&gt;&gt;',*/
    ),
    'htmlOptions'=>array('class'=>'',),

));
?>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<!--//Main content-->

<?php echo $this->renderPartial('../rightbar',array('sellBtn'=>'yes','banner'=>'yes')); ?>
