<?php $k = 0 ?>
<section class="projects-main">
    <h2><?=$project->title?></h2>
    <div class="date-text-block">
        <p><?=$project->realize_period?></p>
    </div>
    <div class="project-img-block">
        <?php if (!empty($project->main_top_image)): ?>
            <img src="<?=Project::IMAGE_PATH.$project->main_top_image?>" alt="">
        <?php /*/ ?>
        <?php else: ?>
            <img src="/img/project-img.png" alt="">
        <?php /*/ ?>
        <?php endif; ?>
        <?php if (!empty($project->specs)): ?>
            <div class="project-charact-block">
                <ul class="project-charact-list">
                    <?php foreach ($project->specs as $spec): ?>
                        <li>
                            <div>
                                <p><?=CHtml::encode($spec->name)?></p>
                            </div>
                            <p><?=CHtml::encode($spec->value)?> м<span class="sup">2</span></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
    <div class="project-charact-add-block">
        <p><?=$project->parameters?></p>
    </div>

    <div class="wysiwyg project-text">
        <?= $project->text ?>
    </div>

    <?php if (!empty($project->sliders)): ?>
        <!--bxSlider-->
        <div class="bxSlider">
            <ul id="slideshow">
                <?php foreach ($project->sliders as $slider): ?>
                    <li>
                        <?php foreach ($slider as $image): ?>
                            <?php if ($image['slide_order'] == 1): ?>
                                <img class="slider-img-big" src="<?=Project::SLIDER_IMAGE_PATH.$image['file']?>">
                                <div class="slider-img-wrap">
                            <?php else: ?>
                                <img class="slider-img-small" src="<?=Project::SLIDER_IMAGE_PATH.$image['file']?>">
                                <?php ++$k; ?>
                            <?php endif; ?>
                            <?php if ($k == 2): ?>
                               </div>
                            <?php $k = 0; endif; ?>
                        <?php endforeach; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div id="slide-counter"></div>
        </div>
    <?php endif;?>

    <?php if (!empty($similarProjects)): ?>
        <div class="project-add-links">
            <h4>Интерьеры в этом стиле</h4>
            <ul class="project-add-links-list">
                <?php foreach ($similarProjects as $project): ?>
                    <li><?= CHtml::link($project->adress, array('project', 'url' => $project->url)) ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
</section>
<?php
    Yii::app()->clientScript->registerScriptFile('/vendor/jquery.bxslider-4-1-2.js');
    Yii::app()->clientScript->registerScriptFile('/js/slider.js');
