<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 14.08.14
 * Time: 6:55
 */
$item = Realty::model()->findByPk($data->item_id);
?>
<div class="item <?php echo $item->vip==1?'vip':''?>">
    <?php echo $item->vip==1?'<div class="stick-vip"></div>':''?>
    <div class="img">
        <a href="<?php echo Yii::app()->createUrl('sell/' . $item->id); ?>" style="text-decoration: none;">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="<?php echo Yii::app()->createUrl('sell/' . $item->id); ?>" class="button button-grey">Посмотреть</a></div>
        </div>
        <?php
        $image = Image::model()->findByAttributes(array('parent_id'=>$item->id),'type="realty"');
        if($image!==null){ ?>
            <img src="<?php echo PreDefineUrl::RealtyItemsThumb().Realty::IMAGE_FOLDER.$image->file; ?>" alt="" />
        <?php }else{ ?>
            <img src="<?php echo PreDefineUrl::RealtyItemsThumb(); ?>/images/nophoto.jpg" alt="" />
        <?php } ?>
        </a>
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Комнат: <b class="pull-right"><?php echo $item->rooms; ?> комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right"><?php echo $item->ploshad; ?></b></div>
        </div>
        <div class="item-price"><?php echo number_format($item->price, 0, ' ', ' ')?> <span>руб.</span></div>
    </div>
</div>
