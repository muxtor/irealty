<!--Sell-buy-block-->
<div class="sell-buy-block">
    <div class="container2">
        <div class="sb-buttons">
            <div class="w50p">
                <a href="/buy" class="button button-bg-blue btn-buy">КУПИТЬ</a>
                <div class="sp-info">
                    <div class="sp-num"><?php echo Realty::model()->count('status=1')?></div>
                    собственника на нашем сайте
                </div>
            </div>
            <div class="w50p">
                <a href="/sell/add" class="button button-bg-green btn-buy">ПРОДАТЬ</a>
                <div class="sp-info">
                    <div class="sp-num">834</div>
                    покупателя заходит ежедневно
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="find-block">
            <form>
                <input class="find-input" value="Введите Ваш запрос. Например, “квартира однокомнатная”">
                <button class="find-button" type="submit"></button>
                <span style="margin-left: 50px"><input type="checkbox"> Только с фото</span>
            </form>
        </div>
    </div>

</div>
<!--//Sell-buy-block-->

<!--Content-->
<div class="content">
<div class="container2">

<!--Main content-->
<div class="main-content">
<div class="paginat">
    <a class="prev" href="#">Предыдущая</a>
    <a class="page active" href="#">1</a>
    <a class="page" href="#">2</a>
    <a class="page" href="#">3</a>
    <a class="next" href="#" >Следующая</a>
</div>

<div class="ritems">
<div class="item vip">
    <div class="stick-vip"></div>
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item vip">
    <div class="stick-vip"></div>
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="clear"></div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="clear"></div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="clear"></div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="item">
    <div class="img">
        <div class="hovered">
            <div class="blue-bg"></div>
            <div class="item-link"><a href="#" class="button button-grey">Посмотреть</a></div>
        </div>
        <img src="images/1.jpg">
    </div>
    <div class="corner-blue"></div>
    <div class="item-info">
        <div class="item-params">
            <div class="item-param">Тип: <b class="pull-right">4-ох комнатная</b></div>
            <div  class="item-param">Кв.м.: <b class="pull-right">60</b></div>
        </div>
        <div class="item-price">2 500 000 <span>руб.</span></div>
    </div>
</div>
<div class="clear"></div>
</div>

<div class="paginat">
    <a class="prev" href="#">Предыдущая</a>
    <a class="page active" href="#">1</a>
    <a class="page" href="#">2</a>
    <a class="page" href="#">3</a>
    <a class="next" href="#">Следующая</a>
</div>
<div class="clear"></div>
</div>
<!--//Main content-->

<!--Rightbar-->
<div class="rightbar">
    <div class="blocks">
        <div class="filtr">
            <div class="b-title">Фильтр</div>
            <form action="?">
                <div class="splitter">
                    <div class="type-input type-select">Сколько комнат?</div>
                    <div class="type-input type-select">Тип дома</div>
                </div>
                <div class="splitter">
                    <div class="param-title">Цена</div>
                    <div class="param-inputs">
                        <input id="price-start" class="price-start" type="text" value="1000">
                        <span style="margin: 0 1px">-</span>
                        <input id="price-end" class="price-end" type="text" value="5000">
                    </div>
                    <div class="range-metter"></div>
                    <div id="price-range"></div>

                </div>
                <div class="splitter">
                    <div class="param-title">Площадь</div>
                    <div class="param-inputs">
                        <input id="ploshad-start" class="price-start" type="text" value="20">
                        <span style="margin: 0 1px">-</span>
                        <input id="ploshad-end" class="price-end" type="text" value="70">
                    </div>
                    <div class="range-metter"></div>
                    <div id="ploshad-range"></div>
                </div>
                <div class="splitter" style="border-bottom: none">
                    <div class="type-input type-select">Район</div>
                </div>
                <div class="filtr-submit-button">
                    <button type="submit" class="button button-blue">Применить</button>
                </div>
            </form>
        </div>
    </div>

    <div class="blocks">
        <div class="banner">
            <div class="img"><img src="images/banner.jpg"/></div>
            <div class="corner-white"></div>
            <div class="banner-text">
                <div class="banner-title">Баннер</div>
                <div class="b-small-text">специальное предложение</div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!--//Rightbar-->