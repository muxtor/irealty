<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <title><?=$this->pageTitle?></title>
</head>
<body class="error-body">
<div class="container">
    <div class="error-block">
        <div class="error-content">
            <h2>404</h2>
            <p class="error-content-custom-text"><?php echo CHtml::encode($message); ?></p>
            <a href="/">На главную</a>
        </div>
    </div>
</div>
</body>
</html>