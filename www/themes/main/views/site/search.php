<!--Sell-buy-block-->
<div class="">
    <div class="container2">
        <div class="find-block">
            <form>
                <input name="q" class="find-input" onblur="if($(this).val()==''){$(this).val('Введите Ваш запрос. Например, “квартира однокомнатная”');}" onfocus="if($(this).val()=='Введите Ваш запрос. Например, “квартира однокомнатная”'){$(this).val('');}" value="<?php if(isset($_GET['q']) AND !empty($_GET['q'])){ echo $_GET['q']; }else{ ?>Введите Ваш запрос. Например, “квартира однокомнатная”<?php } ?>">
                <button class="find-button" type="submit"></button>
                <span style="margin-left: 50px"><input <?php if(isset($_GET['q']) AND isset($_GET['photo'])){ echo 'checked=checked'; }?> name="photo" type="checkbox"> Только с фото</span>
            </form>
        </div>
    </div>

</div>
<!--//Sell-buy-block-->

<!--Content-->
<div class="content">
<div class="container2">

<!--Main content-->
<div class="main-content">
<div class="ritems">
<?php

$this->widget('bootstrap.widgets.TbListView', array(
    'dataProvider'=>$data,
    'itemView'=>'_search_item',
    'ajaxUpdate'=>false,
    'enableSorting' => false,
    'template'=>'{pager}<div class="clear clearfix"></div>{items}<div class="clear clearfix"></div>{pager}{sorter}',
    'cssFile'=>false,
    'sorterHeader'=>'Сортировка',
    /*'sortableAttributes'=> array(
        'price'=>'Цена',
        'ploshad',

    ),*/
    //'sorterCssClass'=>'filter-menu sort',
    'pagerCssClass'=>'paginat',
    'pager'=>array(
        'maxButtonCount'=>'3',
        'header'         => '',
        /*'firstPageLabel' => '&lt;&lt;',
        'prevPageLabel'  => '<img src="images/pagination/left.png">',
        'nextPageLabel'  => '<img src="images/pagination/right.png">',
        'lastPageLabel'  => '&gt;&gt;',*/
    ),
    'htmlOptions'=>array('class'=>'',),
));
?>
<div class="clear"></div>
</div>
<div class="clear"></div>
</div>
<!--//Main content-->

<?php echo $this->renderPartial('../rightbar',array('filtr'=>'yes','banner'=>'yes')); ?>
</div>
</div>