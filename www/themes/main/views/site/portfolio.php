<section class="portfolio-main">
    <div class="portfolio-list-wrap">
        <ul class="portfolio-list">
            <?php foreach ($projects as $project): ?>
                <li>
                    <a href="<?= $this->createUrl('project', array('url' => $project->url)) ?>">
                        <?php if (!empty($project->portfolio_image)): ?>
                            <img src="<?=Project::IMAGE_PATH. $project->portfolio_image?>" alt="">
                        <?php else: ?>
                            <img src="/img/portfolio-img7.png" alt="">
                        <?php endif; ?>
                        <div class="portfolio-address-block">
                            <p><?=$project->adress?></p>
                        </div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>