<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" href="/fonts/fonts.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <title><?=$this->pageTitle?></title>
</head>
<body class="error-body">
    <div class="container">
        <div class="error-block">
            <div class="error-content">
                <h2>500</h2>
                <p>Оп!  Что-то пошло не так. И мы сейчас вовсю работаем над сайтом. Может, уже все работает?</p>
                <a href="#">Попробовать еще раз</a>
            </div>
        </div>
    </div>
</body>
</html>