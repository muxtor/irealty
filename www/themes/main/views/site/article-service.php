<?php 
    $type = $model->type;
    $is_article = ArticleService::CONTENT_ARTICLE;
?>
<section class="stat-main">
    <div class="stat-left-block">
        <h2><?=CHtml::encode($model->title)?></h2>
        <?php if ($type == $is_article): ?>
            <div class="date-text-block">
                <p><?= date('d.m.Y', strtotime($model->created_on)); ?></p>
            </div>
        <?php endif; ?>
        <div class="wysiwyg">
            <?= $model->text ?>
        </div>
        <div class="author-text">
            <?= $model->author_info ?>
        </div>
    </div>
    <div class="stat-right-block">
        <?php if (!empty($model->images)): ?>
            <?php foreach ($model->images as $image): ?>
                <img src="<?=ArticleService::IMAGE_FOLDER.$image->file?>" alt="">
            <?php endforeach; ?>
        <?php endif; ?>
        <img src="/img/stat-img2.png">
        
        <?php if (!empty($models)): ?>
            <div class="stat-links-wrap">
                <ul class="stat-links-list">
                    <?php foreach ($models as $listItem): ?>
                        <li><a href="<?=$this->createUrl('articleService', array('url' => $listItem->url, 'type' => $type))?>"><span><?=$listItem->title?></span></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</section>