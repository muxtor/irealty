<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 01.03.15
 * Time: 13:02
 */ ?>
<!--Map block-->
<div class="map-block <?php echo isset($closed)?'closed':'opened';?>">
    <div class="map-pm" onclick="if($(this).parent().hasClass('opened')){ $('.map-block').animate({height: '200px'}, 300);  $('.map-block').addClass('closed'); $('.map-block').removeClass('opened'); }else{ $('.map-block').animate({height: '700px'}, 300);  $('.map-block').addClass('opened'); $('.map-block').removeClass('closed'); }"></div>
    <div class="map" id="map"></div>
    <div class="container2">
        <div class="rightbar">
            <?php echo $this->renderPartial('../filtr'); ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<!--//Map block-->
<?php
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false');
$cs->registerScriptFile('/js/infobubble.js');

if(isset($_GET['filtr'])){
    $conditions = 'status=1';
    $criteria = new CDbCriteria();
    //$criteria->order='vip DESC';
    $criteria->condition = Realty::model()->myCriteria($conditions);
    $all = Realty::model()->findAll($criteria);
}else{
    $all = Realty::model()->findAllByAttributes(array(),'status=1');
}

?>
<!--<div class="ritems"> <div class="item "> <div class="img"> <div class="hovered"> <div class="blue-bg"></div> <div class="item-link"><a href="/sell/4" class="button button-grey">Посмотреть</a></div> </div> <img src="/images/1.jpg"> </div> <div class="corner-blue"></div> <div class="item-info"> <div class="item-params"> <div class="item-param">Комнат: <b class="pull-right">4-х комнатная</b></div> <div class="item-param">Кв.м.: <b class="pull-right">52</b></div> </div> <div class="item-price">2 656 009 <span>руб.</span></div> </div> </div> </div>-->
<script>
    function initialize() {
        var yakt = new google.maps.LatLng(62.03333299999999, 129.73333300000002);
        var myOptions = {
            zoom: 12,
            center: yakt,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        }

        var map = new google.maps.Map(document.getElementById("map"), myOptions);

        var contentString = '<div class="ritems" style="margin: 0; padding: 0; float: none; width: auto;"> <div class="item " style="margin: 0; padding: 0; float: none; width: auto;"> <div class="img"> <div class="hovered"> <div class="blue-bg"></div> <div class="item-link"><a href="/sell/4" class="button button-grey">Посмотреть</a></div> </div> <img src="/images/1.jpg"> </div> <div class="corner-blue"></div> <div class="item-info" style="border-bottom: none!important; border-left: none!important; border-right: none!important;"> <div class="item-params"> <div class="item-param">Комнат: <b class="pull-right">4-х комнатная</b></div> <div class="item-param">Кв.м.: <b class="pull-right">52</b></div> </div> <div class="item-price">2 656 009 <span>руб.</span></div> </div> </div> </div>';
        var icons = '/img/markers/blue_big.png';

        var realts = [
            <?php
            $arr = array();
            if(count($all>0)){
                foreach($all as $item){?>
                    <?php
                    $image = Image::model()->findByAttributes(array('parent_id'=>$item->id),'type="realty"');
                    if($image!==null){
                         $img = PreDefineUrl::RealtyItemsThumb().Realty::IMAGE_FOLDER.$image->file;
                    }else{
                        $img = '/images/1.jpg';
                    } ?>
                    [
                        <?php echo $item->id; //id 0 ?>,
                        '<?php echo number_format($item->price,0, ' ', ' '); //цена 1 ?>',
                        '<?php echo $img; //рисунок 2 ?>',
                        <?php echo $item->type; //тип дом 3 ?>,
                        <?php echo $item->rooms; //комната 4 ?>,
                        <?php echo empty($item->lat)?'62.03333299999999':$item->lat; //широта 5 ?>,
                        <?php echo empty($item->lng)?'129.73333300000002':$item->lng; //долгота 6 ?>,
                        <?php echo $item->vip; //VIP 7 ?>,
                        <?php echo $item->hot; //Горячие 8 ?>,
                        <?php echo $item->ploshad; //Горячие 9 ?>
                    ],
                    <?php
                }?>

            <?php
    }
?>
        ];
        setMarkers(map, realts);
    }
    function setMarkers(map, locations) {
        var latlngbounds = new google.maps.LatLngBounds();
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var myLatLng = new google.maps.LatLng(beach[5], beach[6]);
            if(beach[7]==0){
                var image = '/img/markers/blue_big.png';
            }else{
                var image = '/img/markers/green_big.png';
            }

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: '',
                zIndex: beach[0]
            });
            latlngbounds.extend(myLatLng);
            attachMessage(marker, i, locations, map);
        }

        map.fitBounds(latlngbounds);

    }
    function attachMessage(marker, number, locations, map) {
        var message='<div id="company_name">Название компании'+locations[number].title+'</div>'+
            '<div id="company_time"> Время работы'+locations[number].info_time+'</div>';

        var contentString = '<div class="ritems" style="margin: 0; padding: 0; float: none; width: auto;">' +
            '<div class="item " style="margin: 0; padding: 0; float: none; width: auto;">' +
            '<div class="img"><a href="/sell/'+locations[number][0]+'" style="text-decoration: none;"><div class="hovered"><div class="blue-bg"></div>' +
            '<div class="item-link"><a href="/sell/'+locations[number][0]+'" class="button button-grey">Посмотреть</a></div>' +
            '</a></div>' +
            '<img src="'+locations[number][2]+'">' +
            '</div>' +
            '<div class="corner-blue"></div>' +
            '<div class="item-info" style="border-bottom: none!important; border-left: none!important; border-right: none!important;">' +
            '<div class="item-params">' +
            '<div class="item-param">Комнат: <b class="pull-right">'+locations[number][4]+' комнатная</b></div>' +
            '<div class="item-param">Кв.м.: <b class="pull-right">'+locations[number][9]+'</b></div></div>' +
            '<div class="item-price">'+locations[number][1]+' <span>руб.</span></div> </div> </div> </div>';

        number = new InfoBubble({
            content: contentString,
            shadowStyle: 0,
            padding: 0,
            borderRadius: 0,
            arrowSize: 10,
            borderWidth: 1,
            borderColor: '#0771a2',
            disableAutoPan: true,
            hideCloseButton: false,
            arrowPosition: 50,
            backgroundClassName: 'phoney',
            arrowStyle: 0
        });
        google.maps.event.addListener(marker, 'click', function() {
            if (!number.isOpen()) {
                number.open(map, marker);
            }
        });
    }
    initialize();
</script>





