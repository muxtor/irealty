<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 04.03.15
 * Time: 7:58
 */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'form-login',
    'action'=>'/user/login',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validateOnType' => false,
    ),
    'focus' => array($model, 'username'),
    'htmlOptions' => array(
        'class' => 'separate-sections'
    ),
));
?>
<?php echo $form->error($model, 'password', array('class' => 'alert alert-error')); ?>


    <?php echo $form->textField($model, 'username', array('class'=>'log-form','placeholder' => 'Ваш номер')); ?>
    <?php $form->error($model, 'username'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <script>
        $('.log-form').inputmasks(optsRU);
    </script>
    <?php echo $form->passwordField($model, 'password', array('placeholder' => $model->getAttributeLabel('password'))); ?>
    <?php $form->error($model, 'password'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <?php echo $form->hiddenField($model, 'rememberMe'); ?>
    <?php //echo $model->getAttributeLabel('rememberMe'); ?>
    <div><a href="/user/signup" >Регистрация</a>   /  <a href="/user/remind">Забыли пароль?</a></div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <div>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('user', 'Войти в кабинет'),
            'icon' => 'icon-signin',
            'buttonType' => 'submit',
            'size' => null,
            'htmlOptions' => array(
                'class' => 'button button-blue',
                'id' => 'submit',
            )
        ));
        ?>
        <?php /*Yii::app()->clientScript->registerScript('loginSubmit', '
                        $("#submit").click(function(){
                            $("#form-login").submit();
                        });

                        ', CClientScript::POS_LOAD);*/ ?>
    </div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
<?php $this->endWidget(); ?>