<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 04.03.15
 * Time: 7:58
 */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'form-login',
    'action'=>'/user/change',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validateOnType' => false,
    ),
    'focus' => array($model, 'username'),
    'htmlOptions' => array(
        'class' => 'separate-sections'
    ),
));
?>
<?php echo $form->error($model, 'password', array('class' => 'alert alert-error')); ?>


    <?php echo $form->passwordField($model, 'password', array('placeholder' => $model->getAttributeLabel('password2'))); ?>
    <?php $form->error($model, 'password'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>

    <?php echo $form->passwordField($model, 'password2', array('placeholder' => $model->getAttributeLabel('password2'))); ?>
    <?php $form->error($model, 'password2'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <div>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('user', 'Изменить пароль'),
            'icon' => 'icon-signin',
            'buttonType' => 'submit',
            'size' => null,
            'htmlOptions' => array(
                'class' => 'button button-blue',
                'id' => 'submit',
            )
        ));
        ?>
        <?php /*Yii::app()->clientScript->registerScript('loginSubmit', '
                        $("#submit").click(function(){
                            $("#form-login").submit();
                        });

                        ', CClientScript::POS_LOAD);*/ ?>
    </div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
<?php $this->endWidget(); ?>