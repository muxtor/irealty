
                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-user',
                        'action'=> '/user/confirm',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);?>
                        <?php echo $form->hiddenField($model, 'phone', array('class' => 'span12','readonly'=>'readonly', 'placeholder' => 'Ваш номер')); ?>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        <?php echo $form->textField($model, 'code', array('class' => 'span12', 'placeholder' => 'Код')); ?>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                    </div>
                </div>


                <!--end paped -->
                <!--end paped -->
                <div class="form-actions" style="text-align:center;">
                    <?php
                    $this->widget(
                        'bootstrap.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'label' => Yii::t('admin', 'Потвердить'),
                            'type' => null,
                            'htmlOptions' => array(
                                'class' => 'button button-blue',
                                'value' => 'index',
                                'name' => 'submit',
                            ),
                            'size' => 'small',
                        )
                    );
                    ?>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
