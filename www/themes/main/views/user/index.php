
<h1>Персонал кабинет: <div class="form-actions pull-right" style="text-align:center;">
        <a class="button button-blue btn btn-small" href="/user/logout">Выйти</a></div></h1>
<div class="sell-form" style="width: 50%; float: left; text-align:center; ">
    <h2 style="font-size:24px; color: #212121; font-weight: bold; margin-bottom:30px;">Изменить имя</h2>
    <?php
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'id' => 'form-user',
            //'action'=>'/user/signup',
            'type' => 'vertical',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'validateOnType' => false,
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data'
            ),
        )
    );
    ?>

    <div class="box-content">

        <div class="padded">
            <?php echo $form->errorSummary($model);?>
            <?php echo $form->textField($model, 'name', array('class' => 'span12', 'placeholder' => 'Ваш имя')); ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
        </div>


    </div>


    <!--end paped -->
    <!--end paped -->
    <div class="form-actions" style="text-align:center;">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'label' => Yii::t('admin', 'Изменить имя'),
                'type' => null,
                'htmlOptions' => array(
                    'class' => 'button button-blue',
                    'value' => 'index',
                    'name' => 'submit',
                ),
                'size' => 'small',
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>
    <!-- end box content -->
</div>
<div class="sell-form" style="width: 50%; float: left; text-align:center; ">
    <h2 style="font-size:24px; color: #212121; font-weight: bold; margin-bottom:30px;">Изменить пароль</h2>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'form-login',
        //'action'=>'/user',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
        ),
        'focus' => array($change, 'username'),
        'htmlOptions' => array(
            'class' => 'separate-sections'
        ),
    ));
    ?>
    <?php echo $form->error($change, 'password', array('class' => 'alert alert-error')); ?>


    <?php echo $form->passwordField($change, 'password', array('placeholder' => $change->getAttributeLabel('password'))); ?>
    <?php $form->error($change, 'password'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>

    <?php echo $form->passwordField($change, 'password2', array('placeholder' => $change->getAttributeLabel('password2'))); ?>
    <?php $form->error($change, 'password2'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <div>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('user', 'Изменить пароль'),
            'icon' => 'icon-signin',
            'buttonType' => 'submit',
            'size' => null,
            'htmlOptions' => array(
                'class' => 'button button-blue',
                'id' => 'submit',
            )
        ));
        ?>
        <?php /*Yii::app()->clientScript->registerScript('loginSubmit', '
                        $("#submit").click(function(){
                            $("#form-login").submit();
                        });

                        ', CClientScript::POS_LOAD);*/ ?>
    </div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <?php $this->endWidget(); ?>
</div>
<div class="clear clearfix" style="clear: both"></div>
<div class="sell-form" style="text-align:center; ">
    <h2 style="font-size:18px; color: #212121; font-weight: bold; margin-bottom:30px;">Получите доступ к <?php echo Realty::model()->count('status=1')?>
        собственникам с нашего
        сайта прямо сейчас!</h2>
    <?php
    $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'id' => 'form-user',
            'action'=>'/pay',
            'type' => 'vertical',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'validateOnType' => false,
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data'
            ),
        )
    );
    ?>

    <div class="box-content">
        <style>

        </style>

        <div class="padded">
            <?php echo $form->errorSummary($pay);  ?>
            <?php //echo $form->radioButtonList($model, 'tarifId', CHtml::listData($tariffs, 'id' ,'price'), array('class' => '')); ?>
            <input name="Pays[tarifId]" id="Pays_tarifId_0" value="0" type="hidden" >
            <?php if(count($tariffs)>0){?>
                <div class="tariffs">
                    <?php foreach($tariffs as $tarif){?>
                        <div class="titem <?php echo $tarif->color; ?>" >
                            <h3 style="text-align: center;"><?php echo $tarif->title; ?></h3>
                            <label for="Pays_tarifId_<?=$tarif->id;?>" style="width: 100%;">
                                <div class="tinput">
                                    <input name="Pays[tarifId]" id="Pays_tarifId_<?=$tarif->id;?>" value="<?=$tarif->id;?>" type="radio" >
                                    <?=$tarif->srok;?>
                                    <?=$tarif->srokTitle; ?>
                                </div>
                                <div class="tprice">
                                    <?=$tarif->price;?> руб.
                                </div>
                                <div class="toldprice">
                                    <?=$tarif->oldprice>0?$tarif->oldprice.' руб.':'';?>
                                </div>
                            </label>
                        </div>
                        <div class="clear clearfix" style="margin-bottom:5px;"></div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="clear clearfix" style="margin-bottom:19px;"></div>
        </div>


    </div>


    <!--end paped -->
    <!--end paped -->
    <div class="form-actions" style="text-align:center;">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'label' => Yii::t('admin', 'Получить доступ'),
                'type' => null,
                'htmlOptions' => array(
                    'class' => 'button button-blue',
                    'value' => 'index',
                    'name' => 'submit',
                ),
                'size' => 'small',
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>
    <!-- end box content -->
</div>