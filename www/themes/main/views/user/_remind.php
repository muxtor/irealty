<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 04.03.15
 * Time: 7:58
 */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'form-login',
    'action'=>'/user/remind',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validateOnType' => false,
    ),
    'focus' => array($model, 'username'),
    'htmlOptions' => array(
        'class' => 'separate-sections'
    ),
));
?>
<?php echo $form->error($model, 'phone', array('class' => 'alert alert-error')); ?>


    <?php echo $form->textField($model, 'phone', array('placeholder' => 'Ваш номер')); ?>
    <?php $form->error($model, 'phone'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
<script>
    $('#RemindForm_phone').inputmasks(optsRU);
</script>

    <div><button type="button" class="getCode button button-green">Отправить</button> Операция бесплатная</div>
    <div>* Следующий раз возможен через 20 минут</div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>

    <?php echo $form->textField($model, 'code', array('readonly'=>'readonly','placeholder' => $model->getAttributeLabel('code'))); ?>
    <?php $form->error($model, 'code'); ?>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
    <div>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('user', 'Изменить пароль'),
            'icon' => 'icon-signin',
            'buttonType' => 'submit',
            'size' => null,
            'htmlOptions' => array(
                'class' => 'smb button button-blue',
                'id' => 'submit',
                'readonly' => 'readonly',
                'style'=>'opacity:0.5',
            )
        ));
        ?>
        <?php /*Yii::app()->clientScript->registerScript('loginSubmit', '
                        $("#submit").click(function(){
                            $("#form-login").submit();
                        });

                        ', CClientScript::POS_LOAD);*/ ?>
    </div>
    <div class="clear clearfix" style="margin-bottom:19px;"></div>
<?php $this->endWidget(); ?>
<script>
    $(document).ready(function(){
        /*$('.getCode').click(function(){
            var s = $('form').serialize();
            $.post(
                "?getCode=yes",
                s,
                onAjaxSuccess
            );

            function onAjaxSuccess(data)
            {
                if(data=='20min'){
                    $('#form-login input').removeAttr('readonly');
                    $('#form-login .smb').css('opacity','1');
                    $('#form-login .smb').removeAttr('readonly');
                    alert('Следующий раз возможен через 20 минут');
                }else{
                    if(data=='ok'){
                        $('#form-login input').removeAttr('readonly');
                        $('#form-login .smb').css('opacity','1');
                        $('#form-login .smb').removeAttr('readonly');
                        alert('СМС с кодом отправлен на номер');
                    }else{
                        alert(data);
                    }
                }
            }
        })*/
    })


</script>