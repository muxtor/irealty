
                <?php
                $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'id' => 'form-user',
                        'action'=>'/user/signup',
                        'type' => 'vertical',
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => false,
                            'validateOnType' => false,
                        ),
                        'htmlOptions' => array(
                            'enctype' => 'multipart/form-data'
                        ),
                    )
                );
                ?>

                <div class="box-content">

                    <div class="padded">
                        <?php echo $form->errorSummary($model);?>
                        <?php echo $form->textField($model, 'name', array('class' => 'span12', 'placeholder' => 'Ваш имя')); ?>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        <?php echo $form->textField($model, 'login', array('class' => 'phoned span12', 'placeholder' => 'Ваш номер')); ?>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                        <script>
                            $('.phoned').inputmasks(optsRU);
                        </script>
                        <?php echo $form->passwordField($model, 'pswd', array('class' => 'span12', 'placeholder' => 'Пароль')); ?>
                        <div class="clear clearfix" style="margin-bottom:19px;"></div>
                    </div>


                </div>


                <!--end paped -->
                <!--end paped -->
                <div class="form-actions" style="text-align:center;">
                    <?php
                    $this->widget(
                        'bootstrap.widgets.TbButton',
                        array(
                            'buttonType' => 'submit',
                            'label' => Yii::t('admin', 'Регистрация'),
                            'type' => null,
                            'htmlOptions' => array(
                                'class' => 'button button-blue',
                                'value' => 'index',
                                'name' => 'submit',
                            ),
                            'size' => 'small',
                        )
                    );
                    ?>
                </div>

                <?php $this->endWidget(); ?>
                <!-- end box content -->
