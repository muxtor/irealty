<div class="sell-form" style="width: 360px; text-align:center; ">
    <h1 style="font-size:24px; color: #212121; font-weight: bold; margin-bottom:30px;">Потвердить ваш номер</h1>
    <?php
        $this->pageTitle = 'Регистрация';
        echo $this->renderPartial('_confirm', array('model' => $model, 'details' => $details, 'icon' => 'icon-edit'));
    ?>
</div>