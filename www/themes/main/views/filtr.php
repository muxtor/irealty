<?php
/**
 * Created by PhpStorm.
 * User: Ulugbek
 * Date: 01.03.15
 * Time: 12:59
 */ ?>
<!--Rightbar-->
<?php if (isset($_GET['filtr'])) {
    $filtrs = $_GET['filtr'];
} ?>
<style>.blocks label{ font-weight: normal!important; margin-bottom: 0!important; cursor: pointer;}</style>
<div class="blocks">
    <div class="filtr">
        <div class="b-title">Фильтр</div>
        <form action="/buy" method="get">
            <div class="splitter">
                <div class="type-input type-select" style="height: auto;">
                    <span>Сколько комнат?</span>
                    <div class="select-inner">
                        <?php $rooms = RealtyRooms::model()->findAll(); ?>
                        <?php foreach ($rooms as $room) { ?>
                            <div><label for="r_<?php echo $room->id_room; ?>"><input
                                        id="r_<?php echo $room->id_room; ?>"
                                    name="filtr[room][<?php echo $room->id_room; ?>]" <?php if (isset($filtrs['room'][$room->id_room])) {
                                    echo 'checked="checked"';
                                } ?> value="<?php echo $room->id_room; ?>" type="checkbox"> <?php echo $room->room; ?></label>
                            </div>
                        <?php } ?>
                    </div>

                </div>
                <div class="type-input type-select" style="height: auto;">
                    <span>Тип дома</span>
                    <div class="select-inner">
                        <?php $types = RealtyType::model()->findAll(); ?>
                        <?php foreach ($types as $type) { ?>
                            <div><label for="t_<?php echo $type->id_type; ?>"><input
                                        id="t_<?php echo $type->id_type; ?>"
                                    name="filtr[tip][<?php echo $type->id_type; ?>]" <?php if (isset($filtrs['tip'][$type->id_type])) {
                                    echo 'checked="checked"';
                                } ?> value="<?php echo $type->id_type; ?>" type="checkbox"> <?php echo $type->type; ?></label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="splitter">
                <div class="param-title">Цена</div>
                <div class="param-inputs">
                    <input id="price-start" name="filtr[priceStart]" class="price-start" type="text" value="1000">
                    <span style="margin: 0 1px">-</span>
                    <input id="price-end" name="filtr[priceEnd]" class="price-end" type="text" value="5000">
                </div>
                <div class="range-metter"></div>
                <div id="price-range"></div>

            </div>
            <div class="splitter">
                <div class="param-title">Площадь</div>
                <div class="param-inputs">
                    <input id="ploshad-start" name="filtr[ploshadStart]" class="price-start" type="text" value="20">
                    <span style="margin: 0 1px">-</span>
                    <input id="ploshad-end" name="filtr[ploshadEnd]" class="price-end" type="text" value="70">
                </div>
                <div class="range-metter"></div>
                <div id="ploshad-range"></div>
            </div>
            <div class="splitter" style="border-bottom: none">
                <div class="type-input type-select" style="height: auto;">
                    <span>Район</span>
                    <div class="select-inner" style="bottom: 100%;">
                        <?php $rayons = Rayon::model()->findAll(); ?>
                        <?php foreach ($rayons as $rayon) { ?>
                            <div><label for="rr_<?php echo $rayon->id_rayon; ?>"><input
                                        id="rr_<?php echo $rayon->id_rayon; ?>"
                                    name="filtr[rayon][<?php echo $rayon->id_rayon; ?>]" <?php if (isset($filtrs['rayon'][$rayon->id_rayon])) {
                                    echo 'checked="checked"';
                                } ?> value="<?php echo $rayon->id_rayon; ?>"
                                    type="checkbox"> <?php echo $rayon->rayon; ?></label>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="filtr-submit-button">
                <button type="submit" class="button button-blue">Применить</button>
            </div>
        </form>
    </div>
</div>
<script>
    $('.select-inner').mouseleave(function(){
        $('.opened .select-inner').hide();
        $('.opened').removeClass('opened');
    });
    $('.type-select span').click(function(){
        if($(this).parent().hasClass('opened')){
            $('.opened .select-inner').hide();
            $('.opened').removeClass('opened');
        }else{
            $(this).parent().find('.select-inner').show();
            $(this).parent().addClass('opened');
        }


    })


    //filtr-slider-range-input
    $(function () {
        <?php
        $criteria = new CDbCriteria();
        $criteria->order='price DESC';
        $criteria->limit=1;
        $fprice = Realty::model()->find($criteria);
    ?>
        $("#price-range").slider({
            range: true,
            min: 0,
            max: <?php echo $fprice->price; ?>,
            values: [
                <?php echo isset($filtrs['priceStart'])?$filtrs['priceStart']:'0';?>,
                <?php echo isset($filtrs['priceEnd'])?$filtrs['priceEnd']:$fprice->price;?>
            ],
            slide: function (event, ui) {
                $("#price-start").val(ui.values[ 0 ]);
                $("#price-end").val(ui.values[ 1 ]);
            }
        });
        $("#price-start").val($("#price-range").slider("values", 0));
        $("#price-end").val($("#price-range").slider("values", 1));
    });

    $(function () {
        <?php
        $criteria1 = new CDbCriteria();
        $criteria1->order='ploshad DESC';
        $criteria1->limit=1;
        $fploshad = Realty::model()->find($criteria1);
    ?>
        $("#ploshad-range").slider({
            range: true,
            min: 0,
            max: <?php echo $fploshad->ploshad; ?>,
            values: [
                <?php echo isset($filtrs['ploshadStart'])?$filtrs['ploshadStart']:'0';?>,
                <?php echo isset($filtrs['ploshadEnd'])?$filtrs['ploshadEnd']:$fploshad->ploshad;?>
            ],
            slide: function (event, ui) {
                $("#ploshad-start").val(ui.values[ 0 ]);
                $("#ploshad-end").val(ui.values[ 1 ]);
            }
        });
        $("#ploshad-start").val($("#ploshad-range").slider("values", 0));
        $("#ploshad-end").val($("#ploshad-range").slider("values", 1));
    });
</script>
