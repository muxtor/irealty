<?php
if (getenv('ENV') != 'PRODUCTION') {
	$yii=dirname(__FILE__).'/../../framework/yii.php';
	$path = dirname(__FILE__).'/../protected/config/';
	ini_set('display_errors',1);
	error_reporting(E_ALL);
	ini_set('output_buffering', 'on');
	//defined('YII_DEBUG') or define('YII_DEBUG',true);
	// specify how many levels of call stack should be shown in each log message
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
} else {
	defined('PRODUCTION') or define('PRODUCTION', true);
    $yii=dirname(__FILE__).'/../../framework/yii.php';
    $path = dirname(__FILE__).'/../protected/config/';
	//$path = getcwd().'/../protected/config/';
}

$config = $path.'main.php';
require_once($yii);
require_once('globals.php');
Yii::createWebApplication($config)->run();