/**
 * Created by Ulugbek on 20.02.15.
 */
$(document).ready(function(){
    $('.getCode').click(function(){
        var s = $('form').serialize();
        $.post(
            "?getCode=yes",
            s,
            onAjaxSuccess
        );

        function onAjaxSuccess(data)
        {
            if(data=='20min'){
                $('#form-login input').removeAttr('readonly');
                $('#form-login .smb').css('opacity','1');
                $('#form-login .smb').removeAttr('readonly');
                alert('Следующий раз возможен через 20 минут');
            }else{
                if(data=='ok'){
                    $('#form-login input').removeAttr('readonly');
                    $('#form-login .smb').css('opacity','1');
                    $('#form-login .smb').removeAttr('readonly');
                    alert('СМС с кодом отправлен на номер');
                }else{
                    alert(data);
                }
            }
        }
    })
})
$(function () {
    $("#price").slider({
        min: 0,
        max: 10000000,
        values: [500000],
        slide: function (event, ui) {
            $("#Realty_price").val(ui.values[ 0 ]);
        }
    });
    $("#Realty_price").val($("#price").slider("values", 0));
});

$(function () {
    $("#ploshad").slider({
        min: 0,
        max: 100,
        values: [20],
        slide: function (event, ui) {
            $("#Realty_ploshad").val(ui.values[ 0 ]);
        }
    });
    $("#Realty_ploshad").val($("#ploshad").slider("values", 0));
});

