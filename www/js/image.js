// Удаление загруженной фотки
function deletePhoto(obj) {
    totalImg = totalImg - 1;
    $.ajax({

        type: "GET",

        url: "http://" + location.host + "/realty/deletePhoto",

        data: {filename: obj.attr('data')},
        success: function () {

            obj.parent().remove();


        }


    });

    return false;
}
// Удаление загруженной фотки
function deleteNewsPhoto(obj) {
    totalImg = totalImg - 1;
    $.ajax({

        type: "GET",

        url: "http://" + location.host + "/realty/deletePhoto",

        data: {filename: obj.attr('data')},
        success: function () {

            obj.parent().remove();


        }


    });

    return false;
}

$(document).ready(function(){
    $(".deletePhoto").on('click', function () {
        deletePhoto($(this))
    });
    $(".deleteNewsPhoto").on('click', function () {
        deleteNewsPhoto($(this))
    });


    //Загрузка изображений на сервер
    var bar = $('.progress-bar>span');
    var percent = $('.percent');
    var status = $('#status');

    totalImg = $(".file").length;


    $("#upload1").on('change', function () {
        $('#formUploadFiles').submit();
    });

    // Ajax загрузка изображений
    $('#formUploadFiles').ajaxForm({
        beforeSend: function (xhr) {
            var inputFile = document.getElementById('upload1').files;

            if ((totalImg + inputFile.length) > 13) {
                alert('Не возможно загрузить больше 13 фотографий');

                xhr.abort();
            }
            else {
                totalImg = totalImg + inputFile.length;
                console.log(totalImg);

                var percentVal = '0%';
                bar.show();
                bar.width(percentVal);
                percent.html(percentVal);
            }

        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
        },
        success: function () {
            var percentVal = '100%';
            bar.width(percentVal)
            percent.html(percentVal);

        },
        complete: function (xhr) {
            status.append(xhr.responseText);
            bar.hide();
            $(".deletePhoto").unbind('click');
            $(".deletePhoto").on('click', function () {
                deletePhoto($(this))
            })
        }
    });
})
